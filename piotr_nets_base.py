

class Position:
    def __init__(self, tokens_num=0):
        self.id = next(Position.enum)
        self.tokens_num = tokens_num
        self.outs = []
        self.ins = []


class Transition:
    def __init__(self):
        self.id = next(Transition.enum)
        self.outs = []
        self.ins = []

    def is_active(self):
        for i in self.ins:
            if i.tokens_num == 0:
                return False

        return True


def up_number():
    i = 1
    while True:
        yield i
        i += 1


Position.enum = up_number()
Transition.enum = up_number()


class Graph:
    def __init__(self):
        self.p1 = Position()
        self.p2 = Position()
        self.p3 = Position(2)
        self.p4 = Position(1)
        self.p5 = Position()
        self.t1 = Transition()
        self.t2 = Transition()
        self.t3 = Transition()
        self.t4 = Transition()

        add_edge(self.p1, self.t1)
        add_edge(self.t3, self.p1)
        add_edge(self.t3, self.p2)
        add_edge(self.p2, self.t2)
        add_edge(self.t1, self.p4)
        add_edge(self.p4, self.t3)
        add_edge(self.p3, self.t3)
        add_edge(self.t2, self.p3)
        add_edge(self.t4, self.p4)
        add_edge(self.p5, self.t4)

        self.positions = [self.p1, self.p2, self.p3, self.p4, self.p5]
        self.transitions = [self.t1, self.t2, self.t3, self.t4]

        for pos in self.positions:
            pos.ins.sort(key=lambda x: x.id)
            pos.outs.sort(key=lambda x: x.id)

        for tran in self.transitions:
            tran.ins.sort(key=lambda x: x.id)
            tran.outs.sort(key=lambda x: x.id)

    def tick(self):
        active_trans = []
        for i in range(len(self.transitions)):
            if self.transitions[i].is_active():
                active_trans.append(i)

        for i in active_trans:
            for inp in self.transitions[i].ins:
                inp.tokens_num -= 1

            for outp in self.transitions[i].outs:
                outp.tokens_num += 1


def add_in(thing, input_thing):
    thing.ins.append(input_thing)


def add_out(thing, output_thing):
    thing.outs.append(output_thing)


def add_edge(to, _from):
    add_in(_from, to)
    add_out(to, _from)

if __name__ == '__main__':
    g = Graph()
    g.tick()
    g.tick()
    markup = [pos.tokens_num for pos in g.positions]
    print(markup)



