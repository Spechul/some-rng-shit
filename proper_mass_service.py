"""
intermediate tick is discrete, equals 1
"""
from collections import deque
from random import uniform
from numpy.random import exponential
import matplotlib.pyplot as plt


def up_number():
    i = 0
    while True:
        yield i
        i += 1


class Event:
    def __init__(self, time: int):
        self.id = next(Event.enumerator)
        self.is_out = False
        self.spawn_time = time
        self.processed_time = 0
        self.time_in_queue = 0 # in intermediate ticks


Event.enumerator = up_number()


class WorkingThread:
    def __init__(self):
        self.id = next(WorkingThread.enumerator)
        self.busy = False
        self.release_tick = 0
        self.event = None


WorkingThread.enumerator = up_number()


class MassService:
    def __init__(self, n: int, m: int, _lambda: float, _mu: float, p: float, tick_size=1.0):
        self.n = n
        self.m = m
        self._lambda = _lambda # input frequency
        self._mu = _mu # output frequency
        self.p = p
        self.current_tick = 0 # on big values float won't work because of precision
        self.intermediate_last_tick = 0
        self.threads = [WorkingThread() for _ in range(n)]
        self.queue = deque(maxlen=m)
        self.tick_size = tick_size

        self.processed = []
        self.requeued = []
        self.thrown_away = []
        self.events_in_queue_by_tick = []
        self.events_processing_by_tick = []
        self.events_in_system_by_tick = []
        self.all_events = []

    def _process(self, thread: WorkingThread, event: Event):
        #if event.is_out: return
        possibility = uniform(0, 1)

        if possibility > self.p:
            if len(self.queue) < self.queue.maxlen:
                self.queue.append(event)
                event.processed_time = thread.release_tick
                self.requeued.append(event)
            else:
                event.is_out = True
                event.processed_time = thread.release_tick
                self.thrown_away.append(event)
        else:
            event.is_out = True
            event.processed_time = thread.release_tick
            self.processed.append(event)

    def _can_deploy(self):
        for t in self.threads:
            if not t.busy:
                return True
        return False

    def _deploy_task(self):
        event = self.queue.popleft()
        for i in range(len(self.threads)):
            if not self.threads[i].busy:
                self.threads[i].busy = True
                self.threads[i].release_tick = self.current_tick + exponential(1 / self._mu) #int(exponential(self._mu))
                self.threads[i].event = event
                break

    def _all_threads_free(self):
        for thread in self.threads:
            if thread.busy:
                return False
        return True

    def test(self, events_number: int):
        if events_number <= 0: return
        self.intermediate_last_tick = 0
        self.current_tick = 0
        next_event_time = 0
        while True:
            if self.current_tick >= next_event_time and events_number >= 0:
                event = Event(self.current_tick)
                if len(self.queue) < self.queue.maxlen:
                    self.queue.append(event)
                else:
                    event.is_out = True
                    event.processed_time = self.current_tick
                    self.thrown_away.append(event)
                events_number -= 1
                next_event_time = self.current_tick + exponential(1 / self._lambda) #int(exponential(self._lambda))

            for thread in self.threads:
                if thread.release_tick <= self.current_tick:
                    if thread.event:
                        self._process(thread, thread.event)
                    thread.release_tick = 0
                    thread.busy = False
                    thread.event = None

            while len(self.queue) > 0 and self._can_deploy():
                self._deploy_task()

            self.events_in_queue_by_tick.append(len(self.queue))
            current_processing = 0
            for thread in self.threads:
                if thread.busy:
                    current_processing += 1
            self.events_processing_by_tick.append(current_processing)
            self.events_in_system_by_tick.append(self.events_processing_by_tick[-1] + self.events_in_queue_by_tick[-1]) # values [-1] are just generated values

            for event in self.queue:
                event.time_in_queue += 1 # intermediate tick

            if events_number <= 0 and len(self.queue) <= 0 and self._all_threads_free(): return # waits, while events end and queue is empty

            self.current_tick += self.tick_size
            self.intermediate_last_tick += 1


if __name__ == '__main__':
    threads = 3
    queue_len = 10
    _lambda =  0.5
    _mu = 1.5
    p = 0.8
    tick_size = 0.01
    ms = MassService(threads, queue_len, _lambda, _mu, p, tick_size=tick_size)
    n = 10000
    ms.test(n)
    print("processed")
    print(len(ms.processed))#, ms.processed)
    print("requeued")
    print(len(ms.requeued))#, ms.requeued)
    print("thrown away")
    print(len(ms.thrown_away))#, ms.thrown_away)

    """proc = [p.processed_time for p in ms.processed]
    plt.hist(proc)
    plt.show()

    req = [r.processed_time for r in ms.requeued]
    plt.hist(req)
    plt.show()

    ta = [t.processed_time for t in ms.thrown_away]
    plt.hist(ta)
    plt.show()"""

    #print(ms.events_processing_by_tick)
    #print(ms.events_in_queue_by_tick)
    #print(ms.events_in_system_by_tick)

    import numpy as np
    print("average time in system")
    system_times = []
    for event in ms.processed:
        system_times.append(event.processed_time - event.spawn_time)
    for event in ms.thrown_away:
        system_times.append(event.processed_time - event.spawn_time)
    print(np.mean(system_times))

    print("average time in queue")
    queue_times = []
    for event in ms.processed:
        queue_times.append(event.time_in_queue)
    for event in ms.thrown_away:
        queue_times.append(event.time_in_queue)
    print(np.mean(queue_times))

    print("ticks passed")
    ticks_passed = ms.current_tick / tick_size
    print(ticks_passed)
    print(ms.intermediate_last_tick)

    print("average bandwidth")
    bandwidth = n / ticks_passed
    print(bandwidth, "events per tick")

    length = threads + queue_len
    s_0_n_nm = [0] * length

    for i in range(len(ms.events_in_queue_by_tick)): # i is the same for this and queue events sizes
        index = ms.events_in_queue_by_tick[i] + ms.events_processing_by_tick[i] - 1
        s_0_n_nm[index] += 1

    #print(s_0_n_nm)
    all_states = sum(s_0_n_nm)

    possibilities = [s_0_n_nm[i] / all_states for i in range(len(s_0_n_nm))]

    plt.plot(range(len(possibilities)), possibilities)
    #plt.plot(range(len(s_0_n_nm)), s_0_n_nm)
    plt.show()

    print("processed frequency")
    print(len(ms.processed) / n)

    print("requeue frequency")
    print(len(ms.requeued) / n)

    print("thrown away frequency")
    print(len(ms.thrown_away) / n)

    plt.xlim((0, 1000))
    plt.plot(range(len(ms.events_processing_by_tick)), ms.events_processing_by_tick)
    plt.show()



