import numpy as np
#import pandas as pd
np.histogram2d

from basic_rand import LCG

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class Discrete2D:
    def __init__(self, n, m, xvals, yvals, theor=None):
        self.gen = LCG()
        self.xvals = xvals
        self.yvals = yvals
        self.total_vars = n * m
        self.n = n
        self.m = m
        self.total = n * m
        self.theor = self._create_theor(theor)
        self.x_prob_t, self.y_prob_t = self._build_components(self.theor)

    def _create_theor(self, theor):
        if theor: return np.array(theor)
        return np.array([[1 / self.total] * self.m] * self.n)

    def _build_components(self, Z):
        x_probs = [np.sum(Z[i, :]) for i in range(self.n)]
        y_probs = [np.sum(Z[:, i]) for i in range(self.m)]
        return x_probs, y_probs

    def generate(self, cnt):
        Z = np.zeros((self.n, self.m))

        for i in range(cnt):
            x, y = self.next_pos()
            Z[x][y] += 1

        self.emp = self._norm(Z)
        self.total = cnt
        self.x_emp = [np.sum(self.emp[i, :]) for i in range(self.n)]
        self.y_emp = [np.sum(self.emp[:, i]) for i in range(self.m)]


    def get_emp(self, cnt):
        Z = np.zeros((self.n, self.m))

        for i in range(cnt):
            x, y = self.next_pos()
            Z[x][y] += 1

        return self._norm(Z)

    def next_pos(self):
        x = self._next_id(self.gen, self.x_prob_t)
        a = self.x_prob_t[x]
        y = self._next_id(self.gen, self.theor[x, :], a)
        return x, y

    @staticmethod
    def _norm(mat):
        return np.array(mat) / np.sum(mat)

    @staticmethod
    def _next_id(gen, probs, max_prob=1):
        cur_prob = 0
        gen_prob = gen.next_float() * max_prob
        for i in range(len(probs)):
            cur_prob += probs[i]
            if cur_prob >= gen_prob:
                return i


"""if __name__ == '__main__':
    d = Discrete2D(10, 10)
    emp = d.get_emp(1000)
    print(emp)"""


def randomvariate(pdf, n=1000, xmin=0, xmax=1):
    """
   Rejection method for random number generation
   ===============================================
   Uses the rejection method for generating random numbers derived from an arbitrary
   probability distribution. For reference, see Bevington's book, page 84. Based on
   rejection*.py.

   Usage:
   >>> randomvariate(P,N,xmin,xmax)
    where
    P : probability distribution function from which you want to generate random numbers
    N : desired number of random values
    xmin,xmax : range of random numbers desired

   Returns:
    the sequence (ran,ntrials) where
     ran : array of shape N with the random variates that follow the input P
     ntrials : number of trials the code needed to achieve N

   Here is the algorithm:
   - generate x' in the desired range
   - generate y' between Pmin and Pmax (Pmax is the maximal value of your pdf)
   - if y'<P(x') accept x', otherwise reject
   - repeat until desired number is achieved

   Rodrigo Nemmen
   Nov. 2011
    """
    # Calculates the minimal and maximum values of the PDF in the desired
    # interval. The rejection method needs these values in order to work
    # properly.
    x = np.linspace(xmin, xmax, 1000)
    y = pdf(x)
    pmin = 0.
    pmax = y.max()

    # Counters
    naccept = 0
    ntrial = 0

    # Keeps generating numbers until we achieve the desired n
    ran = []  # output list of random numbers
    while naccept < n:
        x = np.random.uniform(xmin, xmax)  # x'
        y = np.random.uniform(pmin, pmax)  # y'

        if y < pdf(x):
            ran.append(x)
            naccept = naccept + 1
        ntrial = ntrial + 1

    ran = np.asarray(ran)

    return ran, ntrial




import math
from random import uniform
from matplotlib import pyplot as plt



def f(x, y):
    return (3 / (8 * math.pi)) * (2 - math.sqrt(x**2 + y**2))


def generate(n=100000):
    x_es = []
    y_s = []
    while len(x_es) < n:
        x = uniform(-2, 2)
        y = uniform(-2, 2)
        if x**2 + y**2 > 4:
            continue
        z = uniform(0, 3 / (4 * math.pi)) # 2-nd value is max(f(x, y)), when sqrt = 1
        if z < f(x, y):
            x_es.append(x)
            y_s.append(y)

    return x_es, y_s


if __name__ == '__main__':
    #x, y = generate()
    rng = np.arange(-6, 6, 0.1)
    X, Y = np.meshgrid(rng, rng)
    Z = (3 / (8 * math.pi)) * (2 - np.sqrt(X**2 + Y**2))
    print(Z)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z)
    plt.show()

    """fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    hist, xedges, yedges = np.histogram2d(x, y, bins=70, normed=True)

    xpos, ypos = np.meshgrid(xedges[:-1], yedges[:-1])
    xpos = xpos.ravel()
    ypos = ypos.ravel()
    zpos = 0

    dx = dy = np.ones_like(zpos)
    dz = hist.ravel()

    ax.bar3d(xpos, ypos, zpos, dx, dy, dz)

    plt.show()

    plt.hist2d(x, y, bins=70, density=True)
    plt.colorbar()
    plt.show()
"""