from queue import Queue, Full
import random
import threading
import time
from ziggurat import Exponential

start_time = time.time()

def up_number():
    i = 0
    while True:
        yield i
        i += 1

class Order:
    def __init__(self, number):
        self.number = number


class SpammerTester:
    def __init__(self, _lambda, n):
        self._lambda = _lambda # intesivity of events
        self.n = n # serving threads
        self.thread_count_lock = threading.Lock()
        self.current_running_threads = 0
        self.thrown_list = []
        self.thrown_list_lock = threading.Lock()
        self.exp_time_gen = Exponential()

    def test_mass_service(self, p=0.5, events_remaining=100):
        enumerator = up_number()
        mass_system = MassServiceSystem(m=1, _lambda=self._lambda, p=p)
        self.current_runnung_threads = 0


        def run_one():
            mass_system.process_loop()

        def hold_loop():
            while True:
                for i in range(len(threads)):
                    if not threads[i].is_alive():
                        threads[i] = threading.Thread(target=run_one)
                        threads[i].start()

                if events_remaining == 0:
                    break

                time.sleep(0)

        #events_remaining = 100

        threads = []
        for i in range(self.n):
            threads.append(threading.Thread(target=run_one))

        holder = threading.Thread(target=hold_loop)
        holder.start()

        while True:
            if events_remaining == 0:
                break

            order = Order(next(enumerator))

            try:
                mass_system.queue.put_nowait(order)
            except Full:
                with self.thrown_list_lock:
                    self.thrown_list.append((order.number, time.time() - start_time))
                print('order № {} thrown away because queue is full'.format(order.number))

            events_remaining -= 1


            for i in range(len(threads)):
                if not threads[i].is_alive():
                    threads[i] = threading.Thread(target=run_one)
                    threads[i].start()

            time.sleep(self.exp_time_gen.next(self._lambda) / 100)

        holder.join()

        for thread in threads:
            thread.join()



        print('processed:')
        print(mass_system.processed_list)

        print('requeued:')
        print(mass_system.requeued_list)

        print('thrown away:')
        print(self.thrown_list)

        return mass_system.processed_list, mass_system.requeued_list, self.thrown_list


class MassServiceSystem:
    def __init__(self, m, _lambda, p):
        assert 0 <= p <= 1
        self.m = m # places in Queue
        self._lambda = _lambda  # intesivity of events. ticks to wait
        self.p = p # possibility to fulfill the order
        self.q = 1 - p
        self.queue = Queue(maxsize=m)
        self.random = random.uniform

        # events
        self.processed_list = []
        self.processed_list_lock = threading.Lock()
        self.requeued_list = []
        self.requeued_list_lock = threading.Lock()

    def process_loop(self):
        while not self.queue.empty():
            order = self.queue.get()
            self.try_process(order)
            time.sleep(1)#(self._lambda)


    def try_process(self, order: Order):
        processed = self.random(0,1) < self.p
        if processed:
            with self.processed_list_lock:
                self.processed_list.append((order.number, time.time() - start_time))
            print('order № {} processed'.format(order.number))
            return
        else:
            with self.requeued_list_lock:
                self.requeued_list.append((order.number, time.time() - start_time))
            self.queue.put(order)
            print('order № {} pushed in queue'.format(order.number))


import matplotlib.pyplot as plt

if __name__ == '__main__':
    '''s = Exponential()
    print(s.next(0.1))
    print(s.next(0.1))
    print(s.next(0.1))
    print(s.next(0.1))
    print(s.next(0.1))
    print(s.next(0.1))
    print(s.next(0.1))
    exit(0)'''
    print(time.time())
    tester = SpammerTester(0.1, 4)
    results = tester.test_mass_service()

    ta = []
    for i in results[2]:
        ta.append(i[1])

    req = []
    for i in results[1]:
        req.append(i[1])

    proc = []
    for i in results[0]:
        proc.append(i[1])

    if ta:
        plt.hist(ta, density=True)
        plt.show()

    if req:
        plt.hist(req, density=True)
        plt.show()

    if proc:
        plt.hist(proc, density=True)
        plt.show()

    '''q.put_nowait(1)
    q.put_nowait(2)'''