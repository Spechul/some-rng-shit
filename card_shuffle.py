import random
import basic_rand

EPSILON = 0.01
SCALE = 100000


class CardShuffleGenerator:
    def __init__(self, symbols: list, possibilities: list, default_seed=1):
        self.seed = default_seed
        self.rand = random.Random(self.seed)
        if len(symbols) != len(possibilities):
            raise ValueError('pass two arrays with the same length')
        s = sum(possibilities)
        if s < 1 - EPSILON or s > 1 + EPSILON:
            raise ValueError('possibilities sum is not equal to 1')

        self.cards = []
        self.posittion = 0
        for i in range(len(possibilities)): # ain't matter if len(symbols)
            number = possibilities[i] * SCALE
            int_num = int(number)
            for j in range(int_num):
                self.cards.append(symbols[i])

        self.rand.shuffle(self.cards)

    def next(self):
        ret = self.cards[self.posittion]
        self.posittion += 1
        if self.posittion > len(self.cards) - 1:
            self.rand.shuffle(self.cards)
            self.posittion = 0

        return ret


class CardShuffleChoice:
    def __init__(self, symbols: list, possibilities: list, default_seed=1, default_scale=SCALE):
        self.seed = default_seed
        self.rand = basic_rand.LCG(seed=default_seed)
        if len(symbols) != len(possibilities):
            raise ValueError('pass two arrays with the same length')
        s = sum(possibilities)
        if s < 1 - EPSILON or s > 1 + EPSILON:
            pass
            raise ValueError('possibilities sum is not equal to 1', str(s))

        self.cards = []
        self.posittion = 0
        for i in range(len(possibilities)):  # ain't matter if len(symbols)
            number = possibilities[i] * default_scale
            int_num = int(number)
            for j in range(int_num):
                self.cards.append(symbols[i])

    def next(self):
        index = self.rand.next() % len(self.cards)

        return self.cards[index]


class CardShuffle2D:
    def __init__(self, symbols_x: list, symbols_y: list, possibilities: list, seed=1, scale=SCALE):
        self.seed = seed
        self.rand = basic_rand.LCG(seed=seed)
        self.cards = [[0] * scale for i in range(scale)]
        if len(symbols_x) != len(symbols_y) != len(possibilities):
            raise ValueError('please, pass arrays of same length')
        s = sum(possibilities)
        if s < 1 - EPSILON or s > 1 + EPSILON:
            raise ValueError('possibilities sum is not equal to 1', str(s))

        for i in range(len(possibilities)):
            for j in range(len(possibilities)):
                possibilities[i][j] *= scale


def go_up(i, j):
    return i - 1, j


def go_down(i, j):
    return i + 1, j, 'i'


def go_left(i, j):
    return i, j - 1


def go_right(i, j):
    return i, j + 1, 'j'


def print_and_count(var):
    print(var)
    return 1


def gay_bypass(ls: list, action=print_and_count):
    i = 0
    j = 0
    count = 0
    inc = 'i'
    count += action(ls[i][j])
    i, j, inc = go_right(i, j)
    count += action(ls[i][j])
    i, j, inc = go_down(i, j)
    count += action(ls[i][j])
    i, j = go_left(i, j)
    count += action(ls[i][j])
    max_j = 1
    max_i = 1
    while count < len(ls)**2:
        if j == 0:
            i, j, inc = go_down(i, j)
            count += action(ls[i][j])
            while j < max_i + 1:
                i, j, inc = go_right(i, j)
                count += action(ls[i][j])

        elif i == 0:
            i, j, inc = go_right(i, j)
            count += action(ls[i][j])
            while i < max_j + 1:
                i, j, inc = go_down(i, j)
                count += action(ls[i][j])

        elif i == j:
            if inc == 'i':
                while j > 0:
                    i, j = go_left(i, j)
                    count += action(ls[i][j])
                #i, j, inc = go_down(i, j)
                #count += action(ls[i][j])

            elif inc == 'j':
                while i > 0:
                    i, j = go_up(i, j)
                    count += action(ls[i][j])
                #i, j, inc = go_right(i, j)
                #count += action(ls[i][j])
        max_i = max(max_i, i)
        max_j = max(max_j, j)

if __name__ == '__main__':
    ls = [
        [1,  2,  3,  4,  5],
        [6,  7,  8,  9,  10],
        [11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20],
        [21, 22, 23, 24, 25]
    ]
    gay_bypass(ls)
    pass
'''1 2 5 4 7 8 9 6 3'''
