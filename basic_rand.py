import random
import ctypes

RAND_MIN = 0
RAND_MAX = 0xffffffffffffffff


class BasicRand:
    def __init__(self, seed=1):
        self.rand = random.Random(seed)

    def next(self):
        return self.rand.randint(RAND_MIN, RAND_MAX)


# https://en.wikipedia.org/wiki/Linear_congruential_generator common values list are in the table
class LCG:
    def __init__(self, a=1664525, c=1013904223, modulus=RAND_MAX, seed=1):
        self.a = a
        self.c = c
        self.modulus = modulus
        self.seed = seed

    def next(self):
        self.seed = (self.a * self.seed + self.c) % self.modulus
        return self.seed

    def next_float(self):
        return self.next() % ctypes.c_ulong(self.modulus).value / ctypes.c_ulong(self.modulus).value

"""l = LCG()
for i in range(100):
    print(l.next_float())"""