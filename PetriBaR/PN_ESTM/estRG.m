function [EstR,yr]=estRG(R,E,L)
% This function is to conpute the initial-state estimator of the input RG.
%  R is the input RG obtained by function graphPN in PN_BASIC.
%  Lmat is the labeling function, a (|E|+1) x |T| matrix;
%      Each row stands for a label and each column stands for
%      a transition, the first row corresponds to the
%      unobservable event.
%  E is the alphabet, a row vector of strings, e.g. E=['a','b'] or E='ab'.
%  Re={n,E,f,x0} is the initial-state estimator.

%% Convert the labeling funciton
sizeR1=size(R,1);
n=sizeR1-1;% The number of states.
nt=R(sizeR1,2);%The number of transitions.
np=R(sizeR1,1);%The number of places.

nl=size(E,1); % the number of labels
Emat=[''];
Lmat=zeros(nl+1,nt);
for i=1:nl
    Emat(i)=E{i}(1);
    Lmat(i+1,L{i})=1;
end

for i=1:nt
    if Lmat(:,i)==zeros(nl+1,1)
        Lmat(1,i)=1;
    end
end

%% Convert the RG into an NFA
Gr=RG2Auto(R,Emat,Lmat);

%% Change the initial-state from M0 to R(N,M0), the set of markings.
 Gr{4}=0:Gr{1}-1;

%% Construct the reverse automaton of Gb
Grr=Revers(Gr);

%% Construct the observer of Gbr
[EstR,yr]=NFA2DFA(Grr);

end