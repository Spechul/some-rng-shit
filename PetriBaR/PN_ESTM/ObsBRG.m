function [ObsB,yb]=ObsBRG(B)
% This function is used to construct the observer of a BRG
% T - the BRG
% Obs - the observer has the same form as the output observer of function NFA2DFA
% y - the set of states of the BRG that have been emerged as a state in the
% observer

NFA=Conv_L(B);

[ObsB,yb]=NFA2DFA(NFA);
end

% Copyright Yin Tong