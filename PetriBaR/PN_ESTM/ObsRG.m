function [ObsR,yr]=ObsRG(R,L,E)

% This function computes the observer for the RG
%        
% *****************************************************************
%                               ## SYNTAX ##
%  R -- the reachability graph output by using function graphPN;
%  Lmat -- labeling function, a (|E|+1) x |T| matrix; Each row stands for a
%       label and each column stands for a transition, the first row 
%       corresponds to the unobservable event.
%       For example, if l(t1)=\varepsilon then L(1,1)=1.
%  Emat -- alphabet, a row vector of strings, e.g. E=['a','b'] or E='ab'.
%
%
% see also: graph, NFA2DFA
%

%tic;
sizeR=size(R,1);
%n=sizeR-1;% The number of states.


%p=R(sizeR,1);%The number of places.
nt=R(sizeR,2);%The number of transitions.

nl=size(E,1);
Emat=[''];
Lmat=zeros(nl+1,nt);
for i=1:nl
    Emat(i)=E{i}(1);
    Lmat(i+1,L{i})=1;
end

for i=1:nt
    if Lmat(:,i)==zeros(nl+1,1)
        Lmat(1,i)=1;
    end
end

%% convert the RG to an NFA form
NFA=RG2Auto(R,Emat,Lmat);

% R(:,1:p+2)=[];
% R(sizeR,:)=[];%Remove columns and rows not needed.
% [sizeR,lengthR]=size(R);% Update the size of G
% 
% x0=0; % Initial state of the NFA corresponds to the initial marking M1.
% Xm=[]; % The set of final states is empty.
% 
% %Convert the reachability graph obtained by function
% % graph into the format required by function NFA2DFA
% 
% %  Compute transitions
% delta=[];
% row=1;
% for m=1:n
%     for c=1:2:lengthR
%         t=R(m,c);
%         if t==0
%             continue;
%         else
%             [l,tr]=find(Lmat(:,t)==1);
%             lab=l-1;
%             delta(row,:)=[m-1 lab R(m,c+1)-1];
%             row=row+1;
%         end
%     end
% end
% NFA={n,Emat,delta,x0,Xm};

%% Compute the observer
[ObsR,yr]=NFA2DFA(NFA);

end

% Copyright Yin Tong