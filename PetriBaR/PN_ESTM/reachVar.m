function [XR0]=reachVar(XF0,f)
%This function computes the set of states XR0 reached from states XF0 by
%excuting zero or more than one unobservable events.
% XR0 -- a row vector denoting the reached states
% XF0 -- a row vector denoting the departure states
% T -- a matrix denoting the transition funciton

f=sortrows(f,[1,2,3]);
[m,n]=size(XF0); %XF0 is the set of states that have not been analyzed
XR0=XF0;
X=[]; %X is the set of states that have been analyzed

while n~=0 %XF0 is not empty
    for i=1:n
        x=XF0(i);
        loc=intersect(find(f(:,1)==x),find(f(:,2)==0));
        l=size(loc,1);
        if ~isempty(loc)
            for j=1:l
                XR0=union(f(loc(j),3),XR0);
            end
        end
        X=union(X,x);
    end
    XF0=setdiff(XR0,X); %remove X from XF0
    [m,n]=size(XF0);
end



end

% Copyright Yin Tong