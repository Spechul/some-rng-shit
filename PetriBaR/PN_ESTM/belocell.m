function [bel,loc]=belocell(x,C)
% This function is used to check if vector x is an element of C, where C is a
% cell whose elements are vectors
% bel=1, if x is an element of C; otherwise, bel=0
% loc records the position of vectior x in C; if bel=0, loc=0.

n=size(C,2);
leng=size(x,2);% compute the length of x
bel=0;
loc=0;

for i=1:n
    xc=C{i};
    if leng==size(xc,2)
        v=x==xc;
        if v==ones(1,leng) % x=xc
            bel=1;
            loc=i;
            break; %break the comparison
        end
    end
end
end

% Copyright Yin Tong, Oct. 2014.