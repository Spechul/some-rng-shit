function [XR]=reachE(XF,e,f)
% This function conputes the set of states XR reahced from x in XF when event e occurs
% XF -- a vctor denoting the set of departure states
% XR -- a vector denoting the set of reached states
% e -- the observable event
% f -- a matrix denoting the transition funciton

f=sortrows(f,[1,2,3]);
[m,n]=size(XF);
XR=[];
X=[]; %set of states that have been analyzied

for i=1:n
    x=XF(i);
    loc=intersect(find(f(:,1)==x),find(f(:,2)==e));% comoute the unobservable events that are enabled at x
    l=size(loc,1);
    if ~isempty(loc)
        %compute the reachable states from x by events in loc
        for j=1:l
            XR=union(f(loc(j),3),XR);
        end
    end
end


end

% Copyright Yin Tong