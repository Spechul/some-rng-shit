% This funtion is to compute the observer corresponding to
% the RG and BRG of the LPN with different value of parameter k. To compute the observer of the RG and the BRG, respectively,
% the name of the output parameter of the function should be changed. 

function [Cw]=ExampleCur%(k) % BRG


%% k is the number of tokens in place p2.
%% Re is the initial-state estimator of the RG.
%% Obs is the initial-state estimator of the BRG.

% The net structure.
Post=[0 0 0 1 1;1 0 0 0 0;0 1 0 0 0;0 0 1 0 0];
Pre=[1 0 0 0 0;0 1 0 0 0;0 0 1 1 0;0 0 0 0 1];
% Pre=[1 0 0 0; 0 1 0 0; 0 0 1 1; 0 0 0 0];
% Post=[0 0 0 1; 1 0 0 0; 0 1 0 0; 0 0 1 0];
M0=[1 0 0 0]';
% M0=[1 1 0 0]';
%M0(2)=k;
E={['a'];['b']};L={[1];[3]}; %%%% Lmat is supposed to be computed based on E and L.
%Lmat=[0 1 0 1 1;1 0 1 0 0];
W=[-1 0 0 -1];
K=-1;
S={W;K};
w='aab';
[Cw]=CurEst_RG(Pre,Post,M0,L,E,w);
%[Cbw]=CurEst_BRG(Pre,Post,M0,L,E,w);

% Compute the BRG for opacity
% T=BRG_Cur(Pre,Post,M0,S,L,E);

%Compute the observer for the BRG
% tic;
% [Obs,y]=ObsBRG(T);
% NumObs=Obs{1,1};

% % Compute the R
%R=RG(Pre, Post, M0);
% tic;
% % Convert the RG into an automaton
%[ObsR,y]=ObsRG(R,L,E);
% 
% % Compute the observer of the RG
% Obs=NFA2DFA(R);
% NumObsR=ObsR(1);

end