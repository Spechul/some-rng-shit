%function [Cbw]=CurEst_BRG(Pre,Post,M0,L,E,w)
function [Cbw]=CurEst_BRG(B,L,E,w)
%% compute the BRG
%[B] =BRG_L(Pre,Post,M0,L,E);

%% compute the observer
NFA=Conv_L(B);

[ObsB,yb]=NFA2DFA(NFA);

%% compute the state reached by w
nl=size(E,1); % the number of labels
Emat=[''];

for i=1:nl
    Emat(i)=E{i}(1);
end

f=ObsB{3}; % transition relation of the observer
start1=0; % the initial state is 0=f(1,1)
length=size(w,2); % the length of the word
if length~=0
    for i=1:length
        loc=find(Emat==w(i));
        row1=find(f(:,1)==start1);
        row2=find(f(:,2)==loc);
        row=intersect(row1,row2);
        start2=f(row,3);
        start1=start2;
    end
    endstate=start2+1;
else
    endstate=1;
end

CbwID=yb{endstate};

nc=size(CbwID,2);
Cbw={};
for i=1:nc
    Cbw{i}=B{CbwID(i)+1,2};
end
end