function [NFA]=Conv_L(T)
% This function is used to convert a BRG obtained by using function BRG_L
% into an NFA. Then to compute the observer one can use funtion NFA2DFA directly.
% T - the BRG of a PN
% NFA - a NFA has the form NFA={n,E,f,x0,Xm} as the input of function NFA2DFA.

%Initialize
n=size(T,1); % The number of rows of T is equal to the number of basis markings.
x0=0;
Xm=[];
sizeE=size(T{1,7},2);%The number of events.
E='';
f=[];

%Generate a string E='12...' with the same length of sizeE
Emat=1:sizeE;
for i=1:sizeE
    s=num2str(Emat(i));
    E=strcat(E,s);
end

%Constructe the transition matrix
t=0;
for i=1:n %For each row to compute the corresponding transition matrix.
    Tmax=size(T{i,7},1);%The maximal number of enabled transitions
    for j=1:sizeE % Compute the number of enabled observable transtions
        for k=1:Tmax
            if ~isempty(T{i,7}{k,j})
                t=t+1;
                f(t,1)=i-1;
                f(t,2)=j;
                f(t,3)=T{i,7}{k,j}{1,3}{1,1}-1;
            end
        end
    end
end

NFA={n,E,f,x0,Xm};

end

% Copyright Yin Tong