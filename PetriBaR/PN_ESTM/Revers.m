function [Gr]=Revers(G)
% This function is used to construct the reverse automaton of the input
% one.
%% G is the input automaton.
%% Gr={n,E,f,x0,Xm} is the reverse autoaton of G.

n=G{1};
E=G{2};
x0=G{4};
Xm=G{5};

% Reverse the transition relation.
f=G{3};
temp=f(:,1);
f(:,1)=f(:,3);
f(:,3)=temp;
Gr={n,E,f,x0,Xm};
end