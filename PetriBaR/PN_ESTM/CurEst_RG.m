%function [Cw]=CurEst_RG(Pre,Post,M0,L,E,w)
function [Cw]=CurEst_RG(Pre,Post,M0,R,L,E,w)
% Given an LPN system (Pre,Post,M0,L,E) and an observation w\in E^*, the
% function computes the set Cw of markings consistent with the given
% observation w
% w is a string arrary, e.g., w='acd'

np=size(Pre,1); % no. of places

%% compute the RG
%R=graphPN(Pre,Post,M0);

%% compute the observer of the RG
[ObsR,yr]=ObsRG(R,L,E);

nl=size(E,1); % the number of labels
Emat=[''];

for i=1:nl
    Emat(i)=E{i}(1);
end

%% compute the state reached in the observer by w
f=ObsR{3}; % transition relation of the observer
start1=0; % the initial state is 0=f(1,1)
length=size(w,2); % the length of the word
if length~=0
    for i=1:length
        loc=find(Emat==w(i));
        row1=find(f(:,1)==start1);
        row2=find(f(:,2)==loc);
        row=intersect(row1,row2);
        start2=f(row,3);
        start1=start2;
    end
    endstate=start2+1;
else
    endstate=1;
end

CwID=yr{endstate};

nc=size(CwID,2);
Cw={};
for i=1:nc
    Cw{i}=R(CwID(i)+1,1:np)';
end

end

% @Copyright Yin Tong, 2016.