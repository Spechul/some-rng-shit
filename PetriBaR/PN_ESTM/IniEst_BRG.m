%function [Ibw]=IniEst_BRG(Pre,Post,M0,L,E,w)
function [Ibw]=IniEst_BRG(Pre,Post,M0,B,L,E,w)
%this funciton computes the set of markings from which the observation w
%could be generated using the BRG

%% compute the BRG
%[B] =BRG_L(Pre,Post,M0,L,E);

%% compute the estimator
[EstB,yb]=estBRG(B);

%% compute the state reached by w
np=size(Pre,1);
nl=size(E,1); % the number of labels
Emat=[''];

for i=1:nl
    Emat(i)=E{i}(1);
end

f=EstB{3}; % transition relation of the observer
start1=0; % the initial state is 0=f(1,1)
length=size(w,2); % the length of the word

% reverse the osbervation w to wr
wr='';
for i=1:length
    wr(i)=w(length+1-i);
end
% compute the state reached by wr in the estimator
if length~=0
    for i=1:length
        loc=find(Emat==wr(i));
        row1=find(f(:,1)==start1);
        row2=find(f(:,2)==loc);
        row=intersect(row1,row2);
        start2=f(row,3);
        start1=start2;
    end
    endstate=start2+1;
else
    endstate=1;
end

IwID=yb{endstate};

nc=size(IwID,2);
Ibw={};
for i=1:nc
    Ibw{i}=B{IwID(i)+1,2};
end
end