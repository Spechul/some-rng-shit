function [Gr]=RG2Auto(R,Emat,Lmat)
% This function converts the reachability graph computed by function graph
% into an automaton.
%% R is the output of function graph
%% Lmat is the labeling function, a (|E|+1) x |T| matrix;
%%      Each row stands for a label and each column stands for
%%      a transition, the first row corresponds to the
%%      unobservable event.
%% E is the alphabet, a row vector of strings, e.g. E=['a','b'] or E='ab'.
%% GR is the corresponding automaton of R

sizeR=size(R,1);
n=sizeR-1;% The number of states.

p=R(sizeR,1);%The number of places.
%t=R(sizeR,2);%The number of transitions.

R(:,1:p+2)=[];
R(sizeR,:)=[];%Remove columns and rows not needed.
lengthR=size(R,2);% Update the size of R

x0=0; % Initial state of the NFA corresponds to the initial marking M1.
Xm=[]; % The set of final states is empty.

%Convert the reachability graph obtained by function
% graph into the format required by function NFA2DFA

%  Compute transitions
delta=[];
row=1;
for m=1:n
    for c=1:2:lengthR
        t=R(m,c);
        if t==0
            continue;
        else
            [l,tr]=find(Lmat(:,t)==1);
            lab=l-1;
            delta(row,:)=[m-1 lab R(m,c+1)-1];
            row=row+1;
        end
    end
end
% Construct the corresponding NFA
%NFA=af(n,Emat,delta,x0,Xm); %There is no need to use function af
Gr={n,Emat,delta,x0,Xm};
end

% @Copyright Yin Tong, 2016