function [EstB,yb]=estBRG(B)
% This function is to conpute the initial-state estimator of the input BRG.
%  B is the input BRG obtained by function BRG.
%  Be={n,E,f,x0} is the initial-state estimator.

%% Convert the BRG into an automaton
%Gb=BRG2Auto(B);
Gb=Conv_L(B);

%% Change the initial-state from M0 to M_B, the set of all basis markings.
Gb{4}=0:Gb{1}-1;

%% Construct the reverse automaton of Gb
Gbr=Revers(Gb);

%% Construct the observer of Gbr
[EstB,yb]=NFA2DFA(Gbr);

end