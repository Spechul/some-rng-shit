function [Gd,y]=NFA2DFA(Gn)
% This function computes the equivalent DFA Gd of a given NFA Gn
% Gn -- the NFA has the following form
%%%  Gn={n,E,f,X0,Xm}
%%%  n -- the number of states
%%%  E -- a string denotes the alphabet, e.g. E='ab'
%%%  f -- a |f|x3 dimensional matrix that denotes the transiton function f, where |f| is the number of transitions. e.g, if i[j>k, then there is a row [i j k].  
%%%  x0 -- a row vector denoting the initial states
%%%  Xm -- a vector denoting the set of final states
% Gd -- the obtained equivalent DFA has the same form of Gn
% y -- a 1x|Gd| dimensional cell. Each element is a vector denoting the set
% of states of Gn that have been emerged as a state in Gd

%Initialize
T=Gn{3};
x0=Gn{4};
Xm=Gn{5};
xd0=reachVar(x0,T); % compute the initial state of the DFA
y{1}=xd0;
Xnew={xd0}; %Xnew records the new state that have not been analyzed
Xdm=[];
Td=[]; % The matrix denotes the transition function of Gd
Ed=Gn{2}; % The alphabet of Gd
le=size(Ed,2); % The number of observable events

c=size(Xnew,2);
i=1;

    while i<=c
        XF=Xnew{i};
        for j=1:le 
            XR=reachE(XF,j,T);
            XR0=reachVar(XR,T);
            if ~isempty(XR0)
            
                %check if XR0 is a new state
                [bel1,loc]=belocell(XR0,Xnew);

                if ~bel1
                    c=c+1;
                    Xnew{c}=XR0;
               
                    y{size(y,2)+1}=XR0;
                    
                    if ~isempty(intersect(XR0,Xm))
                        Xdm(size(Xdm,2)+1)=size(y,2);
                    end
                    % compute the transition function matrix
                    Td(size(Td,1)+1,:)=[i-1,j,size(y,2)-1];
                else
                    Td(size(Td,1)+1,:)=[i-1,j,loc-1];
                end
            end
        end
        c=size(Xnew,2);
        i=i+1;
    end
%end
nd=size(Xnew,2); % The number of states of the observer.
Gd={nd,Ed,Td,xd0,Xdm};

end

% Copyright Yin Tong