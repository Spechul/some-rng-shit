function [Pre,Post,M0] = geneMatrix(this)
Pre = zeros(this.pNum,this.tNum);
Post = zeros(this.pNum,this.tNum);
M0 = zeros(this.pNum,1);

for i=1:this.pNum
        M0(i,1) = this.places(i).tokenNum;
end


for i=1:this.tNum
    placePoints = this.transitions(i).sour;%可能有多个
    [arcNum,~] = size(placePoints);
    for j=1:arcNum
        placePoint = placePoints(j,:);
        if(~isempty(placePoint))
            pIndex = findPlaces(this,placePoint);
            if(pIndex~=0)
                Pre(pIndex,i) = this.transitions(i).sourArrow(j);
            end
            
        end
    end
end


for i=1:this.tNum
    placePoints = this.transitions(i).tar;
    [arcNum,~] = size(placePoints);
    for j=1:arcNum
        placePoint = placePoints(j,:);
        if(~isempty(placePoint))
            pIndex = findPlaces(this,placePoint);
            if(pIndex~=0)
                Post(pIndex,i) = this.transitions(i).tarArrow(j);
            end
            
        end
    end
    
end

end



