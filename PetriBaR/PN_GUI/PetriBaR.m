classdef PetriBaR < handle
    
    
    %% Properties
    properties (SetAccess = private)
        % structure containing graphics handles
        handles;
        % place data
        places;
        % transition data
        transitions;
        % num of places
        pNum=0;
        % num of transitions
        tNum=0;
        % the point of last click
        lPoint;
        %save calculation variables(Te,G,T,BRG,...)
        variables;
    end
    
    properties (Access = private)
        % app state,whether place button, transition button,
        % or arc button is selected
        isPlace = false;
        
        isTransition = false;
        
        isArc = false;
        
    end
    
    properties (Access = private, Constant = true)
        % data domain for both X/Y dimensions
        DOM = [0 10];
    end
    
    %% Constructor
    methods
        function this = PetriBaR()
            % initialize UI
            createGUI(this);
            
            % generate data
            genData(this);
            
            % reset ui
            reset(this);
        end
        
        
    end
    
    %% Private Methods
    methods (Access = private)
        function genData(this)
            %GENDATA  Generate data which used by UI
            this.isPlace = false;
            this.isTransition = false;
            this.isArc = false;
            this.lPoint = [];
            
            %initialize intermediate variables Graph, Tree, BRG, BRD_L...
            resetIntermediateVariables(this);
            %Input parameters
            this.variables.Te = [];
            this.variables.M = [];
            this.variables.K = [];
            this.variables.u = [];
            this.variables.F = [];
            this.variables.w = '';
            this.variables.W = [];
            
        end
        
        function reset(this)
            %RESET  Reset UI state
            
            % reset variables: isPlace, isTransition, isArc, lPoint
			% reset hBtnText(show current draw mode), hArcText(show first click in arc mode)
            this.isPlace = false;
            this.isTransition = false;
            this.isArc = false;
            this.lPoint = [];
            set(this.handles.hBtnText,'String','');
            set(this.handles.hArcText,'String','');
            drawnow();
        end
        
        function resetIntermediateVariables(this)
            %Intermediate variables
            this.variables.BRG = [];
            this.variables.BRG_L = [];
            this.variables.BRG_D = [];
            this.variables.BRG_Cur = [];
            this.variables.T = [];
            this.variables.G = [];
            this.variables.MBRG =[];
            this.variables.BRD = [];
        end
        
        function resetLastArc(this)
            % When current click is illegal,reset lPoint 
			this.lPoint = [];
            set(this.handles.hArcText,'String','');
            drawnow();
        end
        
    end
    
    %% UI
    methods (Access = private)
        function createGUI(this)
            %CREATEGUI  Build the UI
            
            % main figure
            hFig = figure('Menubar','none', 'Toolbar','none', ...
                'NumberTitle','off', 'Name','PetriBaR', ...
                'Resize','off', 'Visible','off', ...
                'Units','pixels', 'Position',[100 100 550 630]);
            
            this.handles = struct();
            this.handles.hFig = hFig;
            
            % create panels
            hPan = createGUI_Panels(this, hFig);
            
            createGUI_PanelHeader(this, hPan(1));
            createGUI_PanelOutput(this, hPan(2));
            
            % setup event handlers
            registerCallbacks(this);
            % create menu and setup menu handlers
            createGUI_Menu_registerCallbacks(this, hFig);
            % make figure visible
            set(hFig, 'Visible','on');
        end
        
        function hPan = createGUI_Panels(~, hParent)
            %CREATEGUI_PANELS  Create the layout of the top-level panels
            
            % panel properties
            props = {'ForegroundColor',0.3*[1 1 1]};
            titles = {'', 'Petri Net'};
            pos = {[5 565 525 60],  [5 5 525 550]};
            
            % panels
            hPan = gobj(1, numel(titles));
            for i=1:numel(titles)
                hPan(i) = uipanel('Parent',hParent, 'Title',titles{i}, ...
                    props{:}, 'Units','pixels', 'Position',pos{i},'Fontsize',10);
            end
        end
        %% create menu gui and register callback function
        function createGUI_Menu_registerCallbacks(this, ~)
            
            hMenuFile = uimenu('label','File','position',1);
            hMenuBasic = uimenu('label','Basic Analysis','position',2);
            hMenuMonit = uimenu('label','Monitor Design','position',3);
            hMenuReach = uimenu('label','Reachability','position',4);
            hMenuEstm = uimenu('label','State Estimation','position',5);
            hMenuDiag = uimenu('label','Diagnosis','position',6);
            hMenuOpac = uimenu('label','Opacity','position',7);
            %% FILE
            uimenu(hMenuFile,'label','Open','callback',@this.onMenuFile);
            uimenu(hMenuFile,'label','Save','callback',@this.onMenuFile);
            %% PN_BASIC
            uimenu(hMenuBasic,'label','Compute the RG','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Show the RG','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Compute the RT/CT','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Show the RT/CT','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Step-by-step Simulation','callback',@this.onMenuBasic);
            
            uimenu(hMenuBasic,'label','Boundedness','Separator','on','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Liveness','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Deadlock Markings','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Firability','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Reachability','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Reversibility','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Repetitiveness','callback',@this.onMenuBasic);
            
            uimenu(hMenuBasic,'label','P-invariants','Separator','on','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','T-invariants','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Traps','callback',@this.onMenuBasic);
            uimenu(hMenuBasic,'label','Siphons','callback',@this.onMenuBasic);
            
            %% PN_MONIT
            uimenu(hMenuMonit,'label','Input the GMECs','callback',@this.onMenuMonit);
            uimenu(hMenuMonit,'label','Controllability','callback',@this.onMenuMonit);
            uimenu(hMenuMonit,'label','Compute Controllable GMECs','callback',@this.onMenuMonit);
            uimenu(hMenuMonit,'label','Monitor Places','callback',@this.onMenuMonit);
            uimenu(hMenuMonit,'label','Design the Closed-loop System','callback',@this.onMenuMonit);
            %% PN_REACH
            uimenu(hMenuReach,'label','Compute the BRG','callback',@this.onMenuReach);
            uimenu(hMenuReach,'label','Show the BRG','callback',@this.onMenuReach);
            uimenu(hMenuReach,'label','Reachability Given Te','callback',@this.onMenuReach);
            uimenu(hMenuReach,'label','Compute Explicit Transitions Te','callback',@this.onMenuReach);
            uimenu(hMenuReach,'label','Reachability without Te','callback',@this.onMenuReach);
            %% PN_ESTM
            
            uimenu(hMenuEstm,'label','Observer of the RG','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Initial-State Estimator of the RG','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Input Observation w','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Current-State Estimation (RG-Based)','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Initial-State Estimation (RG-Based)','callback',@this.onMenuEstm);
            
            uimenu(hMenuEstm,'label','Compute the BRG for an LPN','Separator','on','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Show the BRG','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Observer of the BRG','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Initial-State Estimator of the BRG','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Current-State Estimation (BRG-Based)','callback',@this.onMenuEstm);
            uimenu(hMenuEstm,'label','Initial-State Estimation (BRG-Based)','callback',@this.onMenuEstm);
            %% PN_DIAG
            uimenu(hMenuDiag,'label','Input Fault Classes','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Compute the BRG','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Show the BRG','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Compute the MBRG','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Show the MBRG','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Compute the BRD','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Show the BRD','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Diagnosability','callback',@this.onMenuDiag);
            uimenu(hMenuDiag,'label','Diagnosis','callback',@this.onMenuDiag);
            
            %% PN_OPAC
            uimenu(hMenuOpac,'label','Input a Secret','callback',@this.onMenuOpac);
            uimenu(hMenuOpac,'label','Compute the BRG for CSO','callback',@this.onMenuOpac);
            uimenu(hMenuOpac,'label','Show the BRG for CSO','callback',@this.onMenuOpac);
            uimenu(hMenuOpac,'label','Current-State Opacity','callback',@this.onMenuOpac);
            uimenu(hMenuOpac,'label','Initial-State Opacity','callback',@this.onMenuOpac);
            
        end
        
        
        function createGUI_PanelHeader(this, hParent)
            %CREATEGUI_PANELHEADER  Create the header panel UI
            
            % run/step/reset buttons
            hBtnPlace = uicontrol('Parent',hParent, 'Style','pushbutton', ...
                'String','Place', 'Units','pixels', 'Position',[10 10 80 30],'Fontsize',10);
            hBtnTransition = uicontrol('Parent',hParent, 'Style','pushbutton', ...
                'String','Transition', 'Units','pixels', 'Position',[90 10 80 30],'Fontsize',10);
            hBtnArc = uicontrol('Parent',hParent, 'Style','pushbutton', ...
                'String','Arc', 'Units','pixels', 'Position',[170 10 80 30],'Fontsize',10);
            
            hBtnReset = uicontrol('Parent',hParent, 'Style','pushbutton', ...
                'String','Clear', 'Units','pixels', 'Position',[280 10 80 30],'Fontsize',10);
            
            hBtnSaveMatrix = uicontrol('Parent',hParent, 'Style','pushbutton', ...
                'String','Incidence Matrix', 'Units','pixels', 'Position',[360 10 110 30],'Fontsize',10);
            
            % store handles
            this.handles.hBtnPlace = hBtnPlace;%Place mode
            this.handles.hBtnTransition = hBtnTransition;%Transition mode
            this.handles.hBtnArc = hBtnArc;%Arc mode
            this.handles.hBtnReset = hBtnReset;%Clear all
            this.handles.hBtnSaveMatrix = hBtnSaveMatrix;%Save Pre Post M0 matrix
            
        end
        
        function createGUI_PanelOutput(this, hParent)
            %CREATEGUI_PANELOUTPUT  Create the output panel UI
            
            % properties
            props = {'XColor',0.5*[1 1 1], 'YColor',0.5*[1 1 1], ...
                'FontSize',8, 'LineWidth',0.5, 'Box','off'};
            
            
            % axes out (image, colorbar, and scatters)
            hC = uicontainer(hParent, ...
                'Units','pixels', 'Position',[5 5 500 525]);
            hAxOut = axes('Parent',hC, props{:}, ...
                'XLim',this.DOM, 'YLim',this.DOM, 'CLim',[-1 1], ...
                'XTick',this.DOM(1):1:this.DOM(2), ...
                'YTick',this.DOM(1):1:this.DOM(2), ...
                'TickDir','out', 'YDir','normal', ...
                'XAxisLocation','top', 'YAxisLocation','right', ...
                'Units','pixels', 'Position',[20 20 450 450]);
            hBtnStaText = uicontrol('Parent',hC,'Style','text', 'Units', 'pixels', 'Position',[20 500 60 20],'FontSize',10,'String','Selected:');
            
            hBtnText = uicontrol('Parent',hC,'Style','text', 'Units', 'pixels', 'Position',[80 500 80 20],'FontSize',10);
            
            hArcStaText = uicontrol('Parent',hC,'Style','text', 'Units', 'pixels', 'Position',[250 500 150 20],'FontSize',10,'String','Start of the current arc:');
            
            hArcText = uicontrol('Parent',hC,'Style','text', 'Units', 'pixels', 'Position',[400 500 50 20],'FontSize',10);
            
            this.handles.hBtnText = hBtnText;% to show current mode
            this.handles.hArcText = hArcText;% to show the first selected primitive(place or transition)
            this.handles.hAxOut = hAxOut;% to show petri net
        end
        
        function registerCallbacks(this)
            %REGISTERCALLBACKS  Setup event handlers for UI components
            
            set(this.handles.hBtnPlace, 'Callback',@this.onPlace);
            set(this.handles.hBtnTransition, 'Callback',@this.onTransition);
            set(this.handles.hBtnArc, 'Callback',@this.onArc);
            
            
            
            set(this.handles.hAxOut,'ButtonDownFcn',@this.axeButtonDownFcn);
            
            set(this.handles.hBtnReset, 'Callback',@this.onReset);%%Clear
            
            set(this.handles.hBtnSaveMatrix, 'Callback',@this.onSave);
        end
    end
    
    %% UI Callbacks
    methods (Access = private)
        function onPlace(this, ~, ~)
            %ONPLACE  when place button is selected, set other buttons false
            
            this.isPlace = true;
            this.isTransition = false;
            this.isArc = false;
            set(this.handles.hBtnText,'String','Place');% show place mode
            drawnow();
        end
        
        function onTransition(this, ~, ~)
            %ONTRANSITION  When transition button is selected, set other button false
            
            this.isPlace = false;
            this.isTransition = true;
            this.isArc = false;
            
            set(this.handles.hBtnText,'String','Transition');% show transition mode
            drawnow();
        end
        
        function onArc(this, ~, ~)
            %ONARC  When arc button is selected, set other button false
            reset(this);
            this.isPlace = false;
            this.isTransition = false;
            this.isArc = true;
            
            set(this.handles.hBtnText,'String','Arc');% show arc mode
            drawnow();
        end
        
        function onReset(this, ~, ~)
		    % reset data
            this.pNum=0;
            this.tNum=0;
            this.places =[];
            this.transitions =[];
            
            %reset intermediate variables
            resetIntermediateVariables(this);
			%reset ui
            reset(this);
            % clear axes
            axes(this.handles.hAxOut);
            cla
        end
        
        function onSave(this, ~, ~)
		    % generate incident matrix
            [Pre,Post,M0]=geneMatrix(this);
			% get current running M-File Path
            currentMFilePath = mfilename('fullpath');
			% find '\' in path
            i=strfind(currentMFilePath,'\');
            currentMFilePath=currentMFilePath(1:i(end))
			% compose path ( result like ..\..\getMatrix.m)
            filePath =  fullfile(currentMFilePath, 'getMatrix.m');
            % open file 
			file = fopen(filePath,'wt');
			% output text
            fprintf(file,'function [Pre,Post,M0] = getMatrix()\n');
            % count num of row in matrix pre
			[row,~] =size(Pre);
            fprintf(file,'Pre = [\n');
			% define file format for output matrix(%d,%d,...,%d)
            fileformatSpecCell = cell (1,this.tNum);
            for i=1:this.tNum
                if(i==1)
                    fileformatSpecCell(1,i) = {'%d'};
                else
                    fileformatSpecCell(1,i) = {',%d'};
                end
            end
			% get row 1 in cell
            fileformatSpec = [fileformatSpecCell{1,:}];
            for i=1:row
                if(~isempty(fileformatSpec))
                    fprintf(file,fileformatSpec,Pre(i,:));
                end
                fprintf(file,';\n');
            end
            fprintf(file,']\n');
            % count num of row in matrix post
            [row,~] =size(Post);
            fprintf(file,'Post = [\n');
            for i=1:row
                if(~isempty(fileformatSpec))
                    fprintf(file,fileformatSpec,Post(i,:));
                end
                fprintf(file,';\n');
            end
            fprintf(file,']\n');
			% count num of row in matrix M0
            [row,~] =size(M0);
            fprintf(file,'M0 = [');
            for i=1:row
                fprintf(file,'%d',M0(i,:));
                if(i~=row)
                    fprintf(file,';');
                else
                    fprintf(file,']');
                end
            end
            fprintf(file,'\nend\n');
            fclose(file);
        end
        
        function axeButtonDownFcn(this, ~, ~)
            %this callback function is called when axe detects mouse click
            
            switch (get(gcbf,'SelectionType'))
                case 'normal'% click 
                    point=get(gca,'currentpoint');%get point
                    hold on
                    
                    x = floor(point(1));%convert to x,y
                    y = floor(point(3));
                    
                    drawEntity(this,x,y);% draw place, transition, arc
                case 'open'% double click
                    reset(this);
                    point=get(gca,'currentpoint');
                    hold on
                    
                    x = floor(point(1));
                    y = floor(point(3));
                    % if the area which is around the place was clicked
                    if(findPlaces(this,[x,y])~=0)
                        addToken(x,y,this);
                        % if the area which is around the transition was clicked
                    else if(findTransitions(this,[x,y])~=0)
                            retangleButtonDown(this);
                        end
                    end
                    
                case 'alt'% Right mouse click
					% reset ui and clear last click data
                    reset(this);
                otherwise
                    
            end
            
            drawnow();
        end
        
        function circleButtonDown(this,~,~)
            %this callback function is called when place(circle) detects mouse click
            switch (get(gcbf,'SelectionType'))
                case 'normal'% Click
                    point=get(gca,'currentpoint');% get current click point
                    hold on
                    
                    x = floor(point(1));
                    y = floor(point(3));
                    
                    if this.isArc% whether in arc mode
                        pIndex = findPlaces(this,[x,y]);%find which place is clicked
                        if pIndex~=0&&isempty(this.lPoint)%whether current click is the first click
                            this.lPoint = [x,y];% save x,y
                            
                            set(this.handles.hArcText,'String',this.places(pIndex).name);%show place name
                            
                        else
                            if pIndex~=0%current click is not first click
                                % draw arc between last point and current point
                                connectTransitionToPlace(this,this.lPoint,[x,y]);
                                % reset last point and text which is showed the entity of last click
                                resetLastArc(this);
                            else resetLastArc(this);%when current click point cannot find anyone place, reset it
                            end
                        end
                    end
                    
                case 'open'% double click
                    reset(this);% reset ui 
                    point=get(gca,'currentpoint');
                    hold on
                    
                    x = floor(point(1));
                    y = floor(point(3));
                    % this function let users to input num of token 
                    addToken(x,y,this);
                    
                case 'alt'% right click
                    reset(this);% reset ui
                otherwise
            end
            drawnow();
        end

        function retangleButtonDown(this,~,~)
		%this callback function is called when transition(rectangle) detects mouse click
            switch (get(gcbf,'SelectionType'))
                case 'normal'% click
                    point=get(gca,'currentpoint');
                    hold on
                    
                    x = floor(point(1));
                    y = floor(point(3));
                    
                    if this.isArc% whether in arc mode
                        tIndex = findTransitions(this,[x,y]);%find which transition is clicked
                        if tIndex~=0&&isempty(this.lPoint)%whether current click is the first click
                            this.lPoint = [x,y];% save x,y 
                            set(this.handles.hArcText,'String',this.transitions(tIndex).name);%show transition name
                        else
                            if tIndex~=0%current click is not first click
                                % draw arc between last point and current point
                                connectPlaceToTransition(this,this.lPoint,[x,y]);
                                % reset last point and text which is showed the entity of last click
                                resetLastArc(this);
                            else resetLastArc(this);
                            end
                        end
                    end
                case 'alt'% right click
                    reset(this);
                case 'open'% double click
                    point=get(gca,'currentpoint');
                    
                    % reset gui
                    reset(this);
                    x = floor(point(1));
                    y = floor(point(3));
                    tIndex = findTransitions(this,[x,y]);
                    prompt = ['Label of ',this.transitions(tIndex).name];
                    title = 'Labeling Function';
                    options.Resize='on';
                    options.WindowStyle='normal';
                    answer=inputdlg(prompt,title,[1,50],{this.transitions(tIndex).e},options);
                    if(~isempty(answer))
                        this.transitions(tIndex).e =answer{:};
						% reset variables which are determined by graph
						% we drawed
						resetIntermediateVariables(this);
                    end
                    
            end
            drawnow();
        end
        function tokenDoubleClick(this,~,~)
            %the function is called when token(Black dot) detects mouse click
            switch (get(gcbf,'SelectionType'))
                case 'open'% double click
                    
                    reset(this);
                    point=get(gca,'currentpoint');
                    hold on
                    
                    x = floor(point(1));
                    y = floor(point(3));
                    % this function let users to input num of token 
                    addToken(x,y,this);
                case 'normal'% when click, call circle(place) callback function
                    circleButtonDown(this);
            end
            drawnow();
        end
        
        function arcButtonDown(this,~,~)
            
            arc = gcbo;
            % arc = currentFigure.CurrentObject;
            switch (get(gcbf,'SelectionType'))
                case 'open'% double click
                    reset(this);% reset ui
                    userdata = get(arc,'UserData');
                    start = floor(userdata(1:2));% start of arc
                    stop = floor(userdata(4:5));% end of arc
                    tIndex = findTransitions(this,start);
                    if(tIndex~=0)%whether the start of arc is transition 
                        tarPoints = this.transitions(tIndex).tar;
                        [row,~] = size(tarPoints);
                        arrowIndex = [];
                        for i=1:row
                            if stop==tarPoints(i,:)%get index in the target places of this transition
                                arrowIndex = i;
                                break;
                            end
                        end
                        if(~isempty(arrowIndex))% let users input the weight of this arc
                            prompt = [this.transitions(tIndex).name,' -> ',this.places(findPlaces(this,stop)).name,'''s weight'];
                            title = 'Weight Setting';
                            options.Resize='on';
                            options.WindowStyle='normal';
                            answer=inputdlg(prompt,title,[1,50],{num2str(this.transitions(tIndex).tarArrow(arrowIndex))},options);
                            if(~isempty(answer))
                                weight = str2double(answer{:});
                                if(weight~=this.transitions(tIndex).tarArrow(arrowIndex))%weight is changed
                                    this.transitions(tIndex).tarArrow(arrowIndex) = weight;
                                    % reset variables which are determined by graph
                                    % we drawed
                                    resetIntermediateVariables(this);
                                end
                            end
                        else
                            errordlg('Error occured','Error');
                        end
                        
                    else %the start of arc is place 
                        tIndex = findTransitions(this,stop);
                        if(tIndex~=0)
                            sourPoints = this.transitions(tIndex).sour;
                            [row,~] = size(sourPoints);
                            arrowIndex = [];
                            for i=1:row
                                if start==sourPoints(i,:)%get index in the source places of this transition
                                    arrowIndex =i;
                                    break;
                                end
                            end
                            
                            if(~isempty(arrowIndex))% let users input the weight of this arc
                                prompt = [this.places(findPlaces(this,start)).name,' -> ',this.transitions(tIndex).name,'''s weight'];
                                title = 'Weight Setting';
                                options.Resize='on';
                                options.WindowStyle='normal';
                                answer=inputdlg(prompt,title,[1,50],{num2str(this.transitions(tIndex).sourArrow(arrowIndex))},options);
                                if(~isempty(answer))
                                    weight = str2double(answer{:});
                                    if(this.transitions(tIndex).sourArrow(arrowIndex) ~= weight);
                                        this.transitions(tIndex).sourArrow(arrowIndex) = weight;
                                        % reset variables which are determined by graph
                                        % we drawed
                                        resetIntermediateVariables(this);
                                    end
                                end
                            else
                                errordlg('Error occured','Error');
                            end
                        end
                    end
                    
            end
            drawnow();
        end
        function onMenuFile(this,hObject,~)% Menu File is called
            label = get (hObject ,'Label');
            switch label
                case 'Open'% Open MAT-FILE to import data to workspace
                    currentMFilePath = mfilename('fullpath');
					% Open an ui to select file 
                    [filename, pathname] = uigetfile('*.mat', 'Select a MATLAB MAT-File',currentMFilePath);
                    if isequal(filename,0)
                    else
                        filePath =  fullfile(pathname, filename);
						%load the transitions, places variables
                        fileData=load(filePath,'transitions','places');
                        %redraw graphic
						solveFileData(this,fileData);
%                       %close fileData
                    end
                case 'Save'
                    currentMFilePath = mfilename('fullpath');
                    i=strfind(currentMFilePath,'\');
                    currentMFilePath=currentMFilePath(1:i(end));
					% Open an ui to export file
                    [filename,pathname]  = uiputfile({'*.mat'},'Save MAT-File',currentMFilePath);
                    if isequal(filename,0)
                    else
                        filePath =  fullfile(pathname, filename);
                        transitions = this.transitions;
                        places = this.places;
                        [~,pNum] = size(places);
                        for i=1:pNum
                            places(i).tokens = [];% clear the graphic of tokens
                            places(i).textToken = [];% clear the text of token number
                        end
                        save(filePath,'transitions','places');
                    end
            end
            
        end
        function onMenuBasic(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            switch label
                case 'Compute the RG'
                    this.variables.G=graphPN(Pre,Post,M0);
                case 'Show the RG'
                    if(~isempty(this.variables.G))
                        show(this.variables.G);
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Step-by-step Simulation'
                    play(Pre,Post,M0);
                case 'Boundedness'
                    if(~isempty(this.variables.G))
                        bounded(this.variables.G);
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Deadlock Markings'
                    if(~isempty(this.variables.G))
                        [D,Mb]=dead(this.variables.G)
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Firability'
                    if(~isempty(this.variables.G))
                        prompt = {'Please enter a transition sequence (e.g., t1t2=[1 2]):'};
                        dlg_title = 'Parameter Input';
                        num_lines = [1 50];
                        answer = inputdlg(prompt,dlg_title,num_lines);
                        if(size(answer)~=0)
                            I2 = str2num(answer{1});
                            firable(this.variables.G,I2);
                        end
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'P-invariants'
                    PI=pinvar(Pre,Post)
                case 'Siphons'
                    S=siphons(Pre,Post)
                case 'Compute the RT/CT'
                    this.variables.T = tree(Pre,Post,M0);
                case 'Show the RT/CT'
                    if(~isempty(this.variables.T))
                        figure(2)
                        plottree(this.variables.T);
                    else
                        errordlg('Please compute the RT/CT first!','Error');
                    end
                case 'Liveness'
                    if(~isempty(this.variables.G))
                        [L,tr]=live(this.variables.G)
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Reachability'
                    if(~isempty(this.variables.G))
                        prompt = {'Please enter a marking M (e.g., [1;3]):'};
                        dlg_title = 'Parameter Input';
                        num_lines = [1 50];
                        answer = inputdlg(prompt,dlg_title,num_lines);
                        if(size(answer)~=0)
                            M = str2num(answer{1});
                            reachable(this.variables.G,M);
                        end
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Reversibility'
                    if(~isempty(this.variables.G))
                        reversible(this.variables.G)
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                case 'Repetitiveness'
                    if(~isempty(this.variables.G))
                        prompt = {'Please enter a transition sequence (e.g., t1t2=[1 2]):'};
                        dlg_title = 'Parametr Input';
                        num_lines = [1 50];
                        answer = inputdlg(prompt,dlg_title,num_lines);
                        if(size(answer)~=0)
                            I3 = str2num(answer{1});
                            repetitive(this.variables.G,Pre,Post,I3);
                        end
                    else
                        errordlg('Please compute the RG first!','Error');
                    end
                                              
                case 'T-invariants'
                    TI=tinvar(Pre,Post)
                case 'Traps'
                    TP=traps(Pre,Post)
            end
        end
        function onMenuMonit(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            
            switch label
                case 'Input the GMECs'
                    prompt = {'Enter matrix W:','Enter matrix K:','Enter the uncontrollable transitions u:'};
                    dlg_title = 'Input the GMECs';
                    num_lines = [3 50;3 50;1 50];
                    defAns = {mat2str(this.variables.W) ,mat2str(this.variables.K ),mat2str(this.variables.u)};
                    answer = inputdlg(prompt,dlg_title,num_lines,defAns);
                    if(size(answer)~=0)
                        this.variables.W = str2num(answer{1});
                        this.variables.K = str2num(answer{2});
                        this.variables.u = str2num(answer{3});
                    end
                case 'Controllability'
                    if(~isempty(this.variables.W)&&~isempty(this.variables.K)&&~isempty(this.variables.u))
                        checkgmecs(Post-Pre,M0,this.variables.W,this.variables.K,this.variables.u);
                    else
                        errordlg('GMECs or uncontrollable transitions are not defined, please input them first!','Data Error');
                    end
                case 'Compute Controllable GMECs'
                    if(~isempty(this.variables.W)&&~isempty(this.variables.K)&&~isempty(this.variables.u))
                        controllablegmec(Post-Pre,M0,this.variables.W,this.variables.K,this.variables.u);
                    else
                        errordlg('GMECs or uncontrollable transitions are not defined, please input them first!','Data Error');
                    end
                case 'Design the Closed-loop System'
                    if(~isempty(this.variables.W)&&~isempty(this.variables.K)&&~isempty(this.variables.u))
                        monitordesign(Pre,Post,M0,this.variables.W,this.variables.K,this.variables.u);
                    else
                        errordlg('GMECs or uncontrollable transitions are not defined, please input them first!','Data Error');
                    end
                case 'Monitor Places'
                    if(~isempty(this.variables.W)&&~isempty(this.variables.K)&&~isempty(this.variables.u))
                        monitorplaces(Post-Pre,M0,this.variables.W,this.variables.K,this.variables.u);
                    else
                        errordlg('GMECs or uncontrollable transitions are not defined, please input them first!','Data Error');
                    end
            end
        end
        function onMenuReach(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            switch label
                case 'Compute the BRG'
                    prompt = {'Please enter the set of explicit transitions Te (e.g., [1 2]):'};
                    dlg_title = 'Parameter Input';
                    num_lines = [1 50];
                    answer = inputdlg(prompt,dlg_title,num_lines);
                    if(size(answer)~=0)
                        Te = str2num(answer{1});
                        this.variables.BRG=BRG(Pre,Post,M0,Te);
                    end
                case 'Show the BRG'
                    if(~isempty(this.variables.BRG))
                        showBRG(this.variables.BRG);
                    else
                        errordlg('Please compute teh BRG first!','Error');
                    end
                case 'Reachability Given Te'
                    prompt = {'Enter the set of explicit transitions Te:','Enter a marking M:'};
                    dlg_title = 'Parameter Input';
                    num_lines = [3 50;3 50];
                    answer = inputdlg(prompt,dlg_title,num_lines);
                    if(size(answer)~=0)
                        Te = str2num(answer{1});
                        M = str2num(answer{2});
                        r=IfReachTe(M,Pre,Post,M0,Te);
                    end
                case 'Compute Explicit Transitions Te'%%when this function is called there are warning messages
                    Te=CompTe(Pre,Post)
                case 'Reachability without Te'%%when this function is called there are warning messages
                    prompt = {'Please enter a marking M:'};
                    dlg_title = 'Parameter Input';
                    num_lines = [3 50];
                    answer = inputdlg(prompt,dlg_title,num_lines);
                    if(size(answer)~=0)
                        M = str2num(answer{1});
                        [r,Te]=IfReach(M,Pre,Post,M0);
                        Te
                    end
                    
            end
        end
        function onMenuEstm(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            
            switch label
                case 'Input Observation w'
                    prompt = {'Please enter observation w (as a string arrary, e.g.,w=acd):'};
                    dlg_title = 'Parameter Input';
                    num_lines = [1 50];
                    defAns = {''};
                    defAns{1,1} = char(this.variables.w);
                    answer = inputdlg(prompt,dlg_title,num_lines,defAns);
                    if(size(answer)~=0)
                        this.variables.w = char(answer{1});
                    end
                case 'Compute the BRG for an LPN'
                    [L,E] = calculateLE(this);
                    if(isempty(L)||isempty(E))
                        errordlg('The labeling funtion is not defined yet!','Data Error')
                    else
                        this.variables.BRG_L = BRG_L(Pre,Post,M0,L,E);
                    end
                case 'Show the BRG'
                    if(~isempty(this.variables.BRG_L))
                        showBRG_L(this.variables.BRG_L);
                    else
                        errordlg('Please compute the BRG first!','Error')
                    end
                case 'Observer of the RG'
                    if(~isempty(this.variables.G))
                        [L,E] = calculateLE(this);
                        [ObsR,yr]=ObsRG(this.variables.G,L,E)
                        celldisp(ObsR);
                        celldisp(yr);
                    else
                        errordlg('Please compute the RG first!','Error')
                    end
                case 'Observer of the BRG'
                    if(~isempty(this.variables.BRG_L))
                        [ObsB,yb]=ObsBRG(this.variables.BRG_L)
                        celldisp(ObsB);
                        celldisp(yb);
                    else
                        errordlg('Please compute the BRG first!','Error')
                    end
                case 'Initial-State Estimator of the RG'
                    if(~isempty(this.variables.G))
                        [L,E] = calculateLE(this);
                        [EstR,yr]=estRG(this.variables.G,E,L)
                        celldisp(EstR);
                        celldisp(yr);
                    else
                        errordlg('Please compute the RG first!','Error')
                    end
                case 'Initial-State Estimator of the BRG'
                    if(~isempty(this.variables.BRG_L))
                        [EstB,yb]=estBRG(this.variables.BRG_L)
                        celldisp(EstB);
                        celldisp(yb);
                    else
                        errordlg('Please compute the BRG first!','Error')
                    end
                case 'Current-State Estimation (RG-Based)'
                    if(isempty(this.variables.w))
                        errordlg('Observation w is not defined,please input w first!','Data Error');
                    elseif (isempty(this.variables.G))
                        errordlg('Please compute the RG first!','Error')
                    else
                        [L,E] = calculateLE(this);
                        [Cw]=CurEst_RG(Pre,Post,M0,this.variables.G,L,E,this.variables.w);
                        celldisp(Cw);
                    end
                case 'Current-State Estimation (BRG-Based)'
                    if(isempty(this.variables.w))
                        errordlg('Observation w is not defined,please input w first!','Data Error');
                    elseif(isempty(this.variables.BRG_L))
                        errordlg('Please compute the BRG first!','Error');
                    else
                        [L,E] = calculateLE(this);
                        [Cbw]=CurEst_BRG(this.variables.BRG_L,L,E,this.variables.w);
                        celldisp(Cbw);
                    end
                    
                case 'Initial-State Estimation (RG-Based)'
                    if(isempty(this.variables.w))
                        errordlg('Observation w is not defined,please input w first!','Data Error');
                    elseif (isempty(this.variables.G))
                        errordlg('Please compute the RG first!','Error')
                    else
                        [L,E] = calculateLE(this);
                        [Iw]=IniEst_RG(Pre,Post,M0,this.variables.G,L,E,this.variables.w);
                        celldisp(Iw);
                    end
                case 'Initial-State Estimation (BRG-Based)'
                    if(isempty(this.variables.w))
                        errordlg('Observation w is not defined,please input w first!','Data Error');
                    elseif(isempty(this.variables.BRG_L))
                        errordlg('Please compute the BRG first!','Error');
                    else
                        [L,E] = calculateLE(this);
                        [Ibw]=IniEst_BRG(Pre,Post,M0,this.variables.BRG_L,L,E,this.variables.w);
                        celldisp(Ibw);
                    end
            end
        end
        function onMenuDiag(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            
            switch label
                case 'Input Fault Classes'
                    prompt = {'Enter the number of fault classes F:'};
                    dlg_title = 'Parameter Input';
                    num_lines = [1 50];
                    answer = inputdlg(prompt,dlg_title,num_lines);
                    
                    if(size(answer)~=0)
                        number = str2num(answer{1});
                        if(number==0)
                            return;
                        end
                        prompt2 = cell(1,number);
                        dlg_title = 'Input transition number';
                        num_lines2 = zeros(number,2);
                        defAns = cell(1,number);
                        
                        for i=1:number
                            num_lines2(i,1:2) = [1 50];
                            prompt2{1,i} = ['Transitions in fault class f',num2str(i),':'];
                            defAns{1,i} = '';
                        end
                        if(size(this.variables.F)==[number,1])
                            for i=1:number
                                if(~isempty(this.variables.F{i,1}))
                                    defAns{1,i} = num2str(this.variables.F{i,1});
                                end
                            end
                        end
                        ans2 = inputdlg(prompt2,dlg_title,num_lines2,defAns);
                        this.variables.F = cell(number,1);
                        if(~isempty(ans2))
                            for i=1:number
                                if(~isempty(ans2{i}))
                                    this.variables.F{i,1} = str2double(ans2{i});
                                end
                            end
                        end
                    end
                    
                case 'Compute the BRG'%%when this function is called there are warning messages
                    if(~isempty(this.variables.F))
                        [L,E] = calculateLE(this);
                        this.variables.BRG_D = BRG_D(Pre,Post,M0,this.variables.F,L,E);
                    else
                        errordlg('Fault classes F are not defined, please input F first!','Data Error');
                    end
                    
                case 'Show the BRG'
                    if(~isempty(this.variables.BRG_D))
                        showBRG_D(this.variables.BRG_D);
                    else
                        errordlg('Please compute the BRG first!','Error');
                    end
                case 'Compute the MBRG'
                    if(~isempty(this.variables.F))
                        [L,E] = calculateLE(this);
                        this.variables.MBRG = MBRG(Pre,Post,M0,this.variables.F,L,E);
                    else
                        errordlg('Fault classes F are not defined, please input F first!','Data Error');
                    end
                case 'Show the MBRG'
                    if(~isempty(this.variables.MBRG))
                        showMBRG(this.variables.MBRG);
                    else
                        errordlg('Please compute the MBRG first!','Error');
                    end
                case 'Compute the BRD'
                    if(~isempty(this.variables.BRG_D))
                        if(~isempty(this.variables.F))
                            [L,E] = calculateLE(this);
                            this.variables.BRD = BRD(this.variables.BRG_D,Pre,Post,M0,this.variables.F,L,E);
                        else
                            errordlg('Fault classes F are not defined, please input F first!','Data Error');
                        end
                    else
                        errordlg('Please compute the BRG first!','Error');
                    end
                    
                case 'Show the BRD'
                    if(~isempty(this.variables.BRD))
                        showBRD(this.variables.BRD);
                    else
                        errordlg('Please compute the BRD first!','Error');
                    end
                case 'Diagnosability'
                    if(~isempty(this.variables.MBRG)&&~isempty(this.variables.BRD))
                        diagnosability( this.variables.MBRG,this.variables.BRD)
                    else
                        errordlg('Please compute the MBRG and the BRD first!','Error');
                    end
                case 'Diagnosis'
                    if(~isempty(this.variables.F))
                        prompt = {'Please enter observation w (as a string arrary, e.g.,w=acd):'};
                        dlg_title = 'Parameter Input';
                        num_lines = [1 50];
                        answer = inputdlg(prompt,dlg_title,num_lines);
                        if(size(answer)~=0)
                            this.variables.w = char(answer{1});
                            [L,E] = calculateLE(this);
                            diagnosis(Pre, Post, M0,this.variables.w, this.variables.F, L, E);
                        end
                    else
                        errordlg('Fault classes F are not defined, please input F first!','Data Error');
                    end
                    
            end
        end
        
        function onMenuOpac(this,hObject,~)
            [Pre,Post,M0]=geneMatrix(this);
            label = get (hObject ,'Label');
            [L,E] = calculateLE(this);
            switch label
                case 'Input a Secret'
                    prompt = {'Enter matrix W:','Enter matrix K:'};
                    dlg_title = 'Parameter Input';
                    num_lines = [3 50;3 50];
                    defAns = cell(2,1);
                    defAns{1,1} = mat2str(this.variables.W);
                    defAns{2,1} = mat2str(this.variables.K);
                    answer = inputdlg(prompt,dlg_title,num_lines,defAns);
                    if(size(answer)~=0)
                        this.variables.W = str2num(answer{1});
                        this.variables.K = str2num(answer{2});
                    end
                case 'Compute the BRG for CSO'%%when this function is called there are warning messages
                    if(~isempty(this.variables.W)&&~isempty(this.variables.K))
                        S = {this.variables.W;this.variables.K};
                        this.variables.BRG_Cur=BRG_Cur(Pre,Post,M0,L,E,S);
                    else
                        errordlg('The secret is not defined, please input the secret first!','Data Error');
                    end
                case 'Show the BRG for CSO'
                    if(~isempty(this.variables.BRG_Cur))
                        showBRG_Cur(this.variables.BRG_Cur);
                    else
                        errordlg('Please compute the BRG for CSO first!','Error');
                    end
                case 'Current-State Opacity'%%when this function is called there are warning messages
                    if(~isempty(this.variables.BRG_Cur))
%                         [CSO,ObsB,yb]=CurOpac(this.variables.BRG_Cur);
                        CurOpac(this.variables.BRG_Cur);
                    else
                        errordlg('Please compute the BRG for CSO first!','Error');
                    end
                case 'Initial-State Opacity'
                    if(~isempty(this.variables.BRG_L))
                         S = {this.variables.W;this.variables.K};
                        %[ISO,EstB,yb]=InitOpac(this.variables.BRG_L,S);
                        InitOpac(this.variables.BRG_L,S);
                    else
                        errordlg('Please compute the BRG for an LPN first!','Error');
                    end                                      
            end
        end
    end
end
%% Helper Functions
function c = circle(this, x,y,r )% draw circle
c=rectangle('Position',[x-r,y-r,2*r,2*r],'Curvature',[1,1],'linewidth',1);
set(c,'ButtonDownFcn',@this.circleButtonDown);
end
function t = token(this, x,y )% draw token
t=rectangle('Position',[x-0.05,y-0.05,0.1,0.1],'Curvature',[1,1],'linewidth',1,'FaceColor','black');
set(t,'ButtonDownFcn',@this.tokenDoubleClick);
end
function r = drawRectangle(this, x,y)% draw rectangle
r=rectangle('Position',[x-0.1,y-0.3,0.2,0.6],'linewidth',1);
set(r,'ButtonDownFcn',@this.retangleButtonDown);
end
function a = drawArrow(this,start,stop)% draw arrow
[start,stop] = adjustPoints(this,start,stop);% adjust start and stop point 
a=arrow(start,stop);

arrow(a,'BaseAngle',25,'TipAngle',23,'Length',13);
set(a,'ButtonDownFcn',@this.arcButtonDown);
end

function connectTransitionToPlace(this,transLocation,placeLocation)%add arrow data and draw arrow
tIndex = findTransitions(this,transLocation);

if tIndex~=0
    
    this.transitions(tIndex).tar =[this.transitions(tIndex).tar;placeLocation];
    
    a = drawArrow(this,transLocation,placeLocation);
    
    this.transitions(tIndex).tarArrow =[this.transitions(tIndex).tarArrow ;1];
    
end
end

function connectPlaceToTransition(this,placeLocation,transLocation)%add arrow data and draw arrow
pIndex = findPlaces(this,placeLocation);
tIndex = findTransitions(this,transLocation);
if pIndex~=0
    
    this.places(pIndex).tar = [this.places(pIndex).tar ;transLocation];
    this.transitions(tIndex).sour = [this.transitions(tIndex).sour;placeLocation];
    a=drawArrow(this,placeLocation,transLocation);
    this.transitions(tIndex).sourArrow = [this.transitions(tIndex).sourArrow;1];
end
end

function setPlaceValue(this,pIndex,name,x,y,tokenNum,tar)% setter of place
this.places(pIndex).name = name;
this.places(pIndex).x = x;
this.places(pIndex).y = y;

this.places(pIndex).tokenNum = tokenNum;
this.places(pIndex).tar = tar;

this.places(pIndex).tokens = [];
this.places(pIndex).textToken = text(x+0.43,y+0.48,'');
text(x+0.8,y+0.8,this.places(pIndex).name );
if(tokenNum~=0)
    drawToken(this,pIndex,tokenNum);
end

end
function setTransitionValue(this,tIndex,name,x,y,sour,tar,sourArrow,tarArrow,e)% setter of transition
this.transitions(tIndex).name = name;
this.transitions(tIndex).x = x;
this.transitions(tIndex).y = y;

this.transitions(tIndex).sour = sour;
this.transitions(tIndex).tar = tar;

this.transitions(tIndex).sourArrow = sourArrow;
this.transitions(tIndex).tarArrow = tarArrow;

this.transitions(tIndex).e = e;
text(x+0.7,y+0.7,this.transitions(tIndex).name);
end
function [start,stop] = adjustPoints(this,pointStart,pointStop)
%point is center of the graphic
point1 = pointStart + [0.5,0.5];
point2 = pointStop + [0.5,0.5];

% copy of center point
point11 = point1;
point22 = point2;
%delta Y
yF = point1(2) - point2(2);
%delta X
xF = point1(1) - point2(1);
index = findTransitions(this,pointStart);
if(index==0)%start point is place
    startXDelta = 0.3;% the radius of circle is 0.3
    stopXDelta = 0;
    if xF>0
        point22 = point22 +[0.1,0];
    else
        point22 = point22+[-0.1,0];
    end
    
else
    %start point is transition
    stopXDelta = 0.3;
    startXDelta = 0;
    if xF>0
        point11 = point1 +[-0.1,0];
    else
        point11 = point1+[0.1,0];
    end
end


k = (point11(2) - point22(2))./( point11(1) - point22(1) );% calculate slope
b = point11(2) -  k*point11(1);

k1 = cos(atan(k));% compute cos of the angle
stopXDelta = stopXDelta*k1;
startXDelta = startXDelta*k1;

if(xF> 0)
    start = [point11(1)-startXDelta,k*(point11(1)-startXDelta)+b];
    stop =[point22(1)+stopXDelta,k*(point22(1)+stopXDelta)+b];
else if(xF< 0)
        start = [point11(1)+startXDelta,k*(point11(1)+startXDelta)+b];
        stop =[point22(1)-stopXDelta,k*(point22(1)-stopXDelta)+b];
    else
        if(yF>0)
            start = [point1(1),point1(2)-0.3];
            stop = [point2(1),point2(2)+0.3];
        else
            start = [point1(1),point1(2)+0.3];
            stop = [point2(1),point2(2)-0.3];
        end
        
    end
end





end

function addToken(x,y,this)%this function allows user to input token number and then redraw tokens
pIndex = findPlaces(this,[x,y]);
tokenNum = this.places(pIndex).tokenNum;

% prompt = {'Name of the place:','Number of tokens :'};
prompt = {['Number of tokens in ', this.places(pIndex).name, ':']};
title = 'Token Number Setting';
options.Resize='on';
options.WindowStyle='normal';
% answer=inputdlg(prompt,title,[1,50;1,50],{this.places(pIndex).name,num2str(tokenNum),},options);
answer=inputdlg(prompt,title,[1,50],{num2str(tokenNum)},options);
if(~isempty(answer))
    newTokenNum = str2double(answer);
    if newTokenNum==tokenNum
        return;
    else
        tokenNum = newTokenNum;
        delete(this.places(pIndex).tokens);
        this.places(pIndex).tokens = [];
        
        drawToken(this,pIndex,tokenNum);
        
        this.places(pIndex).tokenNum = tokenNum;
        % reset variables which are determined by graph
        % we drawed
        resetIntermediateVariables(this);
    end
end

end

function [L,E]=calculateLE(this)
E= cell(this.tNum,1);
L= cell(this.tNum,1);
length=0;
for i=1:this.tNum
    if (~isempty(this.transitions(i).e))
        for j=1:this.tNum
            if(isempty(E{j,1}))
                E{j,1}=this.transitions(i).e;
                L{j,1}= i;
                length = length + 1;
                break;
            else
                if (strcmp(this.transitions(i).e , E {j,1}))
                    L{j,1} = [L{j,1},i];
                    break;
                end
            end
        end
    end
end
E=E(1:length,1);
L=L(1:length,1);
end

function drawToken(this,pIndex,tokenNum)
if(tokenNum~=0)
x=this.places(pIndex).x+0.2;% radius of a place is 0.3
y=this.places(pIndex).y+0.2;% 
if tokenNum>2&&tokenNum<4 % if more than 3, occupy two lines
    row2 = floor(tokenNum./2);
    row1 = tokenNum - row2;
    deltaXRow1 =  0.6./(1+row1);
    for i=1:row1
        this.places(pIndex).tokens(i)=token(this,x+deltaXRow1*i,y+0.2);
    end
    
    deltaXRow2 =  0.6./(1+row2);
    for i=1:row2
        this.places(pIndex).tokens(i+row1)=token(this,x+deltaXRow2.*i,y+0.4);
        
    end
    set(this.places(pIndex).textToken,'String','');
else if tokenNum<3&&tokenNum>0
        for i=1:tokenNum
            x = x + 0.6./(tokenNum+1);
            this.places(pIndex).tokens(i)=token(this,x,y+0.3);
        end
        set(this.places(pIndex).textToken,'String','');
    else
        if(tokenNum>0)
            set(this.places(pIndex).textToken,'String',num2str(tokenNum));
            set(this.places(pIndex).textToken,'ButtonDownFcn',@this.tokenDoubleClick);
        end
    end
end
end
end
function solveFileData(this,fileData)% use file data to set valut 
axes(this.handles.hAxOut);
this.onReset(this);
[~,tNum] =size( fileData.transitions);
[~,pNum] =size( fileData.places);
this.transitions = fileData.transitions;
this.tNum = tNum;
this.pNum = pNum;
for i=1:pNum
    
    x=fileData.places(i).x;
    y=fileData.places(i).y;
    name=fileData.places(i).name;
    tokenNum = fileData.places(i).tokenNum;
    tar=fileData.places(i).tar;
    
    setPlaceValue(this,i,name,x,y,tokenNum,tar);%it will draw tokens
    circle(this,this.places(i).x+0.5,this.places(i).y+0.5,0.3);
    text(this.places(i).x+0.8,this.places(i).y+0.8,this.places(i).name);% show place name
end
for i=1:tNum
    drawRectangle(this,this.transitions(i).x+0.5,this.transitions(i).y+0.5);
    text(this.transitions(i).x+0.7,this.transitions(i).y+0.7,this.transitions(i).name);%show transition name
    transLocation = [this.transitions(i).x,this.transitions(i).y];
    
    [row,~] = size(this.transitions(i).sour);
    for j=1:row
        this.transitions(i).sour(j,:);
        drawArrow(this,this.transitions(i).sour(j,:),transLocation);
    end
    [row,~] = size(this.transitions(i).tar);
    for j=1:row
        this.transitions(i).tar(j,:);
        drawArrow(this,transLocation,this.transitions(i).tar(j,:));
    end
    
end


end


function drawEntity(this,x,y)

if this.isPlace
    % reset(this);
    if(findPlaces(this,[x,y])==0&&findTransitions(this,[x,y])==0)
        this.pNum = this.pNum +1;
        c=circle(this,x+0.5,y+0.5,0.3);
        name =  ['p',num2str(this.pNum)];
        setPlaceValue(this,this.pNum,name,x,y,0,[]);
        % reset variables which are determined by graph
        % we drawed
        resetIntermediateVariables(this);
        
    end
else
    if this.isTransition
        if(findPlaces(this,[x,y])==0&&findTransitions(this,[x,y])==0)
            r=drawRectangle(this,x+0.5,y+0.5);
            this.tNum = this.tNum+1;
            name =  ['t',num2str(this.tNum)];
            setTransitionValue(this,this.tNum,name,x,y,[],[],[],[],'');
            
            % reset variables which are determined by graph
            % we drawed
            resetIntermediateVariables(this);
        end
    else
        if this.isArc
            pIndex = findPlaces(this,[x,y]);
            tIndex = findTransitions(this,[x,y]);
            if (pIndex~=0||tIndex~=0)&&isempty(this.lPoint)
                this.lPoint = [x,y];
                if pIndex~=0
                    set(this.handles.hArcText,'String',this.places(pIndex).name);
                else
                    set(this.handles.hArcText,'String',this.transitions(tIndex).name);
                end
                
            else
                if pIndex~=0||tIndex~=0
                    pIndexL  = findPlaces(this,this.lPoint);
                    tIndexL = findTransitions(this,this.lPoint);
                    
                    if tIndex~=0&&pIndexL~=0
                        connectPlaceToTransition(this,this.lPoint,[x,y]);
                        
                        resetLastArc(this);
                        % reset variables which are determined by graph
                        % we drawed
                        resetIntermediateVariables(this);
                    else
                        
                        if pIndex~=0&&tIndexL~=0
                            connectTransitionToPlace(this,this.lPoint,[x,y]);
                            
                            resetLastArc(this);
                            % reset variables which are determined by graph
                            % we drawed
                            resetIntermediateVariables(this);
                        else resetLastArc(this);
                        end
                    end
                else resetLastArc(this);
                end
            end
        end
    end
end


end
function H = gobj(varargin)
%GOBJ  Create an array to store graphic handles

try
    H = gobjects(varargin{:});
catch
    H = zeros(varargin{:});
end
end

% @Copyright Siqi Li, May, 2018





