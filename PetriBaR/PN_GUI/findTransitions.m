
function index = findTransitions(this,point)
index = 0;
for i=1:this.tNum
    if this.transitions(i).x == point(1)&&this.transitions(i).y == point(2)
        index = i;
    end
end
end

% @Copyright Siqi Li, May, 2018