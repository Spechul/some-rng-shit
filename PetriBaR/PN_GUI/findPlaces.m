function index = findPlaces(this,point)
index = 0;
for i=1:this.pNum
    if this.places(i).x == point(1)&&this.places(i).y == point(2)
        index = i;
    end
end
end

% @Copyright Siqi Li, May, 2018
