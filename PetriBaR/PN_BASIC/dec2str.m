function s = dec2str(x)
%DEC2STR Converts integer numbers into strings
%   Syntax: S = DEC2STR(X)
%
%   Converts the entries of a vector X into a string S.
% 
%   With respect to INT2STR and NUM2STR, it does not compute the format of 
%   the representation depending on its magnitude, but always uses the 
%   decimal format. Therefore, the entries of the vector always appear
%   separated by a single space, which is useful for compact representations 
%   (this is particularly useful when the vectors contain Inf as a value). 
%
%   Based on function INT2STR (Copyright (c) 1984-97 
%   by The MathWorks, Inc. $Revision: 5.9 $  $Date: 1997/04/08 
%   06:50:30 $)
%   
%   See also NUM2STR, INT2STR.
%
%   Author: Diego Mura, 2003, University of Cagliari

n=length(x);

if length(x) == 1
   s = sprintf('%d',x);
else
   s = '';
   t = [];
      for j = 1:n
         t = [ t ' ' sprintf('%d',x(j))];
      end
      s = [s; t];
   if ~isempty(s)
      while all(s(:,1) == ' ')
         s(:,1) = [];
      end
   end
end
