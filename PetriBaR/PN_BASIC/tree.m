function T = tree(Pre,Post,M0)
% TREE: This function builds the Reachability/Coverability Tree of a Petri Net System.
%       ******************************************************************************
%                                      ## SYNTAX ##
%
%       T=TREE(Pre,Post,M0)  
%         Given a Petri Net with Pre, Post matrices and the initial
%         marking M0, this function returns a matrix T of integers that
%         represents Reachability/Coverability tree.
%
%       See also GRAPH

Mx=M0';
ni=nargin;
error(nargchk(3,3,ni));
[m,n]=size(Post);
[c,p]=size(Mx);
if (size(Post)==size(Pre))&(c==1)&(p==m),
   T=[Mx 0 0 0];
   N=[Mx 0 0 ];
   rN=1;
   rN1=1;
   r=1;
   g=1;
   while r<=g
      if T(r,m+2)==0
         k=1;
         for j=1:n
            if T(r,1:m)>=Pre(:,j)' 
               g=g+1;
               T(r,m+1+2*k+1)=j;
               T(r,m+1+2*k+2)=g;
               M=T(r,1:m)-Pre(:,j)'+Post(:,j)';
               a=r;
               while a~=0  
                  if M >=T(a,1:m),
                     for i=1:m;
                        if M(i)>T(a,i)
                           M(i)=inf;
                        end
                     end
                     a=1;
                  end
                  a=T(a,m+1);
               end
               T(g,1:m)=M;
               T(g,m+1)=r;
               N(rN,m+1)=rN1;
               N(rN,m+2)=rN;
               for w=1:rN 
                  if N(w,1:m)==T(g,1:m)
                     T(g,m+2)=w;
                     T(g,m+3)=N(w,m+1);
                     break
                  end
               end
               k=k+1;
               if T(g,m+2)==0 
                  M1=[M 0 0];
                  N=[N;M1];
                  rN=rN+1;
                  rN1=g;
                  N(rN,m+1)=rN1;
                  N(rN,m+2)=rN;
               end               
            end
         end
         if k==1,
            T(r,m+2)=-1;
            T(r,m+3)=-1;
         else
            T(r,m+2)=-2;
            T(r,m+3)=-2;
         end
      end
      r=r+1;
   end
   [rT,cT]=size(T);
  
   K=zeros(1,cT);
   T=[T;K];
   [i3,i2]=size(T);
   T(i3,1)=m;
   T(i3,2)=n;
   T(i3,3)=0;
   T(:,m+2)=[];  %erase the (m+2)� column of T

elseif size(Post)~=size(Pre)
   fprintf('\n ERROR!! Different matrice''s dimension between Pre and Post!\n')
   fprintf('\n Insert Pre and Post again\n')
else
   fprintf('\n ERROR! Initial marking dimensions wrong!\n')
end
