function show1(G)
%SHOW1: This function shows Reachability/Coverability Graph of a P/T System.
%      *******************************************************************
%                          ## SYNTAX ##
%
%      SHOW1(G)  
%        This function shows in a pretty format a Petri Net's 
%        Reachability/Coverability Graph G 
%
%      See also: PLAY,GRAPH
ni=nargin;
error(nargchk(1,1,ni));
m=G(end,1);
n=G(end,2);
G(end,:)=[];
G(:,m+1:m+2)=[];
G0=G; 
[s,t]=size(G);
dM=ceil(log10(s));
dT=ceil(log10(n+1));
G0(:,1:m)=[];
[b,c]=size(G0);
fprintf('\nMarking')
for z=1:dM+2*m+1,
     fprintf(' ')
end
fprintf('Enabled transitions \n')
fprintf('-------')
for z=1:dM+2*m+1,
     fprintf(' ')
end
fprintf('------------------- \n')
   for f=1:s
      fprintf('\nM')
      fprintf('%d',f-1)
      d=max(ceil(log10(f)),1);
      for z=1:dM-d,
          fprintf(' ')
      end
      fprintf(' =')
      for k=1:m,
          if G(f,k)==Inf,
             fprintf(' w') 
          else
              fprintf(' %d',G(f,k))
          end
      end
      fprintf('  ')
      e=1;
      while e<=c
         if G0(f,e)>0
            fprintf('   t%d',G0(f,e))
            d=max(ceil(log10(G0(f,e)+1)),1);
            for z=1:dT-d,
               fprintf(' ')
            end
            e=e+1;
            fprintf('->M%d',G0(f,e)-1)
            d=max(ceil(log10(G0(f,e))),1);
            for z=1:dM-d,
               fprintf(' ')
            end
            e=e+1;
         else e=e+2;
         end
      end
  end
  fprintf('\n')
