function [x,y,h]=treelay(parent,depth)
%TREELAY computes the coordinates in which to represent the nodes of a tree
%   Syntax: [x,y,h]=treelay(PARENT,DEPTH).
%
%   Given the vector of the parent nodes PARENT and the vector of the depth 
%   of the nodes DEPTH, it returns the vectors x and y of the  
%   coordinates in which to draw the nodes to obtain a readable
%   graphical representation. 
%   It also returns the hight of the tree h. 
%   It differs from the analogous function TREELAYOUT for the fact that the
%   representation is done by levels, namely here nodes with the same
%   depth are put at the same hight. 
%
%   See also TREELAYOUT, DEPTH.
%
%   Autore: Diego Mura, 2003, University of Cagliari, Italy
          


for lvl=0:max(depth)       % lvl=current depth level
   dummy=find(depth==lvl); % current vector of nodes at level
   numnodes=length(dummy); % current number of nodes at level
   k=0;
   for i=1:numnodes
      x(dummy(i))=(2*k+1)/(2*numnodes); % equally distributes the nodes of a level
      k=k+1;
   end
end

h=max(depth);
y=(h+0.5-depth)/(h+1);
