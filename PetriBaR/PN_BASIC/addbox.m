function [Xb,Yb,color]=addbox(x,y,w,h,nature)
%ADDBOX computes the coordinates of a series of colored boxes
%   Syntax: [Xb,Yb,COLOR]=ADDBOX(X,Y,W,H,NATURE)
%
%   Given the vector X and Y of the coordinates at whom boxes should be centered,
%   returs the matrices Xb and Yb of the coordinates of the vertices.
%   The dimensions of boxes are defined by the vector of widths W and the
%   vector of hights H. 
%   The vector of integers NATURE determines the vector COLOR of the colors with 
%   whom boxes should be filled, according to the scheme:
%
%         NATURE(i)==-1.....yellow
%         NATURE(i)==-2.....green
%         NATURE(i)==-3.....white
%         altrimenti........light blue
%
%   NOTE(1): In the particular case in which there are only three boxes
%            to position, the vector COLOR would be considered by function
%            PATCH as a triple RGB, making this function useless. 
%            It has been adopted the solution to add a further box of null
%            dimension and white color positioned in (0,0).
%
%   NOTE(2): This function has been created to be used with function PATCH.
%            The choice of colors has been done on the basis of the meaning 
%            associated with colors in function PLOTTREE.
%
%   NOTE(3): When ADDBOX is executed, the predefined map of colors 
%            "colorcube" is attributed. For information on the predefined
%            map, see the help of GRAPH3D.
%
%   See also PATCH, PLOTTREE.
%
%   Author: Diego Mura, 2003, University of Cagliari
%           indirizzo e-mail: diegolas@inwind.it

Xb=zeros(4,length(x));    
Yb=zeros(4,length(x));    

% computation of the vertices of the boxes

for j=1:length(x)
   Xb(1,j)=x(j)-(w(j)/2);
   Xb(2,j)=x(j)-(w(j)/2);
   Xb(3,j)=x(j)+(w(j)/2);
   Xb(4,j)=x(j)+(w(j)/2);
   Yb(1,j)=y(j)-(h(j)/2);
   Yb(2,j)=y(j)+(h(j)/2);
   Yb(3,j)=y(j)+(h(j)/2);
   Yb(4,j)=y(j)-(h(j)/2);
end

colormap(colorcube);      % definition of the color map,
brighten(0.5);            % brightening
caxis([1 10]);            % definition of its interval of values

% assignment of colors at nodes

color=zeros(1,length(x)); % vector of colors of nodes
for k=1:length(x)         %
   switch nature(k)       %
   case (-1)              %
      color(k)=2.2;       % block node: yellow
   case (-2)              %
      color(k)=2.6;       % normal node: green
   case(-3)               %
      color(k)=9.9;       % background of the legend
   otherwise              %
      color(k)=4.6;       % duplicate node: light blue
   end                    %
end                       %

% the case of three boxes

if length(x)==3
   xagg=[0 0 0 0]';
   yagg=[0 0 0 0]';
   Xb=cat(2,xagg,Xb);
   Yb=cat(2,yagg,Yb);
   color=[9.9 color];
end
