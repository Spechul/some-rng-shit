function play(Pre,Post,M0)
% PLAY: Token Game Animation 
%       *************************************************
%                          ## SYNTAX ##
%
%       PLAY(Pre,Post,M0):
%         Given a Petri Net with Pre,Post matrices and the
%         initial marking M0 this function shows an interactive
%         simulation of its dynamical behaviour.  
%           
%       See also SHOW,TREE,GRAPH

r=2;
while r>1
   [m,n]=size(Pre);
   M=M0';
   k=0;
   fprintf('*******************************************\n\n') 
   fprintf('\nPress "0" to quit PLAY\n\n')
   fprintf('--------- Step 0 ---------\n Marking=\n') 
   for i=1:m,
      fprintf('         %d\n',M(i))
   end;
   fprintf('\n  Transitions enabled to fire: ')
   ab = [];
   for j=1:n,
      if M >=Pre(:,j)',
         fprintf(' %d',j)
         ab= [ab j]; 
      end;
   end;
   j=input('\n Which Transition fires ? ');
   if j==0
      r=1;
   end
   while j>0,
      if min((ab - j).^2)==0,
         M=M-Pre(:,j)'+Post(:,j)';
         k=k+1;
         fprintf('--------- Step %d ---------\n Marking=\n',k) 
         for i=1:m,
            fprintf('         %d\n',M(i))
         end;
         fprintf('\n Transitions enabled to fire: ')
         ab = [];
         for j=1:n 
            if M>=Pre(:,j)',
               fprintf(' %d',j)
               ab= [ab j];
            end;
         end;
         if isempty(ab)
            fprintf('\nThis is a block marking!!\n')
            j=input('\n Press "0" to start again or "1" to quit ');
            if j==0
               break
            end
            if j==1
               return
            end
         else
            j=input('\n Which Transition fires? ');
            if j==0
               r=1;
            end
         end
      else 
         j=input(' ***Not enabled to fire!*** Insert again? ');      
      end;
   end;
end   
