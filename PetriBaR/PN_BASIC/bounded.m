function [b,P]=bounded(I1,I2,I3)
% BOUNDED : This function determines if a P/T System and its places are bounded. 
%          ***********************************************************************
%                                  ## SYNTAX ##
%
%          b=BOUNDED(G): 
%            Given a Petri Net with Reachability/Coverability Graph G   
%            this function returns: 
%                b=1 : if the net is  bounded; 
%                b=0 : if the net is unbounded.            
%
%          [b,P]=BOUNDED(G): 
%            It returns also a place vector P such that P(p) is the bound
%            on place p or is inf if p is unbounded.  
%                         
%          b=BOUNDED(Pre,Post,M0): 
%            It uses the matrices Pre,Post and initial marking M0
%            as input verifying the boundeness on line and stopping
%            as soon as it is verified.
%                 
%
%          See also GRAPH
%
ni=nargin;
no=nargout;
error(nargchk(1,3,ni));
% Determines which syntax is being used for output arguments
switch ni
case 1 % Syntax used with G as input argument
   G=I1;
   if G(end,3)==1
      switch no % Syntax used with B and P as output arguments
      case 2 
         m=G(end,1);
         n=G(end,2);
         [g,d]=size(G);
         G(g,:)=[];
         H=G(:,1:m);
         P=max(H);
         TF=isinf(H);
         Y1=find(TF);
         if isempty(Y1)
            fprintf('\n****The net is bounded!****\n')
            P1=max(P);
            fprintf('\n particularly it is k=%d bounded!\n',P1)
            b=1;
         else
            fprintf('\n****The net is unbounded!****\n')
            b=0;
         end
         for l=1:m
            if max(H(1:g-1,l))<inf;
               fprintf('\n The place P%d is %d bounded!\n',l,max(H(1:g-1,l)))
            else fprintf('\n The place P%d is unbounded!\n',l)
            end   
         end
         fprintf('\n The i-th element of the vector P is equal to "k" if the place P(i) is bounded,otherwise "inf"\n')
%         P;
      otherwise % Syntax used with  only B  as output arguments
         m=G(end,1);
         n=G(end,2);
         [g,d]=size(G);
         G(g,:)=[];
         H=G(:,1:m);
         TF=isinf(H);
         Y1=find(TF);
         if isempty(Y1)
            fprintf('\n****The net is bounded!****\n')
            b=1;
         else
            fprintf('\n****The net is unbounded!****\n')
            b=0;
         end 
      end
   else
      error('You must use G as input argument!!')
   end
case 3   % Syntax used with Pre, Post M0 as input argument
   Pre=I1;
   Post=I2;
   M0=I3';
   [m,n]=size(Post);
   [c,p]=size(M0);
      if (size(Post)==size(Pre))&(c==1)&(p==m),
      T=[M0 0 0];
      N=[M0];
      rN=1;
      rN1=1;
      r=1;
      g=1;
      while r<=g
         if T(r,m+2)==0
            k=1;
            for j=1:n
               if T(r,1:m)>=Pre(:,j)' 
                  g=g+1;
                  T(r,m+1+2*k+1)=j;
                  T(r,m+1+2*k+2)=g;
                  M=T(r,1:m)-Pre(:,j)'+Post(:,j)';
                  a=r;
                  while a~=0  
                     if M >=T(a,1:m),
                        for i=1:m;
                           if M(i)>T(a,i)
                              M(i)=inf;
                              T(:,m+2)=[]; %print the tree
                              M				 %print the first unbounded marking
										fprintf('\n****The net is unbounded!****\n')
                              b=0;
                              return
                           end
                        end
                        a=1;
                     end
                     a=T(a,m+1);
                  end
                  T(g,1:m)=M;
                  T(g,m+1)=r;
                  N(rN,m+1)=rN1;
                  N(rN,m+2)=rN;
                  for w=1:rN    
                     if N(w,1:m)==T(g,1:m)
                        T(g,m+2)=w;
                        T(g,m+3)=N(w,m+1);
                        break
                     end
                  end
                  k=k+1;
                  if T(g,m+2)==0 
                     M1=[M 0 0];
                     N=[N;M1];
                     rN=rN+1;
                     rN1=g;
                     N(rN,m+1)=rN1;
                     N(rN,m+2)=rN;
                  end               
               end
            end
            if k==1,
               T(r,m+2)=-1;
               T(r,m+3)=-1;
            else
               T(r,m+2)=-2;
               T(r,m+3)=-2;  
            end
         end
         r=r+1;
      end 
      T(:,m+2)=[]; %print the tree
      fprintf('\n****The net is bounded!**** \n')
      b=1;
   elseif size(Post)~=size(Pre)
      fprintf('\n Different matrice''s dimension between Pre and Post!\n')
      fprintf('\n Insert Pre and Post again\n')
   else
      fprintf('\n ERROR! Initial marking dimensions wrong!!\n\n')
   end
end