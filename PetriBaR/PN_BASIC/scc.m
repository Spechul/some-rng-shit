function comp = scc(G)
%SCC: this function computes the strongly connected components (SCC) in a 
%     coverability or reachability graph of a Petri Net.
%***************************************************************************
%                             ## SYNTAX ##
%   COMP = SCC(G)
%     G is a Reachability/Coverability graph  in the format  returned by
%     function  GRAPH.  COMP is a  row vector  in which  every component 
%     represents a node in the graph, or, identically,  a marking of the 
%     Petri Net. A flag identifies the component, i.e. two markings with 
%     same flag will be from the same SCC.  This flag will have negative 
%     sign if the component is transitory.
%     
%   see also graphPN

%by Filippo Ledda, 2005
[n,m] = size(G);
numPosti = G(n, 1);

col1 = numPosti + 3; 
G1 = G(1:n-1,col1:m);% remove useless rows from G
G2 = G1;
[n,m] = size(G1);% n is the number of nodes of G

% Start of main algorithm: each arc is considered following the paths of
% the graph 
% the graph is visited and we keep memory of the nodes already explored
% considered paths are kept into memory in a stack

index = 1;% stack index
stack(1) = 1;
current = 1;% we first explore node 1 
componenti = eye(n);
visited = zeros (n ,1);% set =1 if the node is correctly in the stack
visited(1) = 1;
studied(1:n) = 0;
comp(1:n) = 0;
globalFlag = 1;
equiv = sparse([]);% matrix of equivalent components found out
while (1)% the ending condition is that there are no more arcs is consider
    j = 1;   
    while ( j <= m )
       
        if (G1(current,j) == 0)    
            break;% there are no more transitions in this node
        end
        
        if ( G1(current,j) ~= -1 )% we take the transition of the current node if it has no label
            G1(current,j) = -1; % we tag the transition replacing its value wiht -1
            current = G1(current, j + 1); % the node is that one to whom the last transition brings
            j = 1;
            if( current == stack(index) ) % if it is a self loop 
                %display('self loop!!!');%debug
                j= j + 2;% we go to the next step of the loop because 
                         % the transition was a self loop    
            else
                
                if( visited(current) == 1)% if we meet a node that has already been explored in this path
                    %display('node already visited!!');%debug
                    i = index;
                    while( i > 0)% go down along the stack until the position of the visited node is found
                        if( stack(i) == current) 
                            break
                        end
                        i = i - 1;
                    end
                    
                    if(comp(current) ~= 0)
                        flag = comp(stack(i));
                    else
                        % new component found out: put a new flag
                        comp(stack(i)) = globalFlag;
                        flag = globalFlag;
                        globalFlag = max(comp) + 1;
                    end
                    
                    while( i < index ) 
                        i = i + 1;
                        if( comp(stack(i)) == 0 )
                            comp(stack(i)) = flag; 
                        elseif( comp(stack(i)) ~= flag)
                            % 2 equivalent components have been found out
                            tmp = min( comp(stack(i)), flag);
                            globalFlag = max( comp(stack(i)), flag);
                            flag = tmp;
                            for k = 1:n
                                if(comp(k) == globalFlag)
                                    comp(k) = flag;
                                end
                            end  
                        end
                    end
                    current = stack(index);
                    
               elseif(studied(current) == 1 & comp(current) ~= 0)
                    %display('node already investigated!!');
                    if(comp(current) ~= 0)
                        i = index;          
                            while( comp(stack(i)) ~= comp(current))% we go down along the stack until the position of a node of
                                % the same component of the considered node is found out
                                i = i - 1;  
                                if(i == 0) 
                                    break;
                                end                                   
                            end                                                         
                        if( i ~= 0)% if different from 0 a node in the stack belonging to the same component of the one at hand has been found out
                            flag = comp(stack(i));
                            while( i < index )
                                i = i + 1;
                                if( comp(stack(i)) == 0 )
                                    comp(stack(i)) = flag;
                                elseif( comp(stack(i)) ~= flag) 
                                    tmp = min( [comp(stack(i)), flag]);
                                    globalFlag = max( [comp(stack(i)), flag]);
                                    flag = tmp;
                                    for k = 1:n
                                        if(comp(k) == globalFlag)
                                            comp(k) = flag;
                                        end
                                    end  
                                end
                            end
                            current = stack(index);  
                        end
                    end
                        
                else % if we find out a node not visited
                    %display('node not visited yet!');
                    visited(current) = 1;% we tag the node as visited
                    %push current in stack
                    index = index + 1;
                    stack(index) = current;
                end
            end
        else
        j= j + 2;% we move to the next step of the loop since the transition was tagged
        end
    end

    % if a loop is finished, from such a node it is not possible to move to
    % any other node (all nodes are tagged)
    if(index == 1)
        break;% if there are no more transitions at the root node, the algorithm stops
    else
        if(visited(current) == 1)
            %display('pop stack!')
            studied(current) = 1;
            %pop stack 
            visited(current) = 0;
            stack(index) = 0;
            index = index - 1;           
        end
        current = stack(index);%  restart from the previous branch
    end
end

 
done = comp; 

% scctime1 = cputime - t1

% number of entries composed by a single node
globalFlag = max(comp) + 1;
for i = 1:n
    if(comp(i) == 0)
        comp(i) = globalFlag;
        globalFlag = globalFlag + 1;       
    end
end


% finds the transient and the absorbing components (negative sign for the transient componentssegno negativo per comp transitorie)
currComp = 0;
for i = 1:n
    for j = 1:2:m
         if ( G2(i, j) ~= 0)
             if ( comp(G2(i, j+1)) ~= comp(i) )
                 if(comp(i) > 0)
                     comp(i) = - comp(i);
                 end
             end
         end
     end
 end

%if a term belongs to a transient component, then also the other terms belong to it
%vector done contains zero in the terms already studied and/or with a single component
for i = 1:n
    if(comp(i) < 0 & done(i) ~= 0)
        for j = 1:n
            if(comp(j) == -comp(i))
                comp(j) = -comp(j);
                done(j) = 0;
            end
        end
    end
end
            
                   