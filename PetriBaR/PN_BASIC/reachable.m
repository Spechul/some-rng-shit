function [R,sigma,Ms]=reachable(I1,I2,I3,I4)
% REACHABLE:This function determines if a marking of a P/T system is reachable.
%           *********************************************************************
%                                  ## SYNTAX ##
%
%           R=REACHABLE(X,M): 
%             Let X be the Reachability/Coverability Tree or the 
%             Reachability/Coverability Graph of a Petri Net System
%             <N,M0>, and let M be a marking.
%             This function returns: 
%                R = 1 : if M is reachable from the initial marking M0;
%                R = 0 : otherwise. 
%           
%           [R sigma]=REACHABLE(X,M): 
%             If M is reachable with the sequence
%             M0 [ t_a1 > M1 [ t_a2 > M2 ... M(k-1) [ t_ak > M 
%             this function returns also sigma = [ a1 a2 .... ak ]
%
%           [R sigma Ms]=REACHABLE(X,M): 
%             The matrix Ms contains:
%             Ms = [ M0'; M1'; ... M(k-1)';M' ]
%
%           When inovoked with the syntaxes:
%          
%           [R=REACHABLE(Pre,post,M0,M)
%
%           [R,sigma]=REACHABLE(Pre,Post,M0,M)
%          
%           [R,sigma,Ms]=REACHABLE(Pre,Post,M0,M)
%           
%             it uses the matrices Pre,Post and initial marking M0
%             as input building the reachability tree / graph 
%             on line and stopping as soon as M is reached.
%       
%
%           See also TREE,GRAPH
ni=nargin;
error(nargchk(2,4,ni)); 
% Determines which syntax is being used for input arguments
switch ni
case 2 %*****Syntax used when the input arguments are T/G and M*******
   X=I1;
   M=I2';
   m=X(end,1);
   [s,s1]=size(X);
   p=0;
   sigma=[];
   Ms=[];
   x=0;
   R=1;
   c=0;
   for g=1:s   %--------------step 1: Is M in reachability T or G ??--------
      if M(1,1:m)==X(g,1:m)
         x=1;
         f=g;
         break
      end
   end
   if x>0 %----------if yes search for sigma and for Ms-----------
      fprintf('\n The marking requested:\n\n')
      disp(M')
      fprintf(' is reachable!\n')
      a=X(f,m+1);
      if a==0
         fprintf('\n The marking requested is equal to initial marking\n')
         Ms=M';
         c=1;
         %break
      end
      while a~=0
         for b=m+4:2:s1,
            if X(a,b)==f,              
               sigma=[X(a,b-1),sigma]; 
               Ms=[X(a,1:m);Ms];       
            end,
         end
         f=a;
         a=X(f,m+1);
      end
      if c==0
      Ms=[Ms;M]';
      end
      return  %--------EXIT-----------
  end       
   for f=1:s,% ----- If M doesn't appear in T(G): COVERABILITY CHECK-------
      if X(f,1:m) >= M,
         u = 1;
         for j=1:m,
            if (X(f,j) ~= inf) & (X(f,j) ~= M)
               u = 0;
               break
            end
         end
         if u == 1, 
            cov=X(f,1:m);
            p=1;
            break
         end
      end
   end
   if p>0 																			    
      a=X(f,m+1);
      while a~=0
         for b=m+4:2:s1,
            if X(a,b)==f,              
               sigma=[X(a,b-1),sigma]; 
               Ms=[X(a,1:m);Ms];
            end,
         end
         f=a;
         a=X(f,m+1);
      end
      Ms=[Ms;cov]';				
      fprintf('\nThe marking requested\n\n')
      disp(M')
      fprintf('\ncould be reached!\n')
      fprintf('\nIt''s covered by the marking: \n')
      disp(cov')
   else 
      fprintf('\n The marking requested\n\n')
      disp(M')
      fprintf('\n is not reachable!\n')
      R=0;
   end
case 4 %******Syntax used when the input arguments are Pre,Post,M0 ,M******
   Pre=I1;
   Post=I2;
   M0=I3';
   M=I4';
   R=1;
   [m,n]=size(Post);
   [c,p]=size(M0);
   if M==M0
      fprintf('\n The marking requested is a reachable marking!\n')
      fprintf('\n Initial marking is equal to the requested one!\n')
      return
   end
   if (size(Post)==size(Pre))&(c==1)&(p==m),
      T=[M0 0 0 0];
      N=[M0 0 0 ];
      rN=1;
      rN1=1;
      r=1;
      g=1;
      sigma=[];
      Ms=[];
      while r<=g
         if T(r,m+2)==0 
            k=1;
            for j=1:n 
               if T(r,1:m)>=Pre(:,j)' 
                  g=g+1;
                  T(r,m+1+2*k+1)=j;
                  T(r,m+1+2*k+2)=g; 
                  M2=T(r,1:m)-Pre(:,j)'+Post(:,j)';                               
                  a=r;
                  a1=1;
                  while a~=0  
                     if M2 >=T(a,1:m), 
                        for i=1:m;
                           if M2(i)>T(a,i)
                              M2(i)=inf;
                           end
                        end
                        a=1;
                     end
                     a=T(a,m+1);
                  end
                  T(g,1:m)=M2;
                  T(g,m+1)=r;
                  N(rN,m+1)=rN1;
                  N(rN,m+2)=rN;
                  if M==M2
                     a1=T(g,m+1);
                     fprintf('\n The marking requested:\n')
                     disp(M')
                     fprintf('\n is reachable!')
                     [s1,s2]=size(T);
                     while a1~=0
                        for b=m+4:2:s2,
                           if T(a1,b)==g,              
                              sigma=[T(a1,b-1),sigma]; 
                              Ms=[T(a1,1:m);Ms];       
                           end,
                        end
                        g=a1;
                        a1=T(g,m+1);
                     end
                     Ms=[Ms;M];
                     return
                  end                 
                  for w=1:rN    
                     if N(w,1:m)==T(g,1:m)
                        T(g,m+2)=w;
                        T(g,m+3)=N(w,m+1);
                        break
                     end
                  end
                  k=k+1;
                  if T(g,m+2)==0 
                  M1=[M2 0 0];
                  N=[N;M1];
                  rN=rN+1;
                  rN1=g;
                  N(rN,m+1)=rN1;
                  N(rN,m+2)=rN;
                  end
               end
            end
            if k==1, 
               T(r,m+2)=-1;
               T(r,m+3)=-1;
            else
               T(r,m+2)=-2;
               T(r,m+3)=-2;
            end
         end
         r=r+1;
      end
   end 
   %*-----------------COVERABILITY CHECK---------
   [s,s1]=size(T);
   p=0;
   for f=1:s,
      if T(f,1:m) >= M,
         u = 1;
         for j=1:m,
            if (T(f,j) ~= inf) & (T(f,j) ~= M)
               u = 0;
               break
            end
         end
         if u == 1, 
            cov=T(f,1:m);
            p=1;
            break
         end
      end
   end
   if p>0
      a=T(f,m+1);
      while a~=0
         for b=m+4:2:s1,
            if T(a,b)==f,              
               sigma=[T(a,b-1),sigma]; 
               Ms=[T(a,1:m);Ms];
            end,
         end
         f=a;
         a=T(f,m+1);
      end
      Ms=[Ms;cov];			
      fprintf('\n The marking requested\n\n')
      disp(M')
      fprintf('\ncould be reached!\n')
      fprintf('\nIt''s covered by the marking: \n')
      disp(cov')
   else
      fprintf('\n The marking requested\n\n')
      disp(M')
      fprintf('\n is not reachable!\n')
      R=0;
   end
end


   

   