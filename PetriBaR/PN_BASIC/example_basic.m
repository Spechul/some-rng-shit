function example_basic
% This function corresponds to Example 2.1 of the WODES18 paper

%% Input the PN system
%(bounded)
Pre1=[1 0 0;0 1 1;0 0 0];
Post1=[0 1 0; 1 0 0; 0 0 1];
M01=[1 0 0]';

%(unboudned)
Pre2=Pre1;
Post2=[0 2 0; 1 0 0; 0 0 2];
M02=M01;

%% Check if the net system is bounded
%[b1]=bounded(Pre1,Post1,M01)
[b2]=bounded(Pre2,Post2,M02)

%% Build the RG/CG
%G1=graphPN(Pre1,Post1,M01);
G2=graphPN(Pre2,Post2,M02)

%% Build the RT/CT
%T1=tree(Pre1,Post1,M01);
%T2=tree(Pre2,Post2,M02);

%% Show the RG/CG
%show(G1);
%show(G2);

%% Draw the RT/CT
%plottree(T1);
%plottree(T2);

% %% Using GUI to input the PN system
% InPut;
% [Pre,Post,M0]=getMatrix;
% [b]=bounded(Pre,Post,M0)
end