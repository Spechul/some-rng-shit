function show(G)
%SHOW: This function shows Reachability/Coverability Graph of a P/T System.
%      *******************************************************************
%                          ## SYNTAX ##
%
%      SHOW(G)  
%        This function shows how obtain a Petri Net's 
%        Reachability/Coverability Graph G step-by-step
%        starting from the initial marking and following all
%        enabled transions and relative reached markings.
%
%      See also: PLAY,GRAPH
ni=nargin;
error(nargchk(1,1,ni));
m=G(end,1);
n=G(end,2);
G(end,:)=[];
G(:,m+1:m+2)=[];
G0=G; 
[s,t]=size(G);
   G0(:,1:m)=[];
   [b,c]=size(G0);
   for f=1:s
      fprintf('\n Marking M%d =\n',f)
      disp((G(f,1:m))')
      fprintf('\n Transitions enabled to fire:\n\n')
      e=1;
      while e<=c
         if G0(f,e)>0
            fprintf('\n T%d -->  ',G0(f,e))
            e=e+1;
            fprintf(' M%d \n\n ',G0(f,e))
            e=e+1;
         else e=e+2;
         end
      end
   end
   fprintf('\nShow Graph:\n')
   disp(G) 
   fprintf('\n Net places m = %d',m)
   fprintf('\n Net transitions n = %d\n',n)
   fprintf('\n Data structure :\n')
   fprintf('\n M(P1) M(P2) ... M(Pi) **** T1--M1 **** T2--M2 **** T3--M3 **** ... **** Ti--Mi\n') 
   fprintf('\n Where: M(P1)...M(Pi) are net places with their own markings\n')
   fprintf('        T1--M1 **** T2--M2 **** Ti--Mi are enabled transitions & relative reached markings\n')
