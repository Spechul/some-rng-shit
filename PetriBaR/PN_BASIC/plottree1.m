function plottree1(T)
%PLOTTREE1 reachability/coverability tree
%   Syntax: PLOTTREE1(T)
%
%   Plot the reduced reachability/coverability tree given a tree T of Petri
%   Net obtained with the command T=TREE(Pre,Post,M0).
%   It differs from the function PLOTTREE because it uses self-loops, with
%   the effect of a more compact representation of the tree.
%
%   NOTE: For trees with a huge number of nodes at the same depth,
%         markings may overflow from nodes. In such a case, it is sufficient to 
%         to press the left key of the mouse to enlarge the area of the 
%         figure on which the pointer is and easily read the marking;
%         the right key allows to come back to normal representation
%         (funtion ZOOM is active). In some cases, a good solution can be
%         that of enlarging the window of the figure.
%
%   Based on the code of function TREEPLOT (Copyright (c) 1984-97
%   by The MathWorks, Inc. $Revision: 5.7 $  $Date: 1997/04/08 06:40:28 $ ).
%
%   ATTENTION: this function recalls as subroutines the functions ADDBOX,
%               TREELAY, DEPTH, DEC2STR. If these are modified of are not 
%               present in the same folder where PLOTTREE1 is, 
%               the program will not work.
%               
%   See also TREELAY, TREE, DEPTH, ADDBOX, PLOTREE.
%
%   Author: Diego Mura, 2003, University of Cagliari
%           

clf;

[m,n]=size(T);
nature=T(1:m-1,T(m,1)+2)'; % extrapolates from T the column m+2 minus the last term
p=T(1:m-1,T(m,1)+1)';      % extrapolates from T the vector of pointers to parents

d=depth(p); % computes the depth of nodes

nodel=find(~((nature>0)&(nature==p))); % nodes to represent

% dcomputation of the nodes to which the self-loop belongs

del=find(((nature>0)&(nature==p)));    % nodes not to be represented 
pardel=p(del);                         % nodes with self-loops in P

tag=zeros(1,length(p));
tag(nodel)=1;            % the nodes stored up are tagged 1
tag(pardel)=2;           % the nodes with self-loop are tagged 2
indcompact=find(tag);
compact=tag(indcompact);    % tags 0 are removed
indlazo=find(compact==2);   % indices of nodes with lazo in PBIS

% updating of data to be used

dbis=d(nodel);
naturebis=nature(nodel);

% rewriting of the vector of parents with the new numbering of nodes

pbisbuf=p(nodel);
pbis=zeros(1,length(nodel));

for q1=1:length(nodel)
   rew=find(indcompact==pbisbuf(q1));
   if length(rew)~=0;
      pbis(q1)=rew;
   end
end


% rewriting of matrix T according to the new numbering of nodes

rownodel=[nodel m]; 
Tbis=T(rownodel,:);    % reduced matrix Tbis (include the last row of T)
[m1,n1]=size(Tbis);

% computation of the coordinates of the nodes of the tree
[x,y,h]=treelay(pbis,dbis);

f = find(pbis~=0);  % indices of the non-root nodes 
pp = pbis(f);       % indices of the parents of the non-root nodes
X = [x(f); x(pp); repmat(NaN,size(f))];
Y = [y(f); y(pp); repmat(NaN,size(f))];
X = X(:);   % puts everything in a column
Y = Y(:);   %    "     "   "    " 


l = length(pbis);
plot (X, Y, 'r-');     % drawing of arcs

xlabel(['altezza = ' int2str(h)]);
axis([0 1 0 1]);

% computes the maximum number of nodes in a level of the tree

numnodes=0; 
for k=1:max(dbis)
   dummy=find(dbis==k);
   v=length(dummy);
   numnodes=max(numnodes,v);
end

% adds to the graph colored boxes and self-loops

if numnodes==0   % avoids to perform divisions by 0 
   numnodes=1;   % if the tree only contains the root node (max(d)==0!!)
end

maxlargh=min(15*0.012*Tbis(m1,1),2/(2.8*numnodes));% 0.012 is the estimated lenght of the character
largh=maxlargh*ones(1,length(pbis)); % width of the boxes
maxalt=min(0.1,0.4/(h+1));
alt=maxalt*ones(1,length(pbis));          % hight of the boxes
[Xb,Yb,color]=addbox(x,y,largh,alt,naturebis); % coordinaets of the vertices

xc=Xb(3,indlazo);                % self-loops are centered in the left top corner of the boxes
yc=Yb(3,indlazo);                
wc=0.05*ones(length(xc));
hc=0.05*ones(length(xc));
unused=9.9*ones(length(xc));   
[Xc,Yc,garbage]=addbox(xc,yc,wc,hc,unused);
[m2,n2]=size(Xc);
Xc1=[Xc;Xc(1,:);repmat(NaN,1,n2)];
Yc1=[Yc;Yc(1,:);repmat(NaN,1,n2)];
Xc1=Xc1(:);
Yc1=Yc1(:);
hold on
plot(Xc1,Yc1,'r');
plot(xc+0.01,yc-0.025,'r<');  % add the top of the arrows to self-loops

patch(Xb,Yb,color);    % finally visualize boxes

% definition of the type and the dimension of the fonts of the tags

if numnodes<=7
   s=9; 
   carattere='arial';
else
   s=7;
   carattere='verdana';
end

if numnodes<10
   parametro=1;
else
   parametro=0.5;
end

% insert the markings in the boxes, trying to center them

for i=1:length(pbis) 
   marcatura=Tbis(i,1:Tbis(m1,1));
   etichetta=dec2str(marcatura);
   etichetta=strrep(etichetta,'Inf','w'); % replace 'Inf' with 'w'
   text(x(i)-length(etichetta)*0.006*parametro,y(i),etichetta,'fontsize',s,'fontname',carattere);
end

% put the transitions slightly on the left with respect to the middle of the arc


for ii=1:length(pbis)
   num=0;
   jj=Tbis(m1,1)+4; % pointer to the column of nodes reached from the transition at hand
   tt=Tbis(m1,1)+3; % pointer to the column of the transitions at hand
   while (tt<n)&(Tbis(ii,tt)~=0)
      if isempty(find(indcompact==Tbis(ii,jj))) % if the pointed node, is no more there,
         xt=Xb(3,ii)+0.035*parametro;           % it means that the current node
         yt=Yb(3,ii)+0.02-0.03*num*parametro;   % has a self-loop !!
         num=num+1;
      else
         nuovo=find(indcompact==Tbis(ii,jj));
         if abs(x(ii)-x(nuovo))<0.03
            xt=(x(ii)+x(nuovo))/2-parametro*0.025;
         else
            xt=(x(ii)+x(nuovo))/2-parametro*0.04;
         end 
         yt=(y(ii)+y(nuovo))/2;              
      end
      tran=Tbis(ii,tt);	
      transizione=num2str(tran);
      transizione=strcat('t',transizione);
      text(xt,yt,transizione);
      tt=tt+2;
      jj=jj+2;
   end
end

title('Albero di raggiungibilità/copertura');

% construction of the legend

xlegenda=[.85 .745 .745 .745];
ylegenda=[.925 .965 .925 .885];
colorlegenda=[-3 -2 -1 0];
xdim=[0.28 0.05 0.05 0.05];
ydim=[0.13 0.03 0.03 0.03];
[Xleg,Yleg,colorleg]=addbox(xlegenda,ylegenda,xdim,ydim,colorlegenda);
patch(Xleg,Yleg,colorleg);

text(0.79,0.965,'nodo normale');
text(0.79,0.925,'nodo di blocco');
text(0.79,0.885,'nodo duplicato');

% removes the values from the axes

set(gca,'XTicklabel',[''],'YTicklabel',[''],'XTick',[],'YTick',[]);

hold off;

zoom on; % activates the function ZOOM