function [v1,Mf] = firable1(I1,I2)
% FIRABLE1: This function determines if a sequence of transitions of a P/T System is enabled to fire.
%          *****************************************************************************************      
%                                  ## SYNTAX ##
%
%          v1=FIRABLE1(X,sigma): 
%            Let X be the Reachability/Coverability Tree or the 
%            Reachability/Coverability Graph of a Petri Net System
%            <N,M0> and let sigma be a sequence of transitions 
%            sigma = [ a1 a2 .... ak ]. 
%            This function returns 
%               v1 = 1 : if sigma is firable from a marking M;
%               v1 = 0 : otherwise. 
%
%          [v1,Mf]=FIRABLE1(X,sigma):
%            This function returns also a Matrix Mf that contains:            
%            Mf = [ M0'; M1'; ... M(k-1)'; Mk' ].
%
%          See also FIRABLE,GRAPH,TREE 
ni=nargin;
no=nargout;
error(nargchk(2,2,ni));
% Determines which syntax is being used for output arguments
X=I1;
Sigma=I2;
[r,c]=size(X);
m=X(end,1);
n=X(end,2);
M0=X(1,1:m);
Mf=[];
a1=[];
[y,x]=size(Sigma);
if max(Sigma)>n
   error('One sequence''s transition  isn''t in the net!\n')
   return
end 
for j=1:r-1
   k=1;
   while k <= x,
      v1=0;
      for b=m+3:2:c
         if Sigma(k)==X(j,b)
            if k==1
               a1=X(j,1:m);
            end
            a=X(j,b+1);
            Mf=[Mf;X(a,1:m)];
            j=a;
            v1=1;        
            break
         end
      end
      if v1==0, 
         k=x+1;
      else
         k=k+1;
      end
   end
   if v1==1
      break
   end
end
if v1==0,
   fprintf('\n The sequence isn''t enabled to fire!\n')
else
   fprintf('\n The sequence is enabled to fire!\n')
end
Mf=[a1;Mf];
