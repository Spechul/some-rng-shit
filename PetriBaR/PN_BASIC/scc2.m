function comp = scc2(G)
%comp = scc2(G)
%   This function determines the maximal strongly connected components in a
%   reachability/coverability graph given in the format produced by
%   function graphPN. 
%   Its output is a row vector whose i-th entry corresponds to the node,
%   and whose norm identifies the component to which it belongs. A negative
%   sign is associated with ergodic components.


[n,m] = size(G);
numPosti = G(n, 1);

col1 = numPosti + 3;  

G = G(1:n-1,col1:m); % removes from G what is not useful 
[n,m] = size(G);% n is the number of nodes of G
t1 = cputime;
% computation of the adjiacency matrix 
A = zeros(n);
for i = 1:n
    for j = 1:2:m  % computes the row of A
        if (G(i,j) ~= 0) 
            A(i, G(i, j+1)) = 1;
        end
    end
end


% reachability matrix in n-1 steps
% the element ragg(i,j) is not null if from the i-th marking it is possible to reach 
% the j-th marking 
ragg = A + eye(n);
A1 = A;
for i = 2:n-1
    A1 = A1 * A;
    ragg = ragg + A1;
end


% computation of strongly connected components
% the matrix has non null (i,j) entry if i and j belong to the same
% strongly connected component

for i = 1:n
    componenti(i,:)  = ragg(i,:) & ragg(:,i)';
end
componenti;

currComp = 0;
comp(1:n) = 0;
for i = 1:n
    if(comp(i) == 0)
        currComp = currComp + 1;
        for j = 1:n
            if (comp(j) == 0)
                if (componenti(i,j) ~= 0)
                    comp(i) = currComp;
                    comp(j) = currComp;
                end
            end
        end
    end
end



% computation of ergodic components
for i = 1:n 
    for j = 1:n
       if ( (ragg(i,j) ~= 0) )
           if(comp(i) ~= comp(j))
               comp(i) = - comp(i);
               break 
           end
       end
   end
end
