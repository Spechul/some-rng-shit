% Petri Net Toolbox.
%
% Version 4   June 2011
%
% GENERAL PURPOSE
%   incidence    - returns the incidence matrix of a P/T net from Pre and Post
%   graphPN        - builds the Reachability/Coverability Graph of a P/T system
%   tree         - builds the Reachability/Coverability Tree of a P/T system
%   show         - shows the Reachability/Coverability Graph of a P/T system
%   show1        - shows the Reachability/Coverability Graph of a P/T
%                  system in a compact format
%   play         - token game simulation
%   plotree      - draws on screen the Reachability/Coverability Tree of a P/T system 
%   plotree1     - draws on screen the Reachability/Coverability Tree of a
%                  P/T system using self-loops 
%   scc          - computes the strongly connected components of a Reachability/Coverability Graph
%   scc2         - computes the strongly connected components of a Reahability/Coverability Graph 
%                  using an algorithm different from the one used in scc 
%
% BEHAVIORAL ANALYSIS 
%   bounded      - checks if a P/T system and its places are bounded 
%   live         - checks if a P/T system and its transitions are live
%   dead         - checks deadlock markings in a P/T system 
%   reachable    - checks if a marking of a P/T system is reachable 
%   firable      - checks if a sequence of transitions of a P/T system is enabled from M0
%   firable1     - checks if a sequence of transitions of a P/T system is enabled 
%                  from some reachable marking
%   repetitive   - checks if a P/T system has repetitive sequences 
%                  (increasing or stationary)
%   reversible   - checks if a P/T system is reversible
%   
% STRUCTURAL ANALYSIS 
%   pinvar       - P-invariant (or P-semiflow) computation
%   tinvar       - T-invariant (or T-semiflow) computation
%   siphons      - siphons computation
%   traps        - traps computation
%
% CONTROL WITH GMECS (Generalized Mutual Exclusion Constraints)
%   checkgmec     - controllability test for a GMEC (w,k)
%   checkgmec2    - controllability test for a set of GMEC (W,K)
%   ckcont        - controllability test for a GMEC (w,k)
%   ckcont2       - controllability test for a set of GMEC (W,K)
%   ckobs         - observability test for a GMEC (w,k)
%   clnet         - generates a closed loop net given a monitor place
%   closedloop    - generates a closed loop net given a set of monitor places
%   controllablegmec - finds all minimally restrictive controllable GMECS (wi,ki)
%                   that satisfy the uncontrollable GMEC (w,k)
%   gmecsbs       - GMEC Step By Step Calculation
%   monitordesign - designs a closed loop net system given a GMEC (w,k)
%   monitorplace  - Incidence matrix and initial marking of the monitor place
%                   for the controllable GMEC (w,k)
%   monitorplace2 - Incidence matrix and initial marking of the monitor places
%                   for the set of controllable GMEC (W,K)
%
%       Copyright (c) 2000 by Riccardo Castrignano
%                     2001 by Marco Ambu
%                     2001 by Alessandro Giua
%                     2002 by ARP students  
%                     2003 by Massimo Gessa
%                     2003 by Diego Mura
%                     2005 by Filippo Ledda