function plottree(T)
%PLOTTREE reachability/coverability tree
%   Syntax: PLOTTREE(T)
%
%   Provides the graphical representation of a reachability/coverability tree T
%   of a Petri net obtained using the command T=TREE(Pre,Post,M0).
%   Differs from the analogous PLOTTREE1 since it does not use self-loops.
%
%   NOTE: For trees with a high number of nodes at the same depth
%         the markings can exit from the boxes associated to nodes. In such
%         a case, it is sufficiente to press on the left key of the mouse
%         to enlarge the area of the figure on which the pointer is
%         positioned and confortably read the marking; the right key allows
%         to go back to the normal representation. In some cases, a good solution 
%         may be that of enlarging the window of the figure.
%
%   Based on the code of the function TREEPLOT (Copyright (c) 1984-97
%   by The MathWorks, Inc. $Revision: 5.7 $  $Date: 1997/04/08 06:40:28 $ ).
%
%   ATTENTION:  this function calls as subroutines the functions ADDBOX,
%               TREELAY, DEPTH, DEC2STR. If such functions are modified or 
%               are not contained in the same folder where PLOTTREE is, 
%               the program will not work.
%               
%   See also TREELAY, TREE, DEPTH, ADDBOX, PLOTTREE1.
%
%   Author: Diego Mura, 2003, University of Cagliari
%           e-mail: diegolas@inwind.it

clf;

[m,n]=size(T);
nature=T(1:m-1,T(m,1)+2)'; % estrapolates from T the column m+2 minus the last term
p=T(1:m-1,T(m,1)+1)';      % estrapolates from T the vector of pointers to the parents

d=depth(p); % computes the depht of the varius nodes

[x,y,h]=treelay(p,d); % computes the coordinates of the nodes of the tree

f = find(p~=0);  % indices of the not-root nodes
pp = p(f);       % indices of the partents of the not-root nodes
X = [x(f); x(pp); repmat(NaN,size(f))];
Y = [y(f); y(pp); repmat(NaN,size(f))];
X = X(:);   % mette tutto in colonna
Y = Y(:);   %    "     "   "    " 


l = length(p);
plot (X, Y, 'r-');     % draw the arcs

xlabel(['depth = ' int2str(h)]);
axis([0 1 0 1]);

% computes the maximum number of nodes in a level of the tree

numnodes=0; 
for k=1:max(d)
   dummy=find(d==k);
   v=length(dummy);
   numnodes=max(numnodes,v);
end

% adds colored rectangles to the plot

if numnodes==0   % avoides the division for 0 that would occur
   numnodes=1;   % if the tree contains only the rood node (max(d)==0!!)
end

maxlargh=min(15*0.012*T(m,1),2/(2.8*numnodes));% 0.012 is the estimated length of a character
largh=maxlargh*ones(1,length(p)); % width of the rectangles
maxalt=min(0.1,0.4/(h+1));
alt=maxalt*ones(1,length(p));          % hights of the rectangles
[Xb,Yb,color]=addbox(x,y,largh,alt,nature);
patch(Xb,Yb,color);

% definition of the type and the dimention of the characters of the labels

if numnodes<=7
   s=9; 
   carattere='arial';
else
   s=7;
   carattere='verdana';
end

if numnodes<10
   parametro=1;
else
   parametro=0.5;
end


% includes the markings in the rectangles

for i=1:length(p) 
   marcatura=T(i,1:T(m,1));
   etichetta=dec2str(marcatura);
   etichetta=strrep(etichetta,'Inf','w'); % replaces 'Inf' with 'w'
   text(x(i)-length(etichetta)*0.006*parametro,y(i),etichetta,'fontsize',s,'fontname',carattere);
end

% positions the tranisitions slightly on the left with respect to the medium point of the arc


for ii=1:length(p)
   jj=T(m,1)+4; % pointer at the column of the nodes reached from the transition at hand
   tt=T(m,1)+3; % pointer at the column of the transitions at hand
   while (tt<n)&(T(ii,tt)~=0)
      if abs(x(ii)-x(T(ii,jj)))<0.03
         xt=(x(ii)+x(T(ii,jj)))/2-parametro*0.025;
      else
         xt=(x(ii)+x(T(ii,jj)))/2-parametro*0.04;
      end 
      yt=(y(ii)+y(T(ii,jj)))/2;
      tran=T(ii,tt);	
      transizione=num2str(tran);
      transizione=strcat('t',transizione);
      text(xt,yt,transizione);
      tt=tt+2;
      jj=jj+2;
   end
end

title('Reachablity/Coverability Tree');

% creation of the legend

xlegenda=[.85 .745 .745 .745];
ylegenda=[.925 .965 .925 .885];
colorlegenda=[-3 -2 -1 0];
xdim=[0.28 0.05 0.05 0.05];
ydim=[0.13 0.03 0.03 0.03];
[Xleg,Yleg,colorleg]=addbox(xlegenda,ylegenda,xdim,ydim,colorlegenda);
patch(Xleg,Yleg,colorleg);

text(0.79,0.965,'normal node');
text(0.79,0.925,'blocked node');
text(0.79,0.885,'duplicate node');

% remove the scale values from the axes

set(gca,'XTicklabel',[''],'YTicklabel',[''],'XTick',[],'YTick',[]);

zoom on; % activates the function ZOOM