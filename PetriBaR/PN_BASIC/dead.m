function [D,Mb]=dead(G,Mx)
% DEAD: This function determines deadlock markings in a P/T system.
%        *************************************************
%                          ## SYNTAX ##
%
%        D=DEAD(G,M): 
%          Let G be the Reachability/Coverability Graph G of a Petri 
%          Net System <N,M0>, and let M be a marking.
%          The function returns: 
%              D=1 if M is a deadlock marking; 
%              D=0 if M is not a deadlock marking; 
%              D=2 if M is not a reachable marking. 
%
%        [D,Mb]=DEAD(G): 
%          Given a Petri Net with Reachability\Coverability Graph G,  
%          the function returns also a matrix Mb that contains              
%          deadlock markings as column vectors.
%
%        See also GRAPH   
%
% Last revision: November 2002
ni=nargin;
error(nargchk(1,2,ni));
if G(end,3)==1
   m=G(end,1);
   n=G(end,2);
   % Determine which syntax is being used for output arguments
   switch ni % syntax used with G as input argument
   case 1
      u=0;
      A=[];
      G(end,:)=[];
      G(:,m+1:m+2)=[];
      [rG,cG]=size(G);
      for f=1:rG,
         if G(f,m+1:cG)==0,
            A=[A;G(f,1:m)];
            u=1;
         end,
      end
      if u>0
         fprintf('\nThere are deadlock markings in the net!:\n'),
         D=1;
      else
         fprintf('\nThere aren''t deadlock markings in the net!\n')
         D=0;
      end
      Mb=A';
   case 2 % syntax used with G and M as input argument
      M=Mx';
     % check if M has negative components  
      [c,p]=size(M);
for r=1:p
    if (M(r)<0) error ('\n Error! M must be greater or equal to zero!')
    return ;
   end
end
      m=G(end,1);
      n=G(end,2);
      [s,t]=size(M);
      if (s~=1)|(t~=m)
         error('the marking''s dimension is wrong!')
         return
     end
      p=2;
      G(end,:)=[];
      [rG,cG]=size(G);
      for f=1:rG                    
         if (G(f,1:m)==M) 
             if (G(f,m+2)==-1) p=1;
             else p=0;
             break;
             end
         else p=2;
         end
         
      end
      if (p==1)
         fprintf('\n The marking is a deadlock marking! \n')
         D=1;
      else if (p==0)
         fprintf('\n The marking is not a deadlock marking! \n')
         D=0;
        else fprintf('\n The marking is not a reachable marking! \n')
             D=2;
       end    
     end
   end
else
   error('You must use G as input argument!!')
end





