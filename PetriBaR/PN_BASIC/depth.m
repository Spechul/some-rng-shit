
function D=depth(parent)
%DEPHT of the nodes of a tree
%   Syntax: D=DEPTH(PARENT)
%
%   Given the vector of the paren nodes PARENT it return the vector D of
%   the depth of nodes of the tree, starting from the root,
%   which has depth 0.
%   The vector PARENT has the i-th entry the index of the partent node of 
%   node with index i.
%   Nodes are assumed to be numbered in an increasing order according to their
%   position in the tree, starting from the root (in the top position), 
%   from the left to the right, and from the bottom to the top.
%
%   Author: Diego Mura, 2003, University of Cagliari

n = length(parent);

parent = rem (parent+n, n+1) + 1;  
isaleaf = ones(1,n+1);
isaleaf(parent) = zeros(n,1);     

D=zeros(1,n); 
																
for i=1:n
   if isaleaf(i)
      aliga1=parent(i);
      while aliga1~=n+1
         aliga2=i;
         while aliga2~=aliga1
            D(aliga2)=D(aliga2)+D(aliga1)+1;
            aliga2=parent(aliga2);
         end
         if D(aliga1)~=0
            break
         end
         aliga1=parent(aliga1);
      end
   end
end
