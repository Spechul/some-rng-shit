function C = incidence(Pre, Post)
% INCIDENCE Incidence matrix of the Place/Transition Petri Net (Pre,Post)
%
%   Syntax:
%   C = incidence(Pre, Post)
%
ni = nargin;
no = nargout;
error(nargchk(2,2,ni));
if (size(Post)~=size(Pre))
   fprintf('Error: Pre and Post of different sizes\n');
else
   C=Post-Pre;
   if (no == 0)
      disp(C);
   end
end
% Marco Ambu - 2001 Jan 27