function  y=repetitive(G,Pre,Post,Sigma)
% REPETITIVE :This function determines the repeatitive sequences
%             ( eventually stationary) in a P/T System. 
%             *******************************************************************
%                           ## SYNTAX ##
%
%             y=REPETITIVE(G,Pre,Post,Sigma): 
%               Given a Petri Net with Reachability/Coverability Graph G and
%               Pre,Post matrices,this function verifies if a sequence of transitions
%               sigma = [ a1 a2 .... ak ]
%               is repetitive and/or stationary or not.
%               This function returns:
%                  y=0 if sigma is not repetitive;
%                  y=1 if sigma is repetitive and stationary;
%                  y=2 if sigma is repetitive but not stationary;
%                  y=3 if sigma is not firable.
%     
%             See also GRAPH,FIRABLE,FIRABLE1
ni=nargin;
error(nargchk(4,4,ni));
m=G(end,1);
n=G(end,2);
[rG,cG]=size(G);
Sigma1=zeros(n,1);
[rS,cS]=size(Sigma);
C=Post-Pre;
%*********computation of the firing count vector join to sigma sequence**********
if max(Sigma)>n
   error('One sequence''s transition isn''t in the net')
   return
end
for i=1:cS   
   c=Sigma(1,i);
   Sigma1(c,1)=Sigma1(c,1)+1;
end

%****************repeatitiveness check***********
w=firable1(G,Sigma);
if w==1
   if C*Sigma1>=0
      if C*Sigma1==0
         fprintf('\nThe sequence is repetitive and stationary!\n')
         y=1;
         return
      else
         fprintf('\nThe sequence is repetitive but not stationary!\n')
         y=2;
         return
      end
   else
      fprintf('\nThe sequence is not repetitive!\n')
      y=0;
      return
   end
else
   y=3;
   return
end


   