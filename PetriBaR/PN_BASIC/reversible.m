function RE = reversible(G)
% REVERSIBLE: This function determines if a p/t system is reversible.
%             *************************************************************
%                          ## SYNTAX ##
%
%    RE = REVERSIBLE(G): 
%       Given a Petri Net with Reachability/Coverability Graph G,
%       this function returns:
%        - RE = 0 if the net is not reversible;
%        - RE = 1 if the net is reversible;
%        - RE = 2 if the net may be reversible.
%
%    REVERSIBLE(G):
%       A textual answer will be returned.
%
%     See also GRAPH

%by Filippo Ledda, 2005

[n,m] = size(G);
numPosti = G (n, 1);
numTrans = G (n,2);
sc = scc(G);% computation of the strongly connected components
numComp = max(abs(sc));% number of different strongly connected components
G1 = G(1:n-1, 1:numPosti);% I remove useless rows from G
n = n - 1;% n is the number of nodes of G


if( max(sc) == 1)
    if(nargout == 0)
        fprintf('\n The Reachability graph is strongly connected:\n');
        fprintf('\n The net is reversible!\n');
    else
    RE = 1;
    return
end
elseif (max(G1) < inf)
    if(nargout == 0)
        fprintf('\n The Reachability graph is not strongly connected:\n');
        fprintf('\n The net is not reversible!\n');
    else
        RE = 0;
    end
    return;
else
    covMarks( 1:numComp ) = 0;% it is a vector whose generic entry is = 1 if either the component is 
    % transient or it belongs to at least one marking who w-covers the initial one
    covMarks( abs(sc(1)) ) = 1;
    for i = 2:n %per ogni marcatura
        if( max(G1(i,:)) == inf )
            ok = 1;
            for j = 1:numPosti
                if ( G1(i, j) ~= inf & G1(i, j) ~= G1(1, j) )% if the marking does not w-covers the initial marking
                    ok = 0;
                    break;
                end
            end
            if (ok == 1)
                % if the place w-covers the initial marking
                % we cannot exclude that the net is reversible: if one 0 will remain in covMarks
                % we can exclude that the net is reversible
                covMarks(abs(sc(i)) ) = 1;
            end                                           
        end
        if (sc(i) < 0)
            % if the place belongs to a transient component, such a
            % component should in any case be marked because it does not
            % affect the reversibility
            covMarks(abs(sc(i)) ) = 1;
        end
    end
    
    if( min(covMarks)  == 1 )
        if(nargout == 0)
            fprintf('\n Every absorbent component of Covering graph may contain initial marking: \n');
            fprintf('\n The net may be reversible!\n');
        else
            RE = 2;
        end
        return;
    else
        if(nargout == 0)
            fprintf('\n At least one absorbent component of Covering graph can''t contain initial marking: \n');            
            fprintf('\n The net is not reversible!\n');
        else
            RE = 0;
        end
        return;
    end
end
    
    

                
    
