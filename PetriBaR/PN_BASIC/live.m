function [L,tr]=live(G)
%LIVE: This function determines the liveness levels of a p/t system and  
%      of its transitions. The study is based only on 
%      Reachability/Coverability graph information.
%********************************************************************
%                             ## SYNTAX ##
%
%  L = LIVE(G):
%    Let G be the Reachability/Coverability Graph of a Petri 
%    Net system.
%    This function returns: 
%     - L = 0 : if the net has at least a dead transition;
%     - L = 1 : if the net is quasi-live (all transition quasi-live);
%     - L = 2 : if the net may be live (all transitions live or maybe live);
%     - L = 3 : if the net is live (all transitions are live).
%
%  [L,tr] = LIVE(G)
%    tr is a row vector in which every value represents a 
%    transition liveness property. 
%    The values are:
%     - tr(i) = 0 : if transition i is dead;
%     - tr(i) = 1 : if transition i is quasi-live;
%     - tr(i) = 2 : if transition i may be live;
%     - tr(i) = 3 : if transition i is live.
%
%  LIVE(G):
%    A textual answer will be returned.
%
%      See also GRAPH

%by Filippo Ledda, 2005


cfc = scc(G);% vector with information on strongly connected components
[n,m] = size(G);
numPosti = G (n, 1);
numTrans = G (n,2);
col1 = numPosti + 3;

G1 = G(1:n-1,col1:m);
[n,m] = size(G1);


tr = zeros(1, numTrans);
trComp = zeros(max(abs(cfc)),numTrans);% =0 if the transition may firefrom a given component
for i = 1:n % for each node
    for j = 1:2:m  % for each transition 
        if (G1(i,j) ~= 0)  % if the transition fires
            
            tr(G1(i,j)) = 1;%tr(i) = 1 -> quasi live transition (liv.1)
                      
            % we constructure the structure to associate transitions to
            % connected components to analyze liveness
            if( cfc(i) > 0) % if the node belongs to an absorbing component
                if( sum( isinf(G(i,:)) ) == 0 ) % if it is not a w-marking
                    trComp( cfc(i), G1(i,j) ) = 1;%trComp(i,j) = 1 : in the absorbing component i transition j fires               
                else
                    trComp( cfc(i), G1(i,j) ) = 2;%trComp(i,j) = 2 : in the absorbing component i with at least one w -marking 
                    % transition j fires
                end
            
            else % node in absorbing component
                trComp( -cfc(i), G1(i,j)) = -1;  %trComp(i,j) = -1  
            end
        end
    end
end

[q,numComp] = size(cfc);

trComp1 = trComp;
for i = 1:n
    if(cfc(i) < 0)
        trComp1(-cfc(i),:) = -1;
    end
end


% analysis of liveness of transitions (level 4)
for i = 1:numTrans 
    if (any(trComp(:,i)) == 1 ) %
        if( all(trComp1(:,i)) == 1 )        
            if( max(trComp(:,i) ) == 2 )
                tr(i) = 2;% maybe the transition is live 
            else 
                if( max(trComp(:,i) ) == 1 )% the transition is surely live (level 4) 
                    tr(i) = 3;
                end
            end
        end

    end
end

% we study the liveness of the net
if( min(tr) == 3 )
    % if all transitions are live, the net is live
    if(nargout == 0)
        fprintf('\nAll transitions are live:\nThe net is live!\n')
    else
        L = 3;
    end
elseif( min(tr) == 2 )
    % if no transition is certainly non live, the net could be live 
    if(nargout == 0)
        fprintf('All transitions may be live, but we can''t know more by Covering Graph analysis:\n');
        fprintf('\nThe net may be live!\n');
    else
        L = 2;
    end
elseif( min(tr) == 1 )
    
    if(nargout == 0)
        a = find(tr == 1);
        fprintf('These transition aren''t or can''t be live, but are quasi live:\n');
        fprintf('t%d\n', a);
        fprintf('\nThe net is quasi-live!\n');
    else
        L = 1;
    end
else
    
    if(nargout == 0)
        dead = find(tr == 0);
        fprintf('\nThe net has this dead transitions:\n'); 
        fprintf('t%d\n', dead);
        fprintf('\nThe net is not quasi-live or dead!\n');
    else
        L = 0;
    end
end

   
        
