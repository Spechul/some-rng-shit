function [v,Mf] = firable(I1,I2)
% FIRABLE : This function determines if a sequence of transitions of a P/T System is enabled to fire
%          *****************************************************************************************      
%                              ## SYNTAX ##
%
%          v=FIRABLE(X,sigma): 
%            Let X be the Reachability/Coverability Tree or the 
%            Reachability/Coverability Graph of a Petri Net System
%            <N,M0> and let sigma be a sequence of transitions: 
%            sigma = [ a1 a2 .... ak ]. 
%            This function returns: 
%               v = 1 : if sigma is firable from the initial marking M0;
%               v = 0 : otherwise. 
%
%          [v,Mf]=FIRABLE(X,sigma):
%            This function returns also a Matrix Mf that contains:            
%            Mf = [ M0'; M1'; ... M(k-1)'; Mk'].
%
%          See also FIRABLE1,GRAPH,TREE

ni=nargin;
no=nargout;
error(nargchk(2,2,ni));
X=I1;
Sigma=I2;
[r,c]=size(X);
m=X(end,1);
n=X(end,2);
M0=X(1,1:m);
Mf=[M0];
[y,x]=size(Sigma);
if max(Sigma)>n
   error('One sequence''s transition isn''t in the net!\n')
   return
end 
k=1;
a=1;
while k <= x,
   v=0;
   for b=m+3:2:c
      if Sigma(k)==X(a,b)
         a=X(a,b+1);
         Mf=[Mf;X(a,1:m)];
         v=1;
         break
      end
   end
   if v==0, 
      k=x+1;
   else
      k=k+1;
   end
end
if v==0,
   fprintf('\n The sequence isn''t enabled to fire at the initial marking!\n')
else
   fprintf('\n The sequence is enabled to fire at the initial marking!\n')
end

