function Cuo=ckcobs(C, M0, w, k, u)
% CKOBS observability test for a Generalized Mutual Exclusion Constraint (w,k)
%
%   This function tests if the GMEC (w,k) is observable with respect to 
%   a marked Place/Transition Petri Net <N,M0>.
%
% Syntax:
% ckobs(C, M0, w, k, u)
% Cuo = ckobs(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the unobservable transitions' numbers (1 x h) 1<h<=n
%
% Output argument (optional):
%   Cuo: incidence matrix (1 x h) of the monitor corresponding to the GMEC
%        relative to the unobservable transitions
%
% See also CKCONT, CKCONT2, CKOBS2, CONTGMEC, GMECSBS, MONITOR, MONITOR2.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);

if (Cm~=M0m | Cm~=wm | M0n~=1 | wn~=1 | km~=1 | kn~=1 | um~=1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (min(u)<0 | max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% check if M0 is legal for (w,k)
else
   CC = C(:,u);
   if no ==1 
       Cuo = CC;
   end    
   if (isempty(find(w'*CC <> 0)))
      % GMEC is controllable
      fprintf('\n   (w,k) is observable.\n');
   else
      % la GMEC is not controllabile
      fprintf('\n   (w,k) is not observable.\n   See OBSGMEC.\n\n');
   end
end
% Alessandro Giua - 2002 Jan 11