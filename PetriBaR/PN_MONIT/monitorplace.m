function [Cs, Ms0] = monitorplace(C, M0, w, k, u)
% MONITORPLACE Incidence matrix and initial marking of the monitor place
%              for the controllable GMEC (w,k)
%
%   This function finds the row of the incidence matrix of the monitor place s 
%   used to satisfy the controllable GMEC (w,k) for the marked place/transition
%   Petri net <C,M0>.
%
% Syntax:
% [Cs, Ms0] = monitorplace(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   Cs: row of the incidence matrix of the monitor place
%   Ms0: initial marking of the monitor place
%
% See also MONITORPLACE2, CHECKGMEC, CHECKGMEC2, CONTROLLABLEGMEC, GMECSBS, MONITORDESIGN, CLNET.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);
                         
if (Cm~=M0m | Cm~=wm |wn~=1 | M0n~=1 | km~=1 | kn~=1 | um>1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% verifies if M0 belongs to the set of legal markings defined by (w,k)
elseif (k-w'*M0 < 0)
   fprintf('   Error: Initial marking not belonging to M(N,M0,w,k)\n');
   fprintf('   This method is not applicable\n');
else
   Cu = C(:,u);
   if (isempty(find(w'*Cu > 0)))
      % the GMEC is controllable
      fprintf('\n   (w,k) is controllable\n\n');
      Csf = -w'*C;
      Msf = k-w'*M0;
      if (no == 0)
   	   disp(Csf);
         disp(Msf);
      else
         Cs = Csf;
         Ms0 = Msf;
      end
   else
      % the GMEC is not controllable
      fprintf('\n   (w,k) is not controllable\n\n');
   end
end
% Marco Ambu - 2001 May 26