function [Precc,Postcc,Mcc] = closedloop(pre,post,M0,Ccc,M0cc);

%CLOSEDLOOP generates a closed loop net given a set of monitorplaces.
%
%Syntax:
%
%[Precc,Postcc,Mcc] = closedloop(pre,post,M0,Ccc,M0cc);
%
%Input arguments:
%   Pre:  Pre matrix (m x n)
%   Post: Post matrix (m x n)
%   M0:   initial marking column vector (m x 1)
%   Ccc:  monitor places incidence matrix (nc x n)
%   M0cc:  monitor places initial markings (nc x 1)
%
% Output arguments:
%   PREcc:   the pre closed loop matrix
%   POSTcc:  The post closed loop matrix
%   Mcc:     the marking column vector
%
%  MP: uguale alla clnet. Ho lasciato quella dato che � la data pi� recente
% See also MONITORDESIGN, CHECKGMEC, MONITORPLACE, MONITORDESIGN.

[r,c]=size(Ccc);
[pr,pc]=size(pre);
[pr1,pc1]=size(post);
[mr,mc]=size(M0);
[mr1,mc1]=size(M0cc);

if(pr~=pr1|pc~=pc1|mr~=pr|c~=pc|mr1~=1|mc1~=1) 
   fprintf('Error: check size of inputs');
end




C=(Ccc(1:c))';


while c>0
   
if C(c)>0   
   Precc(1:c)=0;
   Postcc(1:c)=Ccc(1:c);
end

if C(c)<0
   Precc(1:c)=abs(Ccc(1:c));
   Postcc(1:c)=0;
end

if C(c)==0
   Postcc(1:c)=0;
   Precc(1:c)=0;
end

c=c-1;

end

Postcc=[post;Postcc];
Precc=[pre;Precc];
Mcc=[M0;M0cc];
  

