function [b,Cu]=checkgmec(C, M0, w, k, u)
% CHECKGMEC controllability test for a Generalized Mutual Exclusion Constraint (w,k)
%
%   This function tests if the GMEC (w,k) is controllable for the marked Place/Transition
%   Petri Net <C,M0> and if M0 belongs to the set of legal markings of (w,w).
%
% Syntax:
% checkgmec(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% See also CHECKGMEC2, CONTROLLABLEGMEC, GMECSBS, MONITORPLACE, MONITORPLACE2, MONITORDESIGN, CLNET.
b=0;
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);

if (Cm~=M0m | Cm~=wm | M0n~=1 | wn~=1 | km~=1 | kn~=1 | um>1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% check if M0 belongs to the set of legal markings defined by (w,k)
elseif (k-w'*M0 < 0)
   if (no ==1)
       Cu = C(:,u);    
   end
   fprintf('   Initial marking not belonging to M(N,M0,w,k)\n');
else
   CC = C(:,u);
   if no ==1 
       Cu = CC;
   end    
   if (isempty(find(w'*CC > 0)))
      % the GMEC is controllable
      b=1;
      fprintf('\n   (w,k) is controllable.\n');
   else
      % the GMEC is not controllable
      b=0;
      fprintf('\n   (w,k) is not controllable.\n   See CONTROLLABLEGMEC.\n\n');
   end
end
% Marco Ambu - 2001 May 26
% Modifyed by Massimo Gessa - 2003 Apr 3