function [Wc, Kc, Wu, Ku] = checkgmecs(C,M0,W,K,u)
% CHECKGMEC controllability test for a set of Generalized Mutual Exclusion Constraints (W,K)
%
%   This function tests if each GMEC (wi,ki) of the set (W,K) is controllable for the 
%   marked Place/Transition Petri Net <C,M0>. It returns the set of controllable GMECs
%   (Wc,Kc) and the set of uncontrollable GMECs (Wu,Ku).
%
%   The columns of W are the 'wi vectors'.
%   The columns and K are the 'ki values'.
%
% Syntax:
% [Wc, Kc, Wu, Ku] = checkgmec2(C, M0, W, K, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   W:   GMEC matrix (m x l)
%   K:   GMEC number column vector (l x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   Wc: the rows of this matrix are w vectors of controllable GMEC
%   Kc: the elements of this vector are k values of controllable GMEC
%   Wu: the rows of this matrix are w vectors of uncontrollable GMEC
%   Ku: the elements of this vector are k values of uncontrollable GMEC
%
% See also CHECKGMEC, CONTROLLABLEGMEC, GMECSBS, MONITORPLACE, MONITORPLACE2.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
%C=Post-Pre;
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(W);
[km,kn] = size(K);
[um,un] = size(u);

if (Cm~=M0m | wn~= km | Cm~=wm | M0n~=1 | um~=1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% check if M0 belongs to the set of legal markings defined by (w,k)
elseif (~isempty(find(K-W'*M0 < 0)))
   fprintf('\n   Initial marking not belonging to M(N,M0,wi,ki) for \n\n');
   h = find(K-W'*M0 < 0);
   for i=1:size(h)
      fprintf(' GMEC %d\n',h(i));
      disp(W(:,h(i))')
      disp(K(h(i),:))
   end
else
   Cu = C(:,u);
   [row,col] = find(W'*Cu > 0);
   if (isempty(row))
      % the GMEC is controllable
      if (no == 0)
         fprintf('\n   (W,K) is controllable.\n');
      else
         Wc = W;
         Kc = K;
         Wu = [];
         Ku = [];
	   end
   else
      % the GMEC is not controllable
      if (no == 0)
         fprintf('\n   Some (wi,ki) are not controllable:\n\n');
         for i=1:wn
            unc = find(row==i);
            if (~isempty(unc))
               fprintf(' GMEC %d\n',i);
               fprintf('\n w = ');
               disp(W(:,i)')
          	   fprintf(' k = ');
               disp(K(i))
             end
         end
         fprintf('\n   See CONTROLLABLEGMEC.\n');
      else
         Wc = [];
         Kc = [];
         Wu = [];
         Ku = []; 
         for i=1:wn
            unc = find(row==i);
            if (~isempty(unc))
               Wu = [Wu W(:,i)];
               Ku = [Ku; K(i)];   
            else
               Wc = [Wc W(:,i)];
               Kc = [Kc; K(i)];  
            end
         end
      end
   end
end
% Marco Ambu - 2001 May 26