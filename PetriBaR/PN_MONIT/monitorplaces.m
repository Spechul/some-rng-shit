function [CS, MS0] = monitorplaces(C, M0, W, K, u)
% MONITORPLACE Incidence matrix and initial marking of the monitor places
%              for the set of controllable GMEC (W,K)
%
%   This function finds the rows of the incidence matrix of each monitor place used
%   to satisfy the set of controllable GMEC (W,K) for the marked place/transition
%   Petri net <C,M0>.
%
%   The columns of W are the 'wi vectors'.
%   The columns and K are the 'ki values'.
%
% Syntax:
% [CS, MS0] = monitorplace2(C, M0, W, K, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   W:   GMEC matrix (m x l)
%   K:   GMEC number column vector (l x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   CS: matrix (l x n): rows of the incidence matrix of the monitor places
%   Ms0: column vector (l x 1): initial marking of the monitor places
%
% See also MONITORPLACE, CHECKGMEC, CHECKGMEC2, CONTROLLABLEGMEC, GMECSBS.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(W);
[km,kn] = size(K);
[um,un] = size(u);
%                   wn~=1    km~=1 
if (Cm~=M0m | wn~= km | Cm~=wm | M0n~=1 | um~=1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% verifies if M0 belongs to the set of legal markings defined by (W,K)
elseif (~isempty(find(K-W'*M0 < 0)))
   fprintf('   Initial marking not belonging to M(N,M0,W,K)\n');
   fprintf('   This method can not be used\n');
else
   Cu = C(:,u);
   [row,col] = find(W'*Cu > 0);
   if (isempty(row))
      % the GMEC is controllable
      Csf = -W'*C;
      Msf = K-W'*M0;
      if (no == 0)
         fprintf('\n Incidence matrix rows of the monitor places\n');
         disp(Csf);
         fprintf('\n Initial marking of the monitor places\n');
         disp(Msf);
      else
         CS = Csf;
         MS0 = Msf;
      end
   else
      % the GMEC is not controllable
      fprintf('\n   (W,K) is not controllable\n\n');
   end
end
% Marco Ambu - 2001 May 26 
