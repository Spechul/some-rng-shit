function [Prec,Postc,Mc] = clnet(pre,post,M0,Cc,M0c);

%clnet generates a closed loop net
%
%This function add a monitor row to pre and post matrix of a Petri net.
%
%Syntax:
%
%[PREc,POSTc,Mc]=clnet(Pre, Post, M0, Cc, M0c);
%
%Input arguments:
%   Pre:  Pre matrix (m x n)
%   Post: Post matrix (m x n)
%   M0:   initial marking column vector (m x 1)
%   Cc:   monitor places incidence matrix (nc x n)
%   M0c:  monitor places initial markings (nc x 1)
%
% Output arguments:
%   PREc:   the pre closed loop matrix
%   POSTc:  The post closed loop matrix
%   Mc:     the marking column vector
%
% See also MONITORDESIGN, CHECKGMEC, MONITORPLACE, MONITORDESIGN.

Postc=Cc;
Prec=Cc;
Mc=M0c;
[r,c]=size(Cc);
[r,c]=size(Cc);
[pr,pc]=size(pre);
[pr1,pc1]=size(post);
[mr,mc]=size(M0);
[mr1,mc1]=size(M0c);

if(pr~=pr1|pc~=pc1|mr~=pr|c~=pc|mc1~=1) 
   fprintf('Error: check size of inputs');
end

i=r;
while i>0
   n=c;
   
   while n>0
      
      if Cc(i:i,n:n)>0
         Postc(i:i,n:n)=Cc(i:i,n:n);
         Prec(i:i,n:n)=0;
      end
      
      if Cc(i:i,n:n)<0
         Prec(i:i,n:n)=abs(Cc(i:i,n:n));
         Postc(i:i,n:n)=0;
      end
      
      if Cc(i:i,n:n)==0
         Postc(i:i,n:n)=0;
         Prec(i:i,n:n)=0;
      end
      n=n-1;
   end
   i=i-1;
end

   
Postc=[post;Postc];
Prec=[pre;Prec];
Mc=[M0;M0c];

      
      
   