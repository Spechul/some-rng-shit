function [W, K] = controllablegmec(C, M0, w, k, u)
% CONTROLLABLEGMEC Finds all controllable Generalized Mutual Exclusion Constraints (wi,ki)
%                  minimally restrictive that satisfy the uncontrollable GMEC (w,k)
%
%   This function computes all the controllable GMEC (wi,ki) that can be used instead of
%   the given uncontrollable GMEC (w,k) for the marked Place/Transition Petri Net <C,M0>.
%   If (w,k) is controllable it returns the given GMEC (w,k)
%   If (w,k) is not controllable it returns a matrix W containing the w row vectors and
%     a vector containing the k values
%
%   This algorithm can be used only if the initial marking of the net M0 belongs to the
%   set of markings allowed by the constraint (w,k).
%
% Syntax:
% [W, K] = controllablegmec(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   W:   the rows of this matrix are w vectors of controllable GMEC
%   K:   the elements of this vector are k values of controllable GMEC
%
% See also GMECSBS, CHECKGMEC, CHECKGMEC2, MONITORPLACE, MONITORPLACE2, MONITORDESIGN, CLNET.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);
%                                                                
if (Cm~=M0m | Cm~=wm | M0n~=1 | wn~=1 | km~=1 | kn~=1 | um~=1 | un>Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
elseif (k-w'*M0 < 0)
   fprintf('   Initial marking not belonging to M(N,M0,w,k)\n');
   fprintf('   This method can not be used\n');
else
   if (un == Cn) 
      fprintf('\n   All the transitions are uncontrollable!\n');
   end
   Cu = C(:,u);
   if (isempty(find(w'*Cu > 0)))
      % the GMEC is controllable
      fprintf('\n   (w,k) is controllable\n');
      if (no ~= 0)
         W = w;
         K = k;
	   end
   else
      % the GMEC is not controllable
      %fprintf('\n   (w,k) is not controllable\n\n');
      GMEC = [];
      trchecked = zeros(1,un);
      % algorithm to compute controllable GMEC 
      nw = 1;
      wCu = [w'*Cu zeros(nw,Cm) ones(nw,1)];
      A = [Cu eye(Cm) zeros(Cm,1); wCu];
      nw = size(A,1) - Cm;
      while (nw~=0)
         j = find(A(Cm+1,1:un)>0);
         if (~(~isempty(j) & trchecked(j(1)))) 
   	      r = find(Cu(:,j(1))<0); 
            rowr = size(r,1);
	         for h=1:rowr
               wCu = A(r(h),:).*A(Cm+1,j(1))-A(Cm+1,:).*A(r(h),j(1));
               if (isempty(find(wCu(1:un)>0)))
                  w1 = wCu(un+1:un+Cm)+wCu(un+Cm+1)*w';
                  k1 = wCu(un+Cm+1)*(k+1)-1;
                 GMEC = [GMEC; w1 k1];
              else
                 A = [A; wCu];
              end
            end
         end
         trchecked(j(1)) = 1;
	      rowA = size(A,1);
         A = [A(1:Cm,:); A(Cm+2:rowA,:)];
         nw = size(A,1) - Cm;
      end
      [nGMEC,h] = size(GMEC);
      %fprintf('   %d controllable GMEC found\n',nGMEC);
      if (no == 0)
         for i=1:nGMEC
            fprintf('\n w%d = ',i);
         	disp(GMEC(i,1:h-1));
         	fprintf(' k%d = ',i);
            disp(GMEC(i,h));
         end
      else
         W = GMEC(:,1:h-1);
         K = GMEC(:,h);
	   end
   end
end
% Marco Ambu - 2001 May 15