function example_monitor

%Pre=[0 1 0 0;0 0 1 0;0 0 0 1;1 0 0 0];
%Post=[1 0 0 0;0 1 0 0;0 0 1 0;0 0 0 1];
%C=Post-Pre;
C=[1 -1 0 0; 0 1 -1 0; 0 0 1 -1; -1 0 0 1];
M0=[2 0 0 0]';
w1=[1 0 0 2]';
w2=[0 0 2 1]';
k1=2;
k2=2;
u=[3];
W=[w1,w2];
K=[k1;k2];
[W2c, K2c] = controllablegmec(C, M0, w2, k2, u);
W2=[w1,W2c'];K2=[k1;K2c];
[CS, MS0] = monitorplaces(C, M0, W2, K2, u)
%W2c=[0 2 2 1]';K2c=2;
%[W2, K2] = controllablegmec(C, M0, w2, k2, u)
%[Wc,Kc,Wu,Ku]=checkgmecs(C,M0,W,K,u)
%[Pre1,Post1,M]=monitordesign(Pre,Post,M0,W,K,u);
%gmecsbs(C,M0,W,K,u)
end