function [W, K] = gmecsbs(C, M0, w, k, u)
% GMECSBS Generalized Mutual Exclusion Constraints Step By Step Calculation
%
%   This function computes, step by step, the controllable GMEC derived by
%   the uncontrollable GMEC given in (w,k) for the marked Place/Transition
%   Petri Net <C,M0>.
%   If the GMEC (w,k) is controllable W = w and K = k
%
%   This function is only to explain the algorithm used in CONTROLLABLEGMEC
%   function. For calculation porpouses use CONTROLLABLEGMEC function.
%
% Syntax:
% [W, K] = gmecsbs(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   W matrix: the row i contains the vector w of the GMEC i
%   K column vector: the value in position i contains the value k of the GMEC i
%
% See also CONTROLLABLEGMEC, CHECKGMEC, CHECKGMEC2, MONITORPLACE, MONITORPLACE2.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);
%
if (Cm~=M0m | Cm~=wm | M0n~=1 | wn~=1 | km~=1 | kn~=1 | um~=1 | un>Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% check if M0 belongs to the legal markings imposed by (w,k)
elseif (k-w'*M0 < 0)
   fprintf('   Initial marking not belonging to M(N,M0,w,k)\n');
   fprintf('   This method can not be used\n');
else
   if (un == Cn) 
      fprintf('\n   All the transitions are uncontrollable!\n');
   end
   Cu = C(:,u);
   if (isempty(find(w'*Cu > 0)))
      % the GMEC is controllable
      fprintf('\n   (w,k) is controllable\n');
      pause;
      fprintf('\n The new incidence matrix with the monitor place is\n');
      disp([C; -w'*C]);
      pause;
      fprintf('The new initial marking is\n');
      disp(k-w'*M0);
      pause;
      if (no ~= 0)
         W = w';
         K = k;
	   end
   else
      % the GMEC is not controllable
      fprintf('\n   (w,k) is not controllable\n\n');
      GMEC = [];
      trchecked = zeros(1,un);
      % algorithm to find controllable GMECs
      nw = 1;
      fprintf('Make the matrix A = [Cu(mxnu) I(m) 0(mx1)]\n');
      fprintf('-  Cu is the matrix C without the columns of controllable transitions\n');
      fprintf('-  I(m) is the identity matrix;\n-  0(mx1) is a column vector of zeros\n');
      pause;
      disp([Cu eye(Cm) zeros(Cm,1)]);
      wCu = [w'*Cu zeros(nw,Cm) ones(nw,1)];
      fprintf('Add to A the row [wT*Cu 0(1xm) 1] after the last row of A -> A = [A; wT*Cu 0(1xm) 1]\n-  wT is the transposed vector of w\n');
      disp(wCu);
      pause;
      A = [Cu eye(Cm) zeros(Cm,1); wCu];
      fprintf('The initial matrix A is\n');
      disp(A);
      pause;
      nw = size(A,1) - Cm;
      while (nw~=0)
         j = find(A(Cm+1,1:un)>0);
         fprintf('Find the first column of A for which the first of the row added has an\n');
         fprintf('element >0 in the column 1...nu, where nu is the number of columns of Cu\n');
         fprintf('  Founded column: %d (considering transition %d)\n',j(1),j(1));
         pause;
         if (~isempty(j) & trchecked(j(1))) 
            fprintf('\nTransition %d has just been analized\n',j(1));
         else
          r = find(Cu(:,j(1))<0); % vettore colonna
            rowr = size(r,1);
            fprintf('\nFind the rows of A for which in the founded column of A there are negative elements\n');
            fprintf('  Founded %d rows:\n', rowr);
            disp(r');
            pause;
            fprintf('\nFor each negative element founded, find the linear combination between the row vector\n');
            fprintf('and the first vector added that gives 0 in the element of the column founded\n');
            fprintf('The linear combination of this two rows gives a new row\n');
            for h=1:rowr
   	           wCu = A(r(h),:).*A(Cm+1,j(1))-A(Cm+1,:).*A(r(h),j(1));
               fprintf('\n   Column %d\n\nThe linear combination between row %d and row %d, that gives 0 in the column %d, is\n',j(1),r(h),Cm+1,j(1));
               disp(wCu);
               pause;
               if (isempty(find(wCu(1:un)>0)))
                  w1 = wCu(un+1:un+Cm)+wCu(un+Cm+1)*w';
                  k1 = wCu(un+Cm+1)*(k+1)-1;
                  GMEC = [GMEC; w1 k1];
                  fprintf('\nThe row calculated has every element in the columns 1...nu <=0\n');
                  fprintf('It must not be added to A and it will be used to find w_new and k_new\n');
                  fprintf('   newrow = [h(1xnu) r(1xm) r2]\n\n');
                  fprintf('-  w_new = r + r2*w     =');
                  disp(w1);
                  fprintf('-  k_new = r2*(k + 1)-1 =');
                  disp(k1);
                  pause;
               else
                  fprintf('\nThe new calculated row has elements >0 in one (or more) of the columns 1...nu\n');
                  A = [A; wCu];
                  pause
                  disp(A);
                  pause;
               end
            end
         end
         trchecked(j(1)) = 1;
         rowA = size(A,1);
         A = [A(1:Cm,:); A(Cm+2:rowA,:)];
         nw = size(A,1) - Cm;
         fprintf('\nDelete the row analyzed\nThe new matrix A is\n');
         disp(A)
         pause
      end
      [nGMEC,h] = size(GMEC);
      fprintf('One of the GMEC founded can be used instead of the uncontrollable GMEC\n');
      fprintf('\n   %d controllable GMEC founded:\n',nGMEC);
      if (no == 0)
         for i=1:nGMEC
            fprintf('\nw%d = ',i);
         	disp(GMEC(i,1:h-1));
         	fprintf('k%d = ',i);
            disp(GMEC(i,h));
         end
      else
         W = GMEC(:,1:h-1);
         K = GMEC(:,h);
	   end
   end
end
% Marco Ambu - 2001 May 15