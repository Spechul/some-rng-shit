function [pre1,post1,M] = monitordesign(pre,post,m,w,k,u);
% monitordesign: designs a closed loop net system given a GMEC (w,k)
%
%   This function first tests if the GMEC (w,k) is controllable for the given
%   Place/Transition Net.
%
%   If the GMEC is controllable, is computes the corresponding monitor and 
%   returns the stucture of the closed loop system.
%
%   If the GMEC (w,k) is not controllable, it determines a new GMEC (wi,ki) 
%   that is controllable using Moody's procedure, computes the monitor
%   place that correponds to (wi,ki) and returns the stucture of the closed 
%   loop system.
%   
% Syntax:
% [PREc,POSTc,Mc]=monitordesign(Pre, Post, M0, w, k, u);
%    
% Input arguments:
%   Pre:  Pre matrix (m x n)
%   Post: Post matrix (m x n)
%   M0:   initial marking column vector (m x 1)
%   w:    GMEC matrix (m x j)
%   k:    GMEC column vector (j x 1)
%   u:    row vector of the uncontrollable transitions' numbers (1 x h) h<=n
%
% Output arguments:
%   PREc:   the pre closed loop matrix
%   POSTc:  The post closed loop matrix
%   Mc:     the marking column vector
%
% See also CHECKGMEC, MONITORPLACE, CONTROLLABLEGMEC, CLNET.

c=post-pre;
b=0;
er=0;
ni=nargin;
error(nargchk(5,6,ni));
if ni<6
  u=[]; 

end 
pre1=[];post1=[];Mcc=[];W1=[];K1=[];W=[];K=[];C=[];M=[];cs=[];ms=[];cs1=[];ms1=[];csa=[];msa=[];csb=[];msb=[];kc=[];kn=[];wn=[];wc=[];

[prer,prec]=size(pre);


[a1,b1]=size(pre);
[a2,b2]=size(post);
[a3,b3]=size(m);
[a4,b4]=size(w);
[a5,b5]=size(k);
[a6,b6]=size(u);

%check input size
if(a1~=a2|b1~=b2|a3~=a1|b3~=1|a4~=a3|a5~=b4|b5~=1|a6>1|b6>b1) 
   fprintf('Error: check size of inputs');
end


%check controllable and uncontrollable GMECs
n=size(w',1);
while n>0
   % verifies controllability
   b=checkgmec(c,m,w(:,n),k(n),u);
   
   if b==1
      % if it is controllable
      wc=[wc,w(:,n)];
      kc=[kc;k(n)];

   end
   
 	if b==0 
      % if it is not controllable
   	wn=[wn,w(:,n)];
      kn=[kn;k(n)];
   if  (k(n)-(w(:,n))'*m < 0)
      er=1; 
    end   
       
   end
   
   	n=n-1;
      
   end


% verifies dimentions
[rp,cp]=size(w);
[rc,cc]=size(wc);
[rn,cn]=size(wn);
   
% computes monitor place(s) in the case of controllable GMECs
n=size(wc',1);
while n>0
   if  (kc(n)-(wc(:,n))'*m < 0)
      er=1; 
    end   

   [csa,msa]=monitorplace(c,m,wc(:,n),kc(n),u);
   cs=[cs;csa];
   ms=[ms;msa];
   n=n-1;
end

% computation of sub-optimal monitors and print of results
n=size(wn',1);
while n>0
  [W1, K1]=controllablegmec(c,m,wn(:,n),kn(n),u);
   W1=W1';
   W1=W1(:,1);
   K1=K1(1);
   W=[W,W1];
   K=[K;K1];
   n=n-1;
end




[nr1,nc1]=size(W);
[nr2,nc2]=size(wn);


% computation of monitor places of sub-optimal GMECs

n=size(W',1);
while n>0
   if  (K(n)-(W(:,n))'*m < 0)
      er=1; 
    end   

   [csb,msb]=monitorplace(c,m,W(:,n),K(n),u);
   cs1=[cs1;csb];
   ms1=[ms1;msb];
   n=n-1;
end

% generation of output
ch=[cs;cs1];
mh=[ms;ms1];
[rch,cch]=size(ch);
pre1=pre;
post1=post;
M=m;
   
   while rch>0
      [pre2,post2,Mcc1]=clnet(pre,post,m,ch(rch,:),mh(rch));
      pre1=[pre1;pre2(prer+1,:)];
      post1=[post1;post2(prer+1,:)];
      M=[M;Mcc1(prer+1)];
            
      rch=rch-1;
end




if er==1
   fprintf('\n\nInitial marking not belonging to M(N,M0,w,k)\n');
   fprintf('This method can not be used\n');
end  
   

if nr1~=nr2 
   fprintf('\n-> Error: computing minimally restrictive GMEC <- \n\n');
   er=1;
end



if er==0
   
   n=100;
while n>0
   fprintf('\n');
   n=n-1;
end

   
  if size(wn',1)==0
      fprintf('\nAll GMEC are controllable\n\n');
   else   
    i=size(wn',1);
    fprintf('Uncontrollable GMEC:\n')
while i>0
   fprintf('w%i=',i)
   disp((wn(:,i))')
   fprintf('k%i=',i)
   disp(kn(i))
   i=i-1;
end
    end
   
  
 if size(wn',1)~=0

   i=size(wn',1);
    fprintf('the minimally restrictive is\n\n')

   while i>0
   fprintf('w%i=',i)
   disp((W(:,i))')
   fprintf('k%i=',i)
   disp(K(i))
   i=i-1;
end
end

fprintf('the monitor\n\n')

  i=size(cs,1);
 
    while i>0
   fprintf(' Cc%i=',i+size(cs1,1))
   disp((cs(i,:)))
   fprintf('M0c%i=',i+size(cs1,1))
   disp(ms(i))
   i=i-1;
    end

  i=size(cs1,1);

   while i>0
   fprintf(' Cc%i=',i)
   disp((cs1(i,:)))
   fprintf('M0c%i=',i)
   disp(ms1(i))
   i=i-1;
   end

fprintf('Pre, Post and marking matrix\n');

   fprintf('pre=\n');
   disp(pre1);
   fprintf('post=\n');
   disp(post1);
   fprintf('M0=\n');
   disp(M');
   
end
%by Massimo Gessa 27/03/03


