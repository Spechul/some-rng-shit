function Cuc=ckcont(C, M0, w, k, u)
% CKCONT controllability test for a Generalized Mutual Exclusion Constraint (w,k)
%
%   This function tests if the GMEC (w,k) is controllable with respect to 
%   a marked Place/Transition Petri Net <N,M0>.
%
% Syntax:
% ckcont(C, M0, w, k, u)
% C_uc = ckcont(C, M0, w, k, u)
%
% Input arguments:
%   C:   incidence matrix (m x n)
%   M0:  initial marking column vector (m x 1)
%   w:   GMEC column vector (m x 1)
%   k:   GMEC number (1 x 1)
%   u:   row vector of the uncontrollable transitions' numbers (1 x h) 1<h<=n
%
% Output argument (optional):
%   Cuc: incidence matrix (1 x h) of the monitor corresponding to the GMEC
%        relative to the uncontrollable transitions
%
% See also CKOBS, CKCONTOBS, CONTGMEC, GMECSBS, MONITOR, MONITOR2.
ni = nargin;
no = nargout;
error(nargchk(5,5,ni));
% check of input size
[Cm,Cn] = size(C);
[M0m,M0n] = size(M0);
[wm,wn] = size(w);
[km,kn] = size(k);
[um,un] = size(u);

if (Cm~=M0m | Cm~=wm | M0n~=1 | wn~=1 | km~=1 | kn~=1 | um~=1 | un>=Cn)
   fprintf('   Error: check the size of inputs\n');
elseif (min(u)<0 | max(u)>Cn)
   fprintf('   Error: check values of u vector\n');
% check if M0 is legal for (w,k)
else
   CC = C(:,u);
   if no ==1 
       Cuc = CC;
   end    
   if (isempty(find(w'*CC > 0)))
      % the GMEC is controllable
      fprintf('\n   (w,k) is controllable.\n');
   else
      % the GMEC is not controllabile
      fprintf('\n   (w,k) is not controllable.\n   See CONTGMEC.\n\n');
   end
end
% Marco Ambu - 2001 May 26
% Updated by: Alessandro Giua - 2002 Jan 11