function [r,Te]=IfReach(M,Pre,Post,M0)
addpath(genpath(pwd)); % to add the glpk directory
% This function is to check if marking is reachable

%% compute the set of explicit transitions

Te=CompTe(Pre,Post);

%% check if M is reachable with E being the set of explicit transitions
% Note: the reachability of M does not depend on the set of explicit
% transitions. Namely if and only if under E it is reachable, then M is reachable

r=IfReachTe(M,Pre,Post,M0,Te);

end