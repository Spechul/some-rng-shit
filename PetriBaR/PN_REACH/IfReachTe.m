function r=IfReachTe(M,Pre,Post,M0,Te)
addpath(genpath(pwd));
% This function is to check if a given marking is rechable of a net with
% the given set of explicit transitions
% RE=1, the marking M is reachable; otherwise, RE=0

% M is the given marking
% (Pre, Post, M0) is the net system
% E is the set of explicit transitions

% initialize
% r=0;

[np,nt]=size(Pre); % np is the number of places, and nt is the no. of transitions
Trans=1:nt; % the set of transitions
I=setdiff(Trans,Te); % the set of implicit transitions 
nu=size(I,2); % no. of implicit transitions

% set the parameters of the ILP 
SENSE=1;
CTYPE='S';
CTYPE(1:np)='S';
CTYPE=CTYPE';
LB=zeros(nu,1);
UB=Inf(nu,1);
VARTYPE='I';
VARTYPE(1:nu)='I';
VARTYPE=VARTYPE';
PARAM=0;
LPSOLVER=1;
SAVE=0;

% compute the incidence matrix of the induced net
C=Post-Pre;
Cu=[];
for i=1:nu
    t=I(i);
    Cu=[Cu,C(:,t)];
end

T=BRG(Pre,Post,M0,Te); % build the BRG

% check if there exists a basis marking Mb s.t. M=Mb+Cx has a integer
% solution

nb=size(T,1); % number of basis markings
c=ones(nu,1); %the objective function coefficients.

for i=1:nb
    Mb=T{i,2};
    b=M-Mb;
    [XMIN,FMIN,STATUS]=glpkmex(SENSE,c,Cu,b,CTYPE,LB,UB,VARTYPE,PARAM,LPSOLVER,SAVE);
    if (STATUS==5) % there is a integer solution
        r=1;
        fprintf('\n The marking M=\n')
        disp(M);
        fprintf('\n is reachable.\n')
        break;
    else  % there is no integer solution
        r=0;
        fprintf('\n The marking M=\n')
        disp(M);
        fprintf('\n is NOT reachable.\n')
    end
end

end