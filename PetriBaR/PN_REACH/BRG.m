function [B] =BRG(Pre,Post,M0,Te)

%BRG:  This function builds the basis reachability graph of a PN net for given the set of explicit transitions Te.
%      
%      ********************************************************************
%                                 ## SYNTAX ##
%
%      [B] = BRG(Pre,Post,M0,E)
%
%      Given a Petri net with its matrices Pre and Post,the initial marking
%      M0, and the row vector E,that indicates the set of explicit transitions,
%      this function returns a cell B that contains for each row:            
%      1) the node's number;
%      2) the basis markings that belong to the node;
%      3) the transitions enabled by the node;
%      4) the nodes that are reachable with the firing of any transition
%         in association with the j-vector correspondent.
%     
%         ______________________________________________________________
%        |   1    |      2      |        3          |     4      |  5  |
%      T=|________|_____________|___________________|____________|_____|
%        |# Number|Basis Marking|Enabled Transitions|Arc-t Matrix| Tag |
%        |________|_____________|___________________|____________|_____|
%        |   6    | 
%        |________|
%        |  Arc_l |
%        | Matrix |
%        |________|
%
%      L must be a cell array that has as many rows as the cardinality of
%      the considered alphabet, that contains in each row the observable
%      transitions having the same label.
%      E=[2 3 6] means t2,t3,t6 are explicit transitions.
%
%      See also: showBRG

% Verfy the input data

[m,n]=size(Post); % m is the number of places and n is the number of transitions
[c,p]=size(M0');
% [sizeL,lenghtL]=size(L);

if (size(Post)==size(Pre))&(c==1)&(p==m),
elseif size(Post)~=size(Pre)
   fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
   fprintf('\n Put again the data\n')
else
   fprintf('\n  ERROR! dimensions of the  marking are wrong!\n')
end

% if (lenghtL==1),
% else
%    fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Labeling Function are wrong!\n')
% end

% if (b==1),
% else
%    fprintf('\n  ERROR! dimensions of the Matrix representing\n the Fault Class are wrong!\n')
% end

% if (size(L)==size(E)),
% else
%    fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Symbol set is not consistent wrt\n  the Labeling function!\n')
% end

% Determine respectively the number of explicit transitions (no),
% and implicit transtions (nu)

no=size(Te,2);
% for i=1:sizeL
%     [sizeLi,lenghtLi]=size(L{i,1});
%     no=no+lenghtLi;
% end

nu=n-no;

% E' strettamente necessario ordinare le transizioni in maniera da avere
% rispettivamente prima quelle osservabili e successivamente le restanti
% non osservabili. Per congruenza con il MBRG faremo in maniera da avere
% le transizioni osservabili, quelle di guasto e per ultime le regolari.

MPre=[];
MPost=[];
% MF=[];
% ML=[];

% Vettore utilizzato in seguito per tenere traccia dell'originaria
% enumerazione delle transizioni.
% Es: if numbers(i)=j then la transizione tj ?stata numerata ti

numbers=zeros(1,n);

% Definizione di un vettore binario tale che
% if(reg(i)==1)
% then la transizione i-esima ?una transizione regolare.
% Infine un contatore utile per risettare le matrici definenti funzione di
% etichettatura e classi di guasto

rego=ones(1,n);
count=0;
for i=1:no
    count=count+1;
    t=Te(i);
    rego(t)=0;
    numbers(count)=t;
    MPre=[MPre,Pre(:,t)];
    MPost=[MPost,Post(:,t)];
    ME=1:count;
end

for i=1:n
    if (rego(i)==1)
        count=count+1;
        numbers(count)=i;
        MPre=[MPre,Pre(:,i)];
        MPost=[MPost,Post(:,i)];
    end
end


Pre=MPre;
Post=MPost;

% Start the timer
%tic

% Initialize the BRG matrix T

B=[{1} {[M0]} {[zeros(1,no)]} {[(empty_vector(no))]} {[0]}];

c=cell2mat(B(:,5));
d=find(c==0);


% Sinch?esiste almeno un nodo senza TAG, trova le coordinate di tali nodi
% da esplorare e salvale nel vettore d

while ~isempty(d)
	d=find(c==0);
	%Per tutte le transizioni osservabili:
    for i=1:no
    
    % Per ciascuna delle marcature di base raggiungibili da Mcurrent con la
    % i-esima transizione Osservabile, a partire dalla prima marcatura non
    % esplorata MM e per ciascuno dei j-vector associati, definisco gli
    % archi del grafo.
    
        MM=B{d(1,1),2};
        %fprintf('Transizione ', i)
        Mb=Mbasis( Pre , Post ,MM , nu , i);
        %sizeMb21 is the number of basis markings reachable from MM
        [sizeMb21,lengthMb21]=size(Mb{2,1});
    
        for j=1:sizeMb21 %循环一次分析一个marking
            Nodeold=0;    
            Mcurrent=Mb{2,1}(j,:);
        
        % Verifichiamo  se le transizioni di guasto sono abilitate.
        % Questa funzione deve essere ricompilata per essere utilizzata su
        % MAC-OS
        
          %ftag=Cons(Pre , Post , Mcurrent' , nu , W , K);
        
        %Utilizzare questa nel caso in cui non si abbia la possibilit?di
        %farlo
        %Sol =faultclass(Pre , Post , Mcurrent' , nu , 0 ,F);
        
        % Verifichiamo il numero di J-Vector associati alla transizione
        % considerata
            [sizeJi,lenghtJi]=size(Mb{2,2}{j,1});
        
            for jj=1:sizeJi
                jcurrent=Mb{2,2}{j,1}(jj,:);
            % Ricordando che il riordinamento delle transizioni porta ad
            % avere un riordinamento anche del vettore di scatto della
            % giustificazione:
%             a=zeros(size(jcurrent));
%             for jc=1:nu
%                 a(numbers(no+jc)-no)=jcurrent(jc);
%             end
%             a

                a=[numbers(1,no+1:end);jcurrent];
                temp=sortrows(a')';
                jcurrent=temp(2,:);
            % Verifica del fatto che una transizione sia abilitata o meno
                if( ~isempty( find(Mcurrent<0) ) )
                    B{d(1,1),3}(1,i)=0;
                    continue
        
            % Verifica del fatto che una transizione "cappio" non scatti  a
            % meno che il posto corrispondente non sia marcato
                elseif(min(Mcurrent'- Post(:,i))<0)
                    B{d(1,1),3}(1,i)=0;
                    continue
        
                else %se la transizione ?abilitata e porta ad una Mcurrent
%             fprintf('\n\nDalla marcatura ')
%             fprintf('%d ',MM)
%             fprintf ('\n alla marcatura ')
%             fprintf('%d ',Mcurrent)
%             fprintf('\ncon t%d e giuistificazione ',i)
%             fprintf('%d ',jcurrent)

                    [numofnode, lengthT]=size(B);

                % Verifico che la marcatura non sia gi?presente nel grafo
                    for k=1:numofnode
                        if(Mcurrent'==B{k,2})
                            B{d(1,1),3}(1,i)=1;
                            [sizej,lengthj]=size(B{d(1,1),4}{1,i});
                            B{d(1,1),4}{1,i}{sizej+1,1}={[k] [jcurrent]};
                            Nodeold=1;
%                             fprintf('\n\narco %d dal nodo %d al nodo %d con giustificazione',i,d(1,1),k)
%                             fprintf(' %d',jcurrent)
                        end            
                    end
                
                % Se la Marcatura non ?gi?presente, Nodeold==0, aggiungo 
                % un nuovo nodo
                    if(Nodeold==0) % creo il nuovo nodo
                        
                        [numofnode, lengthT]=size(B);
                        B{numofnode+1,1}=[numofnode+1];
                        B{numofnode+1,2}=[Mcurrent'];
                        %T{numofnode+1,3}= ftag;
                        A =empty_vector(n-nu);
                        B{numofnode+1,3}= zeros(1,no);
                        B{numofnode+1,4}= A;
                        B{numofnode+1,5}= 0;
                        
                    % Aggiorno i valori del nodo considerato

                        B{d(1,1),3}(1,i)=1;
                    
                    % Questa funzione deve essere ricompilata per essere
                    % utilizzata su MAC-OS
                    
                       %ftag2=Cons(Pre , Post , MM , nu , W , K);

                    %Sol2=faultclass(Pre , Post , MM , nu , 0 ,F);
                        
                        %T{d(1,1),3}=ftag2;
                        [sizej,lengthj]=size(B{d(1,1),4}{1,i});
                        B{d(1,1),4}{1,i}{sizej+1,1}={[numofnode+1] [jcurrent]};
                        B{d(1,1),5}= 0;
%                         fprintf('\n\narco %d dal nodo %d al nodo %d con giustificazione',i,d(1,1),numofnode+1)
%                         fprintf(' %d',jcurrent)
                        [numofnode, lengthT]=size(B);
                    end        
                end
            end
        end
    end

    B{d(1,1),5}= 1;

% Riverifico i nodi non ancora esplorati

    c=cell2mat(B(:,5));
    d=find(c==0);
end

% Inseriamo la nuova matrice degli archi nella colonna numero 7 della
% matrice T del BRG

 [sizeT,lenghtT]=size(B);

% Genero l'array in cui inserire il vettore binario delle transizioni
% abilitate

% for n=1:sizeT
%     T{n,6}=zeros(1,sizeL);
% end
% 
% % Per tutti i nodi del BRG
for n=1:sizeT		
	B{n,6}=empty_vector(no);
    %per tutte le etichette
    for i=1:no
%		[sizeLi,lenghtLi]=size(L{i,1});
%		%per la j-esima transizione appartenente alla j-esima classe
        count=0;
%        for j=1:lenghtLi
            transition=ME(i);
			if((B{n,3}(1,transition))==1)
%				T{n,6}(1,i)=1;
                [sizeJi,lenghtJi]=size(B{n,4}{transition});
                for jj=1:sizeJi
                    count=count+1;
                    % {[simbolo][transizione] {[nodo di arrivo][j-vector]}}
                    B{n,6}{count,i}={[Te(i)] [numbers(transition)] B{n,4}{1,transition}{jj,1}};
                end
			end
%		end
	end
end


%toc
%t=toc;
end