function Te=CompTe(Pre,Post)
% this funciton is to compute the maximal set I (corresponds to the minimal Te) of implicit transitions of a give
% net (Algorithm 3 in the TAC_Ma paper),
% Note: it is assumed that there exists at least one transition that has no
% slefloop

%% initialize
[m,n]=size(Pre); % m is the no. of places and n is the no. of transitions
Tr=1:n; % the set of transitions
I=Tr;
%% compute the adjacent matrix A of the net graph
A11=zeros(m); % A11 describes the arc from places to places 
A22=zeros(n); % A22 describes the arc from transitions to transitions
A12=zeros(m,n); % A12 describes the arc from places to transitions
A21=zeros(n,m); % A21 describes the arc from transitions to places

for i=1:m
    for j=1:n
        if Pre(i,j)>0
            A12(i,j)=1;
        end
        if Post(i,j)>0
            A21(j,i)=1;
        end
    end
end

A=[A11,A12;A21,A22];

%% break all cycles (Lines 2-5 in Algorithm 3)
for i=1:m+n
    Atemp{i}=find(A(i,:)>0);
end

numI=n;
Tag=[1];
while numI>0 && any(Tag)% i.e., I is not empty
    [GSCC,v] = tarjan(Atemp);
    col=size(GSCC,2);
    for i=1:col
        if  size(GSCC{i},2)>1 % there is a cycle
            Tag(i)=1;
            Locate=find(GSCC{i}>m); % location of transitions in the cycle
            tc=GSCC{i}(Locate(1)); % tc is the first transion of a cycle
            % remove tc from I and compute the new adajcent
            % matrix
            I=setdiff(I,tc-m);
            numI=size(I,2);
            Atemp{tc}=[];
        else
            Tag(i)=0;
        end
    end
end

%% put back transitions in T_I (Lines 6-10 in Algorithm 3)
Etemp=setdiff(Tr,I);
for i=1:size(Etemp,2)
    Itemp=I;
    Itemp=[Itemp,Etemp(i)];
    Atemp{Etemp(i)+m}=find(A(Etemp(i)+m,:)>0);
    [GSCC,v] = tarjan(Atemp);
    col=size(GSCC,2);
    for j=1:col
        if  size(GSCC{j},2)>1
           tag(j)=1;
           break;
        else
            tag(j)=0;
        end
    end
    if ~any(tag) % there is no cycle
        I=[I,Etemp(i)];
    else
        Atemp{Etemp(i)+m}=[];
    end
end
Te=setdiff(Tr,I);
end