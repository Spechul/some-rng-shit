function [ Mw ] = Mbasis(Pre , Post , M0, nu , w )


%  Mbasis: This function computes for any observation w, a set, that we
%          denote M(w), whose elements are couple of basis marking and
%          relative j-vectors
%          ****************************************************************
%
%                                  ## SINTAX ##
%          [ Mw ] = Mbasis(Pre, Post, M0, nu, w)
%          Given a Petri net with its matrices Pre and Post, a initial
%          marking M0, a firing sequence of observable transition w and the
%          number of  unobservable transitions nu, this function returns a
%          cell-matrix Mw that contents many row as the length of the 
%          firing sequence +1, and for each row, three columns.
%          The first column is a matrix that represents the basis marking
%          consistent with the firing transition which is represented in 
%          the third column. The second column of each row is an other
%          cell-matrix of one column where, the number of row is equal
%          to the number of  basis marking for this transition. 
%          In each row of is sub cell-matrix there is a matrix that
%          represents the set of j-vectors corresponding to the basis
%          marking in the first column.
%          The firing sequence w must be a row array of integer where each
%          number represents the relative observable transition.
%          The matrices Pre and Post must contain in the first columns the
%          observable transitions and then that unobservable.


  
 [sizew,lenghtw]=size(w); 
 [pn,tn]=size(Pre);
 [x,c]=size(M0);

 % verification of the input data
if (size(Post)==size(Pre))&(c==1)&(pn==x),
elseif size(Post)~=size(Pre)
        fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
        fprintf('\n Insert again the data\n')
else
        fprintf('\n ERRORE! dimensions of the  marking are wrong!\n')
        return
end

if(sizew~=1)
    fprintf('\n ERROR! w must be a row array\n')
    return
end          
 
C=Post-Pre;
Cu=(C(:,(tn-nu+1):tn));

% Initialization of the data matrix. The first row corresponds to initial 
% marking, null justification corresponding to the empty word, and the empty
% word (denoted with null subscript)
% Let M(w)={M0 0}.
Mw = {M0' {zeros(1,nu)} [0]};

% For all the transitions belonging to the given word
for t=1:lenghtw
    % select the transition that fires
    transition=w(t);
    Mw{t+2,2}=[];
    
    % For any new transition, the set of markings reached with the word
    % w't starting from the set of markings considered at the previous step, 
    % is emptied
    M=[];
    JNEW=[];
    Jnew=[];
    % 'm' is the number of basis markings in Mw corresponding to w',
    % (where w=w't t=w(j) )
    Mold=Mw{t,1};
    [sizeMold,lenghtMold]= size(Mold);
    
    % For all the m basis markings reached with w'    
    for i=1:sizeMold
        % we verify the number r of justification vectors corresponing to
        % them
        Mold_i=Mold(i,:);
        Jold=Mw{t,2}{i,1};
        [sizeJold,lenghtJold]= size(Jold);
        

        
        % All the justifications to the j-th transition are computed. 
        % 'B' is a matrix of dimension [c x d] where: 'c' is the number of
        % justifications and 'd' the number of unobservable transitions.
        
        Jcurrent = miny(Pre,Post,Mold_i',transition,nu);
        [sizeJcurrent,lenghtJcurrent]= size(Jcurrent);
        % If B is an empty matrix, then the j-th transition is not
        % enabled at the marking at hand.
        if ( isempty(Jcurrent))
            % We impose that it is an empty vector, as if the transition is 
            % enabled without the requirement of firing any unobservable transition
            Jcurrent=zeros(1,nu); 
        end
        
        % For all the minimal justifications computed for the firing of
        % the j-th transition,
        for g=1:sizeJcurrent
            Jcurrent_g=Jcurrent(g,:);
            % we compute the markings reached with the firing of the 
            % justifications and the j-th transition at hand
            
            %***************************NOTE*******************************
            %
            % 1) Note that in this way all the self-loop transitions are 
            % enabled even if the corresponing place in the loop is unmarked. 
            % 2) Note that the requirement of firing transitions that are 
            %    not enabled, would lead to the addition of negative markings, 
            %    rathern that exiting the program in advance. 
            %
            %   All these errors are taken into account in functions BRG
            %   and MBRG.
            %
            %***************************Note*******************************
            
            HIT =0;
            Mcurrent = Mold_i + (Cu*Jcurrent_g')' + (C(:,transition))';
            Jnew=[];
            for k=1:sizeJold
                Jnew(k,:)=Jold(k,:)+Jcurrent_g;
            end
            
            % Verifies if the marking Mcurrent belongs to the set M
            % of markings consistent with w'
            [sizeM,lenghtM]= size(M);
            
            % If the set of markings consistent with w' is an empty set,
            % then we initialize it to Mcurrent
            if sizeM==0
                M=Mcurrent;
                JNEW{1,1}=Jnew;
                
            % if such is not the case, we verify that the marking has 
            % already been reached
            else
                for k=1:sizeM
                    if Mcurrent==M(k,:)
                        HIT=1;
                        JNEW{k,1}=[JNEW{k,1};Jnew];
                        break
                    end
                end
                
                if HIT ==0
                    M(end+1,:)=Mcurrent;
                    JNEW{end+1,1}=Jnew;
                end
            end
            [sizeM,lenghtM]= size(M);
        end
    end
    
    % For any marking consistent with the word w' it is necessary to
    % remove redundant justifications:
    [sizeM,lenghtM]= size(M);
    % For all the sets of justifications
    for i=1:sizeM
        % For all the justifications in the selected set
        [sizetemp,lenghttemp]= size(JNEW{i,1});
        for ii=1:sizetemp

            if ii> sizetemp
                break
            end
            
            temp=JNEW{i,1}(ii,:);
            
            for iii=sizetemp:-1:1
                if temp==JNEW{i,1}(iii,:) & ii~=iii
                    JNEW{i,1}(iii,:)=[];
                end
            end
            
            [sizetemp,lenghttemp]= size(JNEW{i,1});
        
        end
    end
    
    % We register the set of markings that are reached, the corresponding
    % justifications and the suffix w'
    Mw{t+1,1}=M;
    Mw{t+1,2}=JNEW;
    Mw{t+1,3}=[Mw{t,3},transition];
end

Mw=Mw(1:length(Mw)-1,:);   

end

