function [D] =BRD(T,Pre,Post,M0,F,L,E)

%  BRD:   This function builds the basis reachability diagnoser of basis 
%         markings of a Petri net.
%      
%         *****************************************************************
%                                   ## SYNTAX ##
%
%         [D] = BRD(T,Pre,Post,M0,F,L,E)
%
%         Given a Petri net with its Basis Reachability Graph (T), matrices
%         Pre and Post, the initial marking M0, the matrix F that indicates
%         the Fault class and the matrix L that indicates the Labeling
%         function, this function returns a matrix D that contains for each
%         row:
%         1) the node's number;
%         2) the basis markings that belong to the node;
%         3) the Xfault vector;
%         4) the transitions enabled at the node;
%         5) the Diagnosis State;
%         6) the couple of the enabled transition  and the correspondent
%            destination's node.
%         
%         NB:D{1,2}(1,1) is used to keep memory of the number of net's
%            place
%
%            ________________________________________________________
%           |      1      |              2                |    3     |
%           |_____________|_______________________________|__________|
%         D=|Node's Number| [Basis Marking,Xfault,h?{N,F}]| Diagnosis|
%           |             |                               |   State  |
%           |_____________|_______________________________|__________|
%            _______________________________
%           |         4         |   5   | 6 |
%           |___________________|_______|___|
%           |   Enabled labels  |Marking|Tag|
%           | & destination node| index |   |
%           |___________________|_______|___|
%
%         F must be a cell array that has as many rows as the number of
%         fault classes, that contains in each row the fault transitions
%         that belong to the corresponding fault class.
%         L must be a cell array that has as many rows as the cardinality
%         of the considered alphabet, that contains in each row the
%         observable transitions having the same label.
%         E must be a cell array that contains in each row a string of
%         characters, each one corresponding to a different label in the
%         considered alphabet. Obviously, the cell array E is ordered
%         according to L.
%           
%         See also: MBRG, BRG,showBRD, diagnosability


[m,n]=size(Post);
[c,p]=size(M0');
[numTclas,b]= size(F);
[sizeL,lenghtL]= size(L);

% We compute respectively the number of observable (no), faulty (nf),
% regular (nreg) and unobservable (nu) transitions

no=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    no=no+lenghtLi;
end


nf=0;
for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    nf=nf+lenghtFi;
end

nreg=n-no-nf;
nu=n-no;

numbers=zeros(1,n);
reg=ones(1,n);
c=0;

for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    for j=1:lenghtLi
        c=c+1;
        t=L{i,1}(1,j);
        reg(t)=0;
        numbers(c)=t;
    end
end

for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    for j=1:lenghtFi
        c=c+1;
        f=F{i,1}(1,j);
        reg(f)=0;
        numbers(c)=f;
    end
end

for i=1:n
    if (reg(i)==1)
        c=c+1;
        numbers(c)=i;
    end
end

% The verification of input parameters is done inside the call of function BRG

% Starting of timing computation of Basis Reachability Diagnoser
%tic


[m,n]=size(Post);
[c,p]=size(M0');
[numTclas,b]= size(F);
[sizeL,lenghtL]= size(L);


% Initialization of the Basis Reachability Diagnoser

% We must pay attention to the fact that h(i)=N is denoted as  "-1" while h(i)=F
% is denoted as "-2", to work with vectors only containing numbers.

% D=[{1}, {zeros(1,p+2*numTclas)},{[]},{[]},{[]} {0}];
D={[1], zeros(1,p+2*numTclas),[],[],[],[0]};

% The first row of the second column is used to save the number of places of the net, 
% a parameter used in the function showBRD
D{1,2}(1,1)=-p;

D{1,2}(2,1:p)=M0';
D{1,2}(2,p+1:p+numTclas)=T{1,3};
D{1,2}(2,p+1+numTclas:p+2*numTclas)=-1*ones(1,numTclas);
D{1,3}=T{1,3};
D{1,4}=empty_vector(sizeL);
D{1,5}=[0;0];

% While nodes with no Tag Exist we select one among them and explore it
% [ Remember that a node not already explored has a null tag ]

c=cell2mat(D(:,6));
% We create "d" as a vector with the coordinates of the nodes not explored
d=find(c==0);

while ~isempty(d)
    d=find(c==0);
    % selected a node of the BRD that should be explored, we consider, for all the markings that belong to it,
    % the set of markings reachable given all the possible labels.
    for z=1:sizeL
        clear Dnew Dold
        count=0;
        [numMbasisD,b]= size(D{d(1,1),2});
        Dnew=[{[]}, {[]},{[]},{[]},{[0]}, {0}];
        Dnew{1,2}=zeros(1,p+2*numTclas);
        Dnew{1,4}=empty_vector(sizeL);        
        % For all the basis markings belonging to the node of the BRD under consideration:
        for m=1:numMbasisD
            if(D{d(1,1),2}(m,1)<0)
                continue
            end
            Mcurrent=D{d(1,1),2}(m,1:p);
            Hcurrent=D{d(1,1),2}(m,p+numTclas+1:p+2*numTclas);
            [numofnodeT, lengthT]=size(T);
            
            % We look for the marking under analysis on the BRG
            for j=1:numofnodeT
                if(Mcurrent==(T{j,2})')
                    % Once the marking on the BRG has been found out, we save 
                    % the coordinates of the node and the maximum cardinality of the arcs
                    % associated with the enabled label
                    [numofarcMax,numoflabel]= size(T{j,8});
                    currentNode=j;
                end
            end

            if (T{currentNode,7}(1,z)==1)
                % If the z-th label is enabled, add the reached marking to the new node with the corresponding parameters, 
                % else move the next marking

                for i=1:numofarcMax
                    if(size(T{currentNode,8}{i,z})~=[0,0])
                        count=count+1;
                        nextNode=T{currentNode,8}{i,z}{1,3}{1,1};
                        % remember that the justification should keep into account
                        % the reordering of transitions:
                        jcurrent=T{currentNode,8}{i,z}{1,3}{1,2};
                        MbNext=(T{nextNode,2})';

                        % vector useful in the following to verify the
                        % occurrence of faults
                        temp=[no+1:n;jcurrent];

                        % Definition of the parameters of the new node
                        
                        % Index of the marking
                        Dnew{1,5}(count,1)=nextNode-1;
                        
                        % Basis marking in the second column
                        Dnew{1,2}(count,1:p)=MbNext;

                        % X-fault corresponding in third column
                         Dnew{1,2}(count,p+1:p+numTclas)=T{nextNode,3};
                        % we define a binary vector (hitF), associated
                        % witht the path, which keep into account the
                        % presence of fault transitions
                        hitF=zeros(numTclas);
                        for f=1:numTclas
                            [sizeFi,lenghtFi]= size(F{f,1});
                                   
                            % We define hitFi  as the firing vector relative to the transitions in the f-th fault class
                            hitFi=0;
                            for ff=1:lenghtFi
                                tf=F{f,1}(1,ff);
                                findtf=find(temp(1,:)==tf);
                                size(findtf);
                                if(size(findtf)==[1,1])
                                    hitFi(ff)=jcurrent(findtf);
                                end
                            end
                            % we define hitF as the binary vector such that
                            % hitF(f)=1, if a fault of the f-th class
                            % occurs in the path Mcurrent->MbNext;
                            % hitF(f)=0, otherwise.
                            
                            if (any(hitFi))
                              	hitF(f)=1;
                            else
                                hitF(f)=0;
                            end
                            
                            % Definition of h(i) \in {N,F}|{-1,-2}
                            if (Hcurrent(f)== -2)
                                Dnew{1,2}(count,p+numTclas+f)= -2;
                            else
                                if (hitF(f)==0)
                                    Dnew{1,2}(count,p+numTclas+f)= -1;
                                else
                                	Dnew{1,2}(count,p+numTclas+f)= -2;
                                end
                            end
                        end
                    % If an active arc is not present, we move to the next transition associated with the same label.
                    else
                        continue
                    end
                end
            % if from Mbcurrent the z-th label is not enabled, we move to
            % the next marking
            else
                continue
            end
        end


        % If the row relative to the marking is null, namely if it remains 
        % equal to its initial value, we move to the next iteration and look for a node
        % not explored yet
        
        if(Dnew{1,2}==0)
            continue
        end
        
        % The construction of the BASIS REACHABILITY DIAGNOSER may lead to have 
        % the same marking in a node with the same value of "h={N,F}", a possibility on which we are not interested.
        % We remove thus redundancies:
        
        A=Dnew{1,2};
        [a,b]=size(Dnew{1,2});
        for bb=1:a
            for aa=1:a
                if (aa~=bb &(Dnew{1,2}(bb,:)==A(aa,:)))
                    A(aa,:)=-10;
                    Dnew{1,2}(aa,:)=-10;
                end
            end
        end
        
        B=[];
        clear A
        countM=0;
        for bb=1:a
            if (Dnew{1,2}(bb,:)~=-10)
                countM=countM+1;
                A(countM,:)=Dnew{1,2}(bb,:);
                B(countM,1)=Dnew{1,5}(bb,1);
            end
        end
        Dnew{1,2}=A;
        Dnew{1,5}=B;
        
        % State of the diagnoser in the third column
        
        H=Dnew{1,2}(:,p+numTclas+1:p+2*numTclas);
        Fault=Dnew{1,2}(:,p+1:p+numTclas);
    
        for f=1:numTclas
            if(Fault(:,f)==0 & H(:,f)==-1)
                Dnew{1,3}{1}(1,f)=0;
            elseif(H(:,f)==-2)
                Dnew{1,3}{1}(1,f)=3;
                % At least one entry of X-fault should be unitary
            elseif(sum(Fault(:,f))>0 & H(:,f)==-1)
                Dnew{1,3}{1}(1,f)=1;
            else
                Dnew{1,3}{1}(1,f)=2;

            end
        end

        % Once the definition of the node is finished, we have to verify that 
        % it is not already present in the BRD
        % We keep memory of all the columns, with the exception of the ID of the node and of TAG.
    
        DDnew=Dnew{1,2};
        DDold=[];
        % We determine the number of nodes of the BRD (numofnodeD)
        [numofnodeD,lengthD]=size(D);
        % We determine the cardinality of the node at hand (sizeDDnew)
        [sizeDDnew,lenghtDDnew]=size(DDnew);
        exist=0;
        for dd=1:numofnodeD
            DDold=D{dd,2};
            [sizeDDold,lenghtDDold]=size(DDold);

            % If the cardinality of the nodes is the same, we verify that
            % they are representative of the same parameters (M & h).
      
            if (sizeDDold==sizeDDnew)
                for ddd=1:sizeDDnew
                    for dddd=1:sizeDDold
                        if (DDnew(ddd,:)==DDold(dddd,:))
                            DDold(dddd,:)=-10;
                        break
                        end
                    end
                end
              
                if(DDold==-10)
                    D{d(1,1),4}{z}{2}=dd;
                    D{d(1,1),4}{z}{1}=E{z,1};
                    exist=1;
                    break;
                end                
            end
        end
        if(exist==1)
            continue
        else
            D{numofnodeD+1,1}=numofnodeD+1;
            D{numofnodeD+1,2}=Dnew{1,2};
            D{numofnodeD+1,3}=Dnew{1,3}{1};
            D{numofnodeD+1,5}=Dnew{1,5};
            D{numofnodeD+1,6}=0;
            D{d(1,1),4}{z}{2}=numofnodeD+1;
            D{d(1,1),4}{z}{1}=E{z,1};

        end

        % If the node is present already, we add it to the BRD,
        % we set tag=1 and keep track of the fact that the arc of the previous node 
        % reaches the new node.

    end
    % We verify the nodes not yet considered
    D{d(1,1),6}=1;
    c=cell2mat(D(:,6));
    d=find(c==0);
end
%toc
%t=toc;
end