function showMBRG(T)

%showMBRG: This function shows Modified Reachability Graph of a P/T System.
%        
%          ****************************************************************
%                          ## SYNTAX ##
%
%          showMBRG(T)
%           This function shows how obtain a Petri Net's Modified Basis
%           Reachability Graph T step-by-step starting from the initial 
%           node and following all enabled labels and relative reached
%           nodes.
%
%          See also: BRG, MBRG, BRD, showBRG, showBRD, diagnosability



% Verifies that the number of input arguments to the function is equal to one. 
% If such is not the case, an error message is produced. 

ni=nargin;
error(nargchk(1,1,ni));

% Reads the number of nodes (n) and the number of transitions, including 
% observable and faulty transitions (t).

n=T{end,1};

% We do not need the number of transitions, rather the maximum number
% associated with transitions, since they do not have a predefined order.

[sizeL,LenghtL]=size(T{1,8});
t=0;
for f=1:n
    for ff=1:LenghtL
        [sizeL,LenghtL]=size(T{f,8});
        for fff=1:sizeL
            if(size(T{f,8}{fff,ff})==[0 0])
                continue
            else
                tt=T{f,8}{fff,ff}{1,2};
                    if(tt>t)
                        t=tt;
                    end
            end
        end
    end
end

% Number of digits useful to the enumeration of markings (ncifre) and of
% transitions (tcifre).
% [As an example, if we have 100 markings, we would be interested in enumerating 
% the initial marking as M�� rather than M�]

ncifre= ceil(log10(n));
tcifre= ceil(log10(t));

% Computation of the number of places of the net
[p,uno]=size(T{1,2});

% Display the number of nodes of the BRG

fprintf('\n Modified Basis Reachability Graph node''s number n = %d\n\n',n)

for f=1:n % for all the markings
    
    % Computation of the number of labels (LenghtL) and the maximum number of
    % transitions associated with a label (sizeL).
    [sizeL,LenghtL]=size(T{f,8});
    
    % Display the current marking
    
    fprintf('\n#\t\b Marking M')
    
    if(f-1<10)
        for zz=1:ncifre-1
            fprintf('0')
        end
    elseif(f-1<100)
        for zz=1:ncifre-2
            fprintf('0')
        end
    elseif(f-1<1000)
        for zz=1:ncifre-3
            fprintf('0')
        end
    elseif(f-1<10000)
        for zz=1:ncifre-4
            fprintf('0')
        end
    end
        
    fprintf('%d=[',f-1)
    for ff=1:p
        fprintf('%d ',T{f,2}(ff,1))
    end
    fprintf('\b]''t\b')
    
    % Display the fault diagnosis state (distinction between state 0 and 1).
    
    [x,y]=size(T{f,3});
    fprintf('\n  x=[')
    if y==0
        fprintf(' ]')
    else
        for xx=1:y
            fprintf('%d ',T{f,3}(1,xx))
        end
        fprintf('\b]')
    end
    

    
    % Display the transitions enabled from the considered marking
    % and the relative justification vector
    
    fprintf('\n\n \t\b Observable and faulty transitions enabled to fire:\n\t\b')
    
    % variable useful to verify if a node is "dead" 
    noarc=1;
    
    for ff=1:LenghtL
        for fff=1:sizeL
            [a,b]=size(T{f,8}{fff,ff});
            if a~=0
                noarc=0;
                l=T{f,8}{fff,ff}{1,1};
                t=T{f,8}{fff,ff}{1,2};
                n_next=T{f,8}{fff,ff}{1,3}{1};
                
                if (l==0)
                    fprintf('  eps')
                else
                    fprintf('  %s(t',l)
                end

                if(t-1<9)
                    for zz=1:tcifre-1
                        fprintf('0')
                    end
                elseif(t-1<99)
                    for zz=1:tcifre-2
                        fprintf('0')
                    end
                elseif(t-1<999)
                    for zz=1:tcifre-3
                        fprintf('0')
                    end
                elseif(t-1<9999)
                    for zz=1:tcifre-4
                        fprintf('0')
                    end
                end

                if (l==0)
                    fprintf('%d  -> M',t)
                else
                    fprintf('%d) -> M',t)
                end
                
                if(n_next-1<10)
                    for zz=1:ncifre-1
                        fprintf('0')
                    end
                elseif(n_next-1<100)
                    for zz=1:ncifre-2
                        fprintf('0')
                    end
                elseif(n_next-1<1000)
                    for zz=1:ncifre-3
                        fprintf('0')
                    end
                elseif(n_next-1<10000)
                    for zz=1:ncifre-4
                        fprintf('0')
                    end
                end
                
                % it could happen that the number of regular transitions
                % is null and, thus, the set of justifications is 
                % equal to the empty set
                [u,v]=   size(T{f,8}{fff,ff}{1,3}{2});
                if(v==0)
                    fprintf('%d:\t\b e=[]\n',n_next-1)
                else
                    fprintf('%d:\t\b e=[',n_next-1)
                    for c=1:v
                        fprintf('%d ',T{f,8}{fff,ff}{1,3}{2}(1,c))
                    end
                    fprintf('\b]\n')
                end
            end
        end
        
    end
    
	% Verifies if there exist output arcs from the current node
    if noarc==1
        fprintf('\t\b  None\n\t\b')
    end

    fprintf('\n\t\t*******\n')
end

% fprintf('\nShow Basis Reachability Graph:\n')
% disp(T)