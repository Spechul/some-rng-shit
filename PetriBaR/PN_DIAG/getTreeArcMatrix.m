function [Et] =getTreeArcMatrix(E)

%GETTREEARCMATRIX: This function get arc costs matrix of the Basis
%                  Reachability Diagnoser'graph
%
%                  ********************************************************
%                                        ## SYNTAX ##
%
%                  [Et] =getTreeArcMatrix(E)
%
%                  Given an arc cost Matrix, this function returns an arc
%                  cost matrix Et corrisponding to a coverability tree.
%
%                  See also: BRD, getBrdArcMatrix
%                           


% Verifies that the number of input arguments to the function is equal to two. 
% If such is not the case, an error message is produced.

ni=nargin;
error(nargchk(1,1,ni));

% Initialization of the cost matrix of the arcs of the covering tree
n=max(size(E));
Et=zeros(n);
node=zeros(1,n);
node(1,1)=1;

for i=1:n
    for j=1:n
        if(node(j)==0 & j~=i)
            if (E(i,j))
                Et(i,j)=1;
                node(j)=1;
            end
        end
    end
end


end
