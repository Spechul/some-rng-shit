function [Sol] =faultclass(Pre , Post , M0, nu , w ,F)

%   faultclass:	This function resolve the following constraits set  :
%                   _
%                  |  M0 +Cu*z>=0
%                 < 
%                  |_ z(tf)>0
%  
%                  Where M0 is the Marking obtained by the firing of the
%                  suffix i-w and tf is a fault transition.
%                  
%                  If the set admit solution the Sol will be 1, otherwise
%                  the Sol will be 0.    
%
%                  ********************************************************
%                                   ## SYNTAX ##
%                                   
%                  [Sol] =faultclass(Pre , Post , M0, nu , w ,F)
%
%                  Given a Petri net with its matrices Pre and Post,
%                  the initial marking M0, the number of unobservable
%                  transitions nu and the matrix F that  indicates the
%                  Fault class.
%                  F must be a cell matrix [numFaultclass X 1] that
%                  describes the faults class,in the i-th row of the matrix
%                  there will be the number of unobservable transition that
%                  corresponds at the Ti-th fault class.



[a,sizw]= size(w);
[numTclas,b]= size(F);

[pn,tn]=size(Pre);
[x,c]=size(M0);

 % verification of the input data
if (size(Post)==size(Pre))&(c==1)&(pn==x),
     elseif size(Post)~=size(Pre)
      fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
      fprintf('\n Insert again the data\n')
     else
      fprintf('\n ERRORE! dimensions of the  marking are wrong!\n')
      return
end

 if(a~=1)
     fprintf('\n ERROR! w must be a row array\n')
     return
 end          
 
C=Post-Pre;
Cu=(C(:,(tn-nu+1):tn));
x=0;

N=[];

Prenew=Pre;

    if(w==0)
        for j=1:tn-nu
            
        
        zer=zeros(1,tn);
        zer(j)=1;
        Prenew(pn+j,:)=zer; % we a control place to all the observable transitions
        % so that they may fire only as many times as the number of tokens
        % in the corresponding control placesaggiungo un posto di controllo a tutte le transizioni osservabili in modo che possano 
%         scattare solo tante volte quanti sono i gettoni nei corrispondenti posti di controllo
        end
        
        Postnew=[Post;zeros(tn-nu,tn)];% none of the observable transitions can be checked in this case
        M0new=[M0;zeros(tn-nu,1)];
        
    else

        


        for i=1:sizw
            [sizN,b]=size(N);
            if(sizN==0)
                N=zeros(1,2);
                N(1,1)=w(i);
                N(1,2)=1;

                [sizN,b]=size(N);
                
            else
                
            for k=1:sizN                
                if(N(k,1)==w(i))
                    N(k,2)=N(k,2)+1;
                    x=1;
                end
            end
            
            if(x==0)
                 N(sizN+1,1)=w(i);
                 N(sizN+1,2)=1;
            end
            x=0;
            
            end
            
        end
        
  
    
    [numt,vv]=size(N);
    
% We add as many control places as the number of different transitions.  
% Each control place contains a number of markings equal to the number of times the marking
% it controls, is repeated (w only contains controllable transitions)

%     M0new=[M0;zeros(numt,1)];

    M0new=[M0;zeros(tn-nu,1)];

    Postnew=[Post;zeros(tn-nu,tn)];
    
    
for l=1:numt        % for all the transitions belonging to the firing vector w
        
    zer=zeros(tn-nu,tn);
    
    for post=1:tn-nu
        
    zer(post,post)=1; %it is redefining zer=[Diag(n_o) | zeros (n_o X n_u)]
    
    end      
    
    Prenew=[Pre;zer];     
    
    % we add a control place to all the observable transitions that may
    % only fire a number of times equal to the number of tokens in the
    % corresponding control places

   M0new(pn+N(l,1))=N(l,2);     % the initial marking of the added places is equal
   % to the number of times the transition appears in w
   
end
    end
    
    
  G=graphPN(Prenew,Postnew,M0new);    % We construct the reachability graph starting from the new net
 [numMreach,lengG]=size(G);         % numReach=number of markings
 [nump,w]= size(M0new);
 
 Sol=zeros(1,numTclas);
 
  for m=1:numMreach-1    
      for l=nump+3:2:lengG;
          for k=1:numTclas
              lengTclass=length(F{k,1}); 
                                        
              for p=1:lengTclass;
                  if(G(m,l)==F{k,1}(1,p))
                      Sol(k)=1;                      
                        elseif(all(Sol))
                        return
                  end
              end
          end
      end
  end
  
end