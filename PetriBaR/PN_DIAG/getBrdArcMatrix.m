function [E] =getBrdArcMatrix(BRD)

%GETBRDARCMATRIX: This function get arc costs matrix of the Basis
%                 Reachability Diagnoser'graph
%
%                 *********************************************************
%                                       ## SYNTAX ##
%
%                 [E] =getBrdArcMatrix(BRD)
%
%                 Given the BRD, this function returns a binary Matrix E,
%                 similar to a incidence Matrix, such that:
%                           _
%                          |  1 if exist an arc from the i-th to j-t nodeh
%                          |    
%                 E(i,j)= < 
%                          |_ 0 otherwise
%           
%                 See also: BRD,getBrdArcMatrix


% It verifies that the number of input arguments to the function is equal to 
% two. If such is not the case, an error message is produced

ni=nargin;
error(nargchk(1,1,ni));

% Initialization of the cost matrix of the arcs
[sizeBRD,lenghtBRD]= size(BRD);
E=zeros(sizeBRD,sizeBRD);

for i=1:sizeBRD
    [sizeBRDi,lenghtBRDi]=size(BRD{i,4});
    for j=1:lenghtBRDi
        if(size(BRD{i,4}{j})~=0)
            to=BRD{i,4}{j}{2};
            E(i,to)=1;
        end
    end
end


end
