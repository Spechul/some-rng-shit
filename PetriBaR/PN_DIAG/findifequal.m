
% findifequal: This function find if in the columns of the input matrix 
%              there are someone equal to an other,in this case every 
%              copy is remove form the matrix and the output matrix
%              will be made with just a copy of each column
%
%                        ## SINTAX ##
% 
%              [G] = findifequal(M)
%              M is the input matrix or array
%              G is the output matrix or array made from just one copy of 
%              each different columns , thus in G each columns will be 
%              different from the others
 
function [G]=findifequal(M)
Msize=size(M);
G=[];
m=1;
if(Msize(2)==1)
    G=M;
    return
    
else
for k=1:Msize(2)
    for y=1:Msize(2)
            if(M(:,k)== M(:,y))
                if( k~= y )
                    M(:,y)=[NaN];
                end
            end
    end
end

F=isnan(M);
C = find(F(1,:)>0);
M(:,C(1,:))=[];
G=M;

end