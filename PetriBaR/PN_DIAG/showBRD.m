function showBRD(T)

%showBRD: This function shows Basis Reachability Diagnoser of a P/T System.
%        
%         *****************************************************************
%                               ## SYNTAX ##
%
%          showBRD(T)
%          This function shows how obtain a Petri Net's Basis Reachability
%          Diagnoser T step-by-step starting from the initial node and
%          following all enabled labels and relative reached nodes.
%
%         See also: MBRG, BRG, BRD, showMBRG, showBRG, diagnosability



% Verifies that the number of input arguments to the function is equal to one.
% If such is not the case, an error message is produced.

ni=nargin;
error(nargchk(1,1,ni));

% Reads the number of nodes (n)
n=T{end,1};

% Reads the number of markings (m).
M=T(:,5);
M=cell2mat(M);
m=max(M);

% Number of digits useful to enumerate the markings(ncifre) and the nodes.
% [As an example, if we have 100 markings, we would be interested in enumerating
% the initial marking as M�� rather than M�]

ncifre= ceil(log10(n));
mcifre= ceil(log10(m));

% Computation of the number of places of the net (p) and of the number of
% fault classes(numTclas)
p=-T{1,2}(1);
[tot1,tot2]=size(T{1,2});
numTclas=(tot2-p)/2;

% Display the number of nodes of the BRG

fprintf('\n Basis Reachability Diagnoser node number N = %d\n\n',n)

for f=1:n % for all the nodes
    
    % Display the current node
    
    fprintf('\n#\t\b Node N')
    
    if(f-1<10)
        for zz=1:ncifre-1
            fprintf('0')
        end
    elseif(f-1<100)
        for zz=1:ncifre-2
            fprintf('0')
        end
    elseif(f-1<1000)
        for zz=1:ncifre-3
            fprintf('0')
        end
    elseif(f-1<10000)
        for zz=1:ncifre-4
            fprintf('0')
        end
    end
        
    fprintf('%d',f-1)
    
    % Display the fault diagnosis state
    fprintf('\t Delta=[')
    if numTclas==0
        fprintf(' ]')
    else
        for fff=1:numTclas
            fprintf('%d ',T{f,3}(1,fff))
        end
        fprintf('\b]')
    end
    
    % Computation of the number of markings (sizeN) associated with the node.
    [sizeN,LenghtN]=size(T{f,2});
    
    fprintf('\n\n \t\b Basis Markings and corresponding X-fault and H:\t\b')
    for ff=1:sizeN
        
        if(T{f,2}(ff,1)>-1)
            % Display the ff-th marking of the node and the number mm
            % in the BRG/MBRG)
            mm=T{f,5}(ff,1);
            fprintf('\n\t\b  Marking M')
    
            if(mm-1<9)
                for zz=1:ncifre-1
                    fprintf('0')
                end
            elseif(mm-1<99)
                for zz=1:ncifre-2
                    fprintf('0')
                end
            elseif(mm-1<999)
                for zz=1:ncifre-3
                    fprintf('0')
                end
            elseif(mm-1<9999)
                for zz=1:ncifre-4
                    fprintf('0')
                end
            end
                fprintf('%d=[',mm)
                
            for fff=1:p
                fprintf('%d ',T{f,2}(ff,fff))
            end
            fprintf('\b]''t\b')

            % Display X-Fault (Distinction from state 0 and 1).
    
            fprintf('    x=[')
            for fff=p+1:p+numTclas
                fprintf('%d ',T{f,2}(ff,fff))
            end
            fprintf('\b]')
            
            % Display H.
            
            fprintf('    h=[')
            for fff=p+1+numTclas:p+2*numTclas
                if(T{f,2}(ff,fff)==-1)
                    fprintf('N ')
                elseif(T{f,2}(ff,fff)==-2)
                    fprintf('F ')
                end
            end
            fprintf('\b]')
            
        end
    end
    % Display the transitions enabled at the considered node and the
    % relative reached node
    
    % Variable useful to verify if a node is "dead"
    noarc=1;
    
    fprintf('\n\n \t\b Observable transitions enabled to fire:\n\t\b')
    [sizeT,lenghtT]=size(T{f,4});
    for ff=1:lenghtT
        [a,b]=size(T{f,4}{ff});
        if a~=0
            noarc=0;
            l=T{f,4}{1,ff}{1,1};
            n_next=T{f,4}{1,ff}{1,2};
                
            fprintf('  %s  -> N',l)

            if(n_next-1<10)
                for zz=1:ncifre-1
                    fprintf('0')
                end
            elseif(n_next-1<100)
                for zz=1:ncifre-2
                    fprintf('0')
                end
            elseif(n_next-1<1000)
                for zz=1:ncifre-3
                    fprintf('0')
                end
            elseif(n_next-1<10000)
                for zz=1:ncifre-4
                    fprintf('0')
                end
            end

            fprintf('%d\n',n_next-1)

        end
    end
     
	% Verifies if the there exist output arcs from the node
    if noarc==1
        fprintf('\t\b  None\n\t\b')
    end

    fprintf('\n\t\t*******\n')
end

% fprintf('\nShow Basis Reachability Graph:\n')
% disp(T)