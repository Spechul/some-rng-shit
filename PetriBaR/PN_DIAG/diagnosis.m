function       diagnosis(Pre, Post, M0, w, F, L, E)

%   diagnosis: This function computes diagnosis on-line for a generic Petri
%              net system.
%
%              ***********************************************************
%
%                                   ## SYNTAX ##
%                           
%              diagnosis(Pre, Post, M0, w, F, L, E)
%                
%              Given a Petri net with its matrices Pre and Post, the
%              initial marking M0, the observed word w, the matrix F 
%              representing the Fault class,the matrix L, that indicates
%              the Labeling function, and the Matrix E,that indicates the
%              Alphabet of label, this function returns a matrix where:
%              - in the first column, there is the actually fired suffix of
%                the considered word(w);
%              - in the second column, there is the corresponding Diagnosis
%                states.               
%    
%              F must be a cell matrix [numFaultclass X 1] that describes
%              the faults class: in the i-th row of the matrix there will
%              be the number of unobservable transition that corresponds at
%              the I-th fault class.L must be a cell matrix [numLabels X 1]
%              that describes the labeling function: in the i-th row of the
%              matrix there will be the number of the observable transition
%              that corresponds at the I-th label.
%              E must be a cell matrix [numLabels X 1] that describes the
%              symbol associated to the corresponding label: in the i-th
%              row of the matrix there will be the symbol that corresponds
%              at the I-th label.
%
%              The Diagnosis State for the i-th fault class is equal to:
%                
%              - 0   If the ith fault cannot have occurred, because none of
%                    the firing sequence consistent with the observation 
%                    contains fault transitions of class i;
%                 
%              - 1   If a fault transition of class i may have occurred,
%                    but is not contained in any justification of w;
%                
%              - 2   If a fault transition of class ith may have occurred,
%                    but is contained in one (but not in all) justification
%                    of w;
%                    
%              - 3   If the i-th fault must have occurred, because all
%                    firable sequences consistent with the observation
%                    contain at least one fault transition of class i.
%              
%              NB:
%              To distinguish between the case 0 and 1 we resolve the
%              following constraits set :
%                _
%               |  M +Cu*z>=0
%              <
%               |_ z(tf)>0
%                      
%              Where M is whichever Marking obtained by the firing of the 
%              suffix i-w and tf is a fault transition.
%
%              If the set admit solution the element will be 1, otherwise
%              the element will be 0.
%
%
%              See also: BRG, showBRG, diagnosability


%   /_____________ DATA PREPARATION_____________/

% Computation of the number of places (np), transitions (n), labels and
% fault classes:
[np,n]=size(Post);
[c,p]=size(M0');
[numLabel,lenghtL]=size(L);
[numTclas,b]= size(F);


% Computation of the number of observable (no), unobservable
% (nu), regular (nreg) and faulty (nf) transitions:

no=0;
for i=1:numLabel
    [sizeLi,lenghtLi]=size(L{i,1});
    no=no+lenghtLi;
end


nf=0;
for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    nf=nf+lenghtFi;
end

nreg=n-no-nf;

nu=nreg+nf;


% Since the transitions can be ordered arbitrarily, we enumerate them
% so as to have first the observable transitions, and the, the regular and
% the faulty transitions.

% NUMBERS is a vector used to keep tracK of the original enumeration of transitions
% EX: if numbers(i)=j then transition tj has been enumerated as ti
numbers=zeros(1,n);

% We define a binary vector such that:
%(reg(i)==1) if the i-th transition is a regular transition; and 
% a counter useful to reset the matrices defining the labeling function and the
% fault classes

MPre=[];
MPost=[];
MF=[];
ML=[];

reg=ones(1,n);
count=0;
for i=1:numLabel
    [sizeLi,lenghtLi]=size(L{i,1});
    for j=1:lenghtLi
        count=count+1;
        t=L{i,1}(1,j);
        reg(t)=0;
        numbers(count)=t;
        MPre=[MPre,Pre(:,t)];
        MPost=[MPost,Post(:,t)];
        ML{i,1}(1,j)=count;
    end
end

for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    for j=1:lenghtFi
        count=count+1;
        f=F{i,1}(1,j);
        reg(f)=0;
        numbers(count)=f;
        MPre=[MPre,Pre(:,f)];
        MPost=[MPost,Post(:,f)];
        MF{i,1}(1,j)=count;
    end
end

for i=1:n
    if (reg(i)==1)
        count=count+1;
        numbers(count)=i;
        MPre=[MPre,Pre(:,i)];
        MPost=[MPost,Post(:,i)];
    end
end

Pre=MPre;
Post=MPost;
L=ML;
F=MF;

%   /_____________ START COMPUTATION_____________/

% We set up the current marking equal to the initial marking of the net and 
% the corresponding justification equal to the null vector.
%
% Mold and Jold are, respectively, the matrices containing the basis markings 
% reached with the suffix under consideration and the corresponding justifications.
% Mnew e Jnew 
Mnew=[];
Jnew=[];
Mold=M0';
Jold=zeros(1,nu);

% Computation of the initial diagnosis state
%Sol =constrainset(Pre, Post, M0, nu, F);
% Alternatively, we may use:
Sol =faultclass(Pre , Post , M0, nu , 0 ,F);

% Initialization of the data matrix relative to the diagnosis
D{1,1}='eps';
D{1,2}=Sol;

% It verifies the dimensions of the observed word
[sizew,lenghtw]=size(w);

for i=1:lenghtw
	% we read the current label
    label=w(i);
    transition=[];
    
    % we initialize the counter of the pairs (M,y) that we are going to
    % compute
    countM=1;
    enabled=[];
    % We determine which transitions are associated with the current label
    for ii=1:numLabel
        if E{ii}==label
            transition=L{ii};
            [sizeTransition,lenghtTransition]=size(transition);
        end
    end

	% For each basis marking that is reached, we compute the basis markings
	% reachable with the firing of all transitions t such that L(t)=w(i), 
    % thus those transitions contained in "transition".
    [sizeM,lenghtM]=size(Mold);
    
    % For all the transitions associated with the observed label
    for ii=1:lenghtTransition
        t=transition(ii);
        % starting from the basis markings previously reached
        for iii=1:sizeM
            Mcurrent=Mold(iii,:);
            Jcurrent=Jold(iii,:);
            Mw=Mbasis(Pre,Post,Mcurrent',nu,t);
            
            
            % The function Mbasis is written in a way that requires to verify that
            % "t" is actually enabled at the current marking, i.e., the 
            % resulting marking has no negative entries.
            if all(Mw{2,1}>-1)==0
                enabled(ii)=0;
                continue
            else
                enabled(ii)=1;
            end
            
            % The marking is enabled, thus, we verify the reached marking
            % and the corresponding justification
            Mnext=Mw{2,1};
            [sizeJnext,lenghtJnext]=size(Mw{2,2});
            
            for iiii=1:sizeJnext
                Jnext=Mw{2,2}{iiii,:};
                Mnew(countM,:)=Mnext;
                Jnew(countM,:)=Jnext+Jcurrent;
                countM=countM+1;
            end
        end
    end
    
    % If no transition is enabled, the word cannot be observed, thus the 
    % diagnosis process is terminated:
	if enabled==0
        LastMarking=Mcurrent;
        labelUnfired=label;
        break
	end
    
    % Now er have a set of pairs (marking - justifications) on which the
    % computation of the diagnosis state is performed
    
    % For each fault class
    for ii=1:numTclas
        Tfi=F{ii};
        TEMP=(Jnew(:,Tfi-no));
        % once the elements relative to fault class at hand are extracted
        % from the j-vectors, we can compute the diagnosis state
        if(TEMP==0)
            %Sol=constrainset(Pre,Post,M0,nu,F);
            % alternatively, we may use:
            Sol =faultclass(Pre , Post , M0, nu , 0 ,F);

            % If the linear programming problem has no solution, then:
            if(Sol(1,ii)==0)
                StatoDiDiagnosi(1,ii)=0;
            % in the other case:
            else
                StatoDiDiagnosi(1,ii)=1;
            end
        % If all the occurrences of justification relative to the i-th fault class are positive, then:
        end
        [sizeTEMP,lenghtTEMP]=size(TEMP);
        for iii=1:lenghtTEMP
            if TEMP(:,iii)>0
                StatoDiDiagnosi(1,ii)=3;
                break
            else
                StatoDiDiagnosi(1,ii)=2;
            end
        end
    end

    % We write data on the array matrix D, output of the function
    
    if(i==1)
        D{i+1,1}=w(i);
    else
        D{i+1,1}=[D{i,1},w(i)];
    end
    D{i+1,2}=StatoDiDiagnosi;
    
    Jold=Jnew;
    Mold=Mnew;
    Mnew=[];
    Jnew=[];
    StatoDiDiagnosi=zeros(1,numTclas);
end


%   /_____________ Output of the results_____________/

[sizeD,lenghtD]=size(D);

fprintf('\n\n The following results summarize the step of the on-line')
fprintf('\n diagnosis carried out by the observed word:')
fprintf('\n\t- "%s".\n\n\n',w')

fprintf('')
fprintf('\tSUFFIX\t\t\t\t\tDIAGNOSIS STATE\n\n')
for i=1:sizeD
    fprintf('\t %s\t\t\t\t',D{i,1})
    if i==1
        fprintf('\t\t')
    elseif i<4
        fprintf('\t\t\t')
    elseif i<8
        fprintf('\t\t')
    elseif i<12
        fprintf('\t')
    elseif i>15 & i<20
        fprintf('\b')
    elseif i>19
        fprintf('\b\b')
    end
    fprintf('%d ',D{i,2})
    fprintf('\n')
end

if sizeD~=lenghtw+1
    % If no transition is enabled, the word cannot be observed, thus the 
    % diagnosis process is terminated
    fprintf('\n\n- WARNING')
    fprintf('\n  The on-line diagnosis has been terminated because\n')
    fprintf('  transition "%c" cannot fire from the marking\n  [',labelUnfired)
    fprintf('%d ',LastMarking)
    fprintf('\b].\n\n')
end


end