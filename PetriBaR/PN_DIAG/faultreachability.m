function [N,hit] =faultreachability(MBRG,node_start)

%faultreachability: This function verifies the rechability of a MBRG's node
%                   with firing of faulty transitions only.
%
%                   *********************************************************
%                                      ## SYNTAX ##
%
%                   [fr] =faultreachability(MBRG)
%
%                   Given the MBRG, this function a matrix fr such that:
%
%                   fr(i)={j:exist a path i->j with only faulty transition}
%
%                   if diag(i)==0, then no faulty transition are enable
%                   from i-th node
%             
%           
%                   See also: diagnosability

[n_node,eight]=size(MBRG);

[one,numTclas]=size(MBRG{1,3});

N=node_start;
hit= [];
hit{1,1}=zeros(1,numTclas);
tag=0;
count=0;

[d,dd]=size(N);

% A path cannot be longer than the total number of nodes;  the
% investigation ends if such is the case or all the paths are tagged.

% counter useful to keep track of the evolution of hit
step=0;

while tag~=1
    count=0;
    [multeplicity,numOfArc]=size(MBRG{1,8});
    [d,dd]=size(N);
    % Number of labels of the language
    numLabel=numOfArc-numTclas;
	
    step=step+1;
    
    % The path is finished if the current node is already present in the list,
    % thus either in the case of a cycle, or if no output arcs exist from a node, 
    % namely the node is dead.

    for i=1:d

        if tag(i)==0
            % If the path is not finished
            n_current=N(i,dd);
            [multeplicity,numOfArc]=size(MBRG{n_current,8});
            % noarc=1 if from the node under consideration, there does not 
            % exist a faulty output arc. In such a case the path is tagged.
            noarc=1;
            x=0;
            % For all the faulty classes
            for k=1:numTclas
                % We verify the possibility that there exist more than one 
                % fault in the class
                for kk=1:multeplicity

                    % If a faulty arc exists
                    if(size(MBRG{n_current,8}{kk,k+numLabel})~=0 & MBRG{n_current,8}{kk,k+numLabel}{1}==0)
                         n_next=MBRG{n_current,8}{kk,k+numLabel}{3}{1};
                        % We add the node just reached to the vector that contains 
                        % the nodes beloning to the path, and the label to the 
                        % vector that contains the consistent word
                        if(x==0)
                            N(i,dd+1)=n_next;
                             x=1;
                             noarc=0;
                             if(~exist('hit(i,step)'))
                                 hit{i,step}=zeros(1,numTclas);
                             end
                             hit{i,step}(k)=1;

                        else
                            count=count+1;
                            N(d+count,:)=[N(i,1:dd),n_next];
                            hit(d+count,1:step-1)=hit(i,1:step-1);
                            hit{d+count,step}=zeros(1,numTclas);
                            hit{d+count,step}(k)=1;
                            tag(d+count,1)=0;
                        end
                    end
                end
            end
        
            % We tag the path which has no output arcs
            if noarc==1
                tag(i)=1;
            end
            
        end
    end
	[d,dd]=size(N);
end

[sizehit,lenghthit]=size(hit);

% For all the rows
for i=1:sizehit
        hit{i,1}=boolean(hit{i,1});
end

% For all the rows
for i=1:sizehit
	% For all the columns
    for j=2:lenghthit
        if size(hit{i,j-1})~=0
            hit{i,j}=boolean(hit{i,j}+hit{i,j-1});
        else
            hit{i,j}=boolean(hit{i,j}+zeros(1,numTclas));
        end
    end
end




end