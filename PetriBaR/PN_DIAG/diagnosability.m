function diagnosability(MBRG,BRD)

%DIAGNOSABILITY: This function verifies necessary and sufficient conditions 
%                for diagnosability of bounded Petri nets system.
%                The method to test diagnosability is based on the analysis
%                of two graphs, the MBRG (Modified Basis Reachability 
%                Graph) and the BRD (Basis Reachability Diagnoser).
%
%                NB: as soon as we find an indeterminate cycle for a fault
%                class, we stop to search other indeterminate cycle fort
%                and this class conclude that the system is notdiagnosable 
%                wrt that fault class.
%
%                **********************************************************
%                                      ## SYNTAX ##
%
%                diagnosability(MBRG,BRD)
%
%                Given the MBRG and the associated BRD, this function
%                returns faulty class that are not diagnosable.
%           
%                See also: MBRG, BRG, BRD, showMBRG, showBRG, showBRD


% It verifies that the number of input arguments to the function is equal to two. 
% If such is not the case, an error message is returned. 

ni=nargin;
error(nargchk(2,2,ni));

% Starting of the compution of the time spent to verify the necessary and 
% sufficient conditions for the diagnosability

tic

% Initialization of the vector 'N', where to write the sequence of nodes
% visited along the path. The vector 'W', where we will go to write the observed
% word consistent with the executed path, will be initialized at the next step.

N=1;
diag=ones(size(BRD{1,3}));
CYCLE=[];
WORD=[];
%W='a';
% Counter of cycles

count_c=0;

% Until there exsts at least one node that is either not dead, or not belonging to a cycle,
% it will beling the matrix/vector N and it will be thus explored.

[d,dd]=size(N);

while d~=0
    % The paths not terminating in a cycle or in a "dead" node should be explored. 
    [d,dd]=size(N);

	cl=ones(1,d);
    % Counter of the output arcs added to the explored nodes
	count=0;
    for i=1:d
        n_current=N(i,dd);
        [one,numOfArc]=size(BRD{n_current,4});
        x=0;
        for k=1:numOfArc
            if(size(BRD{n_current,4}{k})~=0)
                n_next=BRD{n_current,4}{k}{2};
                label =BRD{n_current,4}{k}{1};
                % We add the node just reached to the vector containing
                % the nodes in the path, and the label in the vector
                % containing the consistent word
                if(x==0)
                    N(i,dd+1)=n_next;
                    W(i,dd)=label;
                    x=1;
                    cl(1,i)=0;
                else
                    count=count+1;
                    N(d+count,:)=[N(i,1:dd),n_next];
                    W(d+count,:)=[W(i,1:dd-1),label];
                    cl(1,d+count)=0;
                end
            end
        end
    end
    
    
    [d,dd]=size(N);
	
    % we cancel the paths that end in nodes from which no transition is enabled
	for i=d:-1:1
        if cl(1,i)==1
            N(i,:)=[];
            W(i,:)=[];
        end
    end
    
    
    [d,dd]=size(N);
    
    % Verification of the presence of possible cycles among the paths just computed
    
    % Be careful to the structure of
    %       __________________________________
    %      |     1     |     2     |    3     |
    %      |___________|___________|__________|
    % WORD=| Observable| Observable|  binary  |
    %      |Observable |Observable |  vector  |
    %      |    Path   |    Path   |    b     |
    %      |___________|___________|__________|
    %
    % Where binary-vector is such that:
    %                        _
    %                       |  1 if the cycle is candidate to be
    %                       |    indeterminate for the i-th fault class
    %                b(i)= <     
    %                       |
    %                       |_ 0 otherwise
	%        _________________________
    %       |     1      |     2      |
    %       |____________|____________|
    % CYCLE=|  Sequence  |  Sequence  |
    %       |  of nodes  |  of nodes  |
    %       |visited in  |visited in  |
    %       |  the path  |  the path  |
    %       |____________|____________|
    
    count_c=0;
    
    for i=d:-1:1
        n_current=N(i,dd);
        exist=find(N(i,1:dd-1)==n_current);
        [e,ee]=size(exist);
        if(ee~=0)
            count_c=count_c+1;
            CYCLE{count_c,1}=N(i,1:exist-1);
            CYCLE{count_c,2}=N(i,exist:dd);
            WORD{count_c,1}=W(i,1:exist-1);
            WORD{count_c,2}=W(i,exist:dd-1);
            WORD{count_c,3}=zeros(size(BRD{1,3}));
            
            % If the path is identified as a cycle, it stops being explored 
            % (elementary cycle)
            N(i,:)=[];
            W(i,:)=[];
        end
    end
    
    
    %**********************************************************************
    %   We want to be sure that, in the case that a cycle candidate to be
    %   indeterminates is obtained, it is explored in the MBRG, in order to 
    %   guarantee that non all cycles are explored if not necessary.
    %**********************************************************************
    
    [sizeCYCLE,lenghtCYCLE]=size(CYCLE);
    [b,numTclas]= size(BRD{1,3});
    
    % If among the analyzed paths, cycles are found out
    if sizeCYCLE~=0
        % it is verified if among them there are indeterminate cycles

        % Thus, we only have to verify that they only contain nodes with diagnosis 
        % state in {1,2}
        for i=sizeCYCLE:-1:1
            for k=1:numTclas
                [sizeCYCLEi,lenghtCYCLEi]=size(CYCLE{i,2});
                for j=1:lenghtCYCLEi
                    node=CYCLE{i,2}(1,j);
                    diag_state=BRD{node,3}(k);
                    if (diag_state==0)
                        WORD{i,3}(k)=0;
                        break
                    elseif(diag_state==3)
                        WORD{i,3}(k)=0;
                        break
                    else
                        WORD{i,3}(k)=1;
                    end
                end
            end

            if(WORD{i,3}==0)
                WORD(i,:)=[];
                CYCLE(i,:)=[];
            end
        end  
    end
    
	% sizeCYCLE= number of cycles candidate to be indeterminate
    [sizeCYCLE,lenghtCYCLE]=size(CYCLE);
    [b,numTclas]= size(BRD{1,3});
    
	if sizeCYCLE~=0
        
        % Now, we only have to establish if the cycles are indeterminate or not.
        % For each fault class, we verify if there exists in the BRD a cycle
        % to which it corresponds a pair of cycles in the MBRG with the same
        % observable projection, one containing the fault and the other one not.

        % The matrix/vector
        candidates=cell2mat(WORD(:,3));

        % We verify the number of cycles candidate to be indetarminate
        [numcycle,numTclas]=size(candidates);

        if numcycle==0
            continue
        end

        
        % For each fault class we have to verify that there exists an
        % indeterminate cycle in the MBRG



        PATH{1,1}=1;
        % Denotes if a fault has occurred or not. Initially it is set up
        % to False
        PATH{1,2}=zeros(1,numTclas);
        
            %*************************************************************
            %   It is necessary to include all those nodes with silent
            %   transitions enabled at the initial node
            %*************************************************************
            
        [NN,hit] =faultreachability(MBRG,1);
        [sizeNN,lenghtNN]=size(NN);
        countf=1;
        if(lenghtNN>1)
            for iii=1:sizeNN
                for jjj=2:lenghtNN
                    current_node=NN(iii,jjj);
                    % If the reached node is not present already among those
                    % reachable from the initial node, we create a new path
                    founded=find(PATH{1,1}==current_node);
                    [sizefounded,lenghtfounded]=size(founded);
                    
                    if sizefounded==0
                        countf=countf+1;
                        PATH{1,1}(countf,1)=current_node;
                        PATH{1,2}(countf,:)=hit{iii,jjj-1};
                    else
                        % If the node os already present, but it is reached 
                        % with a different fault dynamics, we create a new path
                        for iiii=1:sizefounded
                            if hit{iii,jjj-1}~=PATH{1,2}(founded(iiii),:)
                                countf=countf+1;
                                PATH{1,1}(countf,1)=current_node;
                                PATH{1,2}(countf,:)=hit{iii,jjj-1};
                            end
                        end
                    end
                end
            end
        end
            %************************************
            %   Fault transitions verified
            %************************************
            
        PATHold=PATH;
            
        % For each fault class
        for c=1:numTclas
            % we keep track of the indices relative to the cycles that could be
            % indeterminate for the c-th fault class
            candidatesc=find(candidates(:,c)==1);
            [sizeCand,lenghtCand]=size(candidatesc);
    
            % for all those cycles of the BRD that are candidate to be
            % indeterminate for the c-th fault class
            for cc=1:sizeCand
                % if the class is not diagnosable, we are not interested in determining other indeterminate cycle!!
                if(diag(c)==0)
                    continue
                else
                PATH=PATHold;
        
                % For any cycle that has been found out,
                % we read the word that we have to verify on the MBRG
                cycle=WORD{candidatesc(cc),2};
                path=WORD{candidatesc(cc),1};
                [one,leghtc]=size(cycle);
                [one,leghtp]=size(path);
        
        % We verify that such a word corresponds to at least one pair of cycles 
        % in the MBRG with the features we were looking for (we save the
        % generated paths in terms of a sequence of visited nodes).
        
                % For all the labels that compose the path at hand
                for q=1:leghtp
                    [sizePATH,lenghtPATH]=size(PATH{1,1});
                    % We reset the counter of the paths added to
                    % "sizePATH" already considered
                    count=0;
                    cl=ones(1,sizePATH);
                    for qq=1:sizePATH

                        % We read the current node
                        n_current=PATH{1,1}(qq,1);
                
                        % We verify the "cardinality" of the output arcs
                        [multeplicity,numOfArc]=size(MBRG{n_current,8});
                
                        % We reset to zero the counter of the arcs exiting from
                        % n_current with the same label
                        x=0;
                        % we exclude the fault transitions that will be considered 
                        % later on
                        for yy=1:numOfArc-numTclas
                            for xx=1:multeplicity
                                % if there exists an arc labeled with the label of interest
                                if size(MBRG{n_current,8}{xx,yy})~=0
                                    if (MBRG{n_current,8}{xx,yy}{1}==path(q))
                                        if (x==0)
                                            PATH{1,1}(qq,1)=MBRG{n_current,8}{xx,yy}{3}{1};
                                            x=1;
                                            cl(1,qq)=0;
                                        else
                                            count=count+1;
                                            PATH{1,1}(sizePATH+count,1)=MBRG{n_current,8}{xx,yy}{3}{1};
                                            PATH{1,2}(sizePATH+count,:)=PATH{1,2}(qq,:);
                                            cl(1,count+sizePATH)=0;
                                        end
                                    end
                                end
                            end
                        end
                    end

            % we cancel the paths that end in nodes where the transition at hand is not enabled
                    for qq=sizePATH:-1:1
                        if cl(1,qq)==1
                            PATH{1,1}(qq,:)=[];
                            PATH{1,2}(qq,:)=[];
                        end
                    end
                    [sizePATH,lenghtPATH]=size(PATH{1,1});
            
            %*************************************************************
            %  It is necessary to include all those nodes that are reachable
            %  with silent transitions strating from the reached nodes
            %*************************************************************
            for qq=1:sizePATH
                    currentnode=PATH{1,1}(qq,1);
                    [NN,hit] =faultreachability(MBRG,currentnode);
                    [sizeNN,lenghtNN]=size(NN);
                    countf=0;
                    % if no further nodes are reached:
                    if(lenghtNN>1)
                        for iii=1:sizeNN
                            for jjj=2:lenghtNN
                                current_node=NN(iii,jjj);
                    % If the reached node is not yet present among those that are
                    % reachable from the initial node, we create a new path

                                founded=find(PATH{1,1}==current_node);
                                [sizefounded,lenghtfounded]=size(founded);
                    
                                if sizefounded==0
                                    countf=countf+1;
                                    PATH{1,1}(sizePATH+countf,1)=current_node;
                                    PATH{1,2}(sizePATH+countf,:)=hit{iii,jjj-1}+PATH{1,2}(qq,:);
                                else
                            % If it is present, but is reached with a different fault
                            % dynamics, we create a new path
                                    for iiii=1:sizefounded
                                        if hit{iii,jjj-1}~=PATH{1,2}(founded(iiii),:)
                                            countf=countf+1;
                                            PATH{1,1}(sizePATH+countf,1)=current_node;
                                            PATH{1,2}(sizePATH+countf,:)=hit{iii,jjj-1}+PATH{1,2}(qq,:);
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            
            %************************************
            %  Fault transitions verified
            %************************************

            % if check if repetitions exist
                matPATH=cell2mat(PATH);
                [sizePATH,lenghtPATH]=size(PATH{1,1});            
                for qq= sizePATH:-1:1
                    xxx=matPATH(qq,1);
                    ff=find(matPATH(:,1)==xxx);
                    [sizef,lenghtf]=size(ff);
                    if sizef~=0
                        for fff=1:sizef
                            if matPATH(ff(fff),:)==matPATH(qq,:) & qq~=ff(fff)
                                matPATH(qq,:)=[];
                                PATH{1,1}(qq,:)=[];
                                PATH{1,2}(qq,:)=[];
                                break
                            end                        
                        end
                    end              
                end
            end
        
        
        % We have computed all the nodes and should verify if they are 
        % starting points for possible cycles

            [sizePATH,lenghtPATH]=size(PATH{1,1});            

            PATH{1,1}(:,2)=PATH{1,1}(:,1);

            for q=1:leghtc
                [sizePATH,lenghtPATH]=size(PATH{1,1});
                % We reset the counter of the paths added to "sizePATH"
                % that have been already considered
                count=0;
                cl=ones(1,sizePATH);
                for qq=1:sizePATH

                % We read the current node
                    n_current=PATH{1,1}(qq,2);
                
                % We verify the "cardinality" of the output arcs
                    [multeplicity,numOfArc]=size(MBRG{n_current,8});
                
                % We reset to zero the counter of the arcs exiting from
                % n_current with the same label
                    x=0;
                % we exclude the fault transitions that we will analyze later on
                    for yy=1:numOfArc-numTclas
                        for xx=1:multeplicity
                        % If there exists an arc labeled with a label of interest
                            if size(MBRG{n_current,8}{xx,yy})~=0
                                if (MBRG{n_current,8}{xx,yy}{1}==cycle(q))
                                    if (x==0)
                                        PATH{1,1}(qq,2)=MBRG{n_current,8}{xx,yy}{3}{1};
                                        x=1;
                                        cl(1,qq)=0;
                                    else
                                        count=count+1;
                                        PATH{1,1}(sizePATH+count,:)=[PATH{1,1}(qq,1), MBRG{n_current,8}{xx,yy}{3}{1}];
                                        PATH{1,2}(sizePATH+count,:)=PATH{1,2}(qq,:);
                                        cl(1,count+sizePATH)=0;
                                    end
                                end
                            end
                        end
                    end
                end
            
            % we cancel the paths that ends in nodes from which the transition at hand is not enabled
                for qq=sizePATH:-1:1
                    if cl(1,qq)==1
                        PATH{1,1}(qq,:)=[];
                        PATH{1,2}(qq,:)=[];
                    end
                end

                [sizePATH,lenghtPATH]=size(PATH{1,1});
            
            %*************************************************************
            %  It is necessary to include all those nodes that are reachable with
            %  silent transitions starting from the reached nodes
            %*************************************************************
                for qq=1:sizePATH
                    currentnode=PATH{1,1}(qq,2);
                    [NN,hit] =faultreachability(MBRG,currentnode);
                    [sizeNN,lenghtNN]=size(NN);
                    countf=0;
                    if(lenghtNN>1)
                        for iii=1:sizeNN
                            for jjj=2:lenghtNN
                                current_node=NN(iii,jjj);
                    % If the reached node is not already present among those 
                    % reachable from the initial node, we create a new path

                                founded=find(PATH{1,1}(:,2)==current_node);
                                [sizefounded,lenghtfounded]=size(founded);
                    
                                if sizefounded==0
                                    countf=countf+1;
                                    PATH{1,1}(sizePATH+countf,2)=current_node;
                                    PATH{1,1}(sizePATH+countf,1)=PATH{1,1}(qq,1);
                                    PATH{1,2}(sizePATH+countf,:)=hit{iii,jjj-1}+PATH{1,2}(qq,:);
                                else
                            % If it is present but it is reached with a different fault dynamics
                            % we create a new path
                                    for iiii=1:sizefounded
                                        if hit{iii,jjj-1}~=PATH{1,2}(founded(iiii),:)
                                            countf=countf+1;
                                            PATH{1,1}(sizePATH+countf,2)=current_node;
                                            PATH{1,1}(sizePATH+countf,1)=PATH{1,1}(qq,1);
                                            PATH{1,2}(sizePATH+countf,:)=hit{iii,jjj-1}+PATH{1,2}(qq,:);
                                        end
                                    end
                                end
                            end
                        end
                    end                    
                end
            
            %************************************
            %  Fault transitions verified
            %************************************

            % we verify if repetitions exist
                matPATH=cell2mat(PATH);
                [sizePATH,lenghtPATH]=size(PATH{1,1});            
                for qq= sizePATH:-1:1
                    xxx=matPATH(qq,2);
                    ff=find(matPATH(:,2)==xxx);
                    [sizef,lenghtf]=size(ff);
                    if sizef~=0
                        for fff=1:sizef
                            if matPATH(ff(fff),:)==matPATH(qq,:) & qq~=ff(fff)
                                matPATH(qq,:)=[];
                                PATH{1,1}(qq,:)=[];
                                PATH{1,2}(qq,:)=[];
                                break
                            end                        
                        end
                    end              
                end
            
            end
        
%         For a single fault class, we have a matriz whose rows are in the form:
%         
%         PATH={[i,j],[{1,0}^r]}
%         
%         if i==j then we are in the presence of a cycle, we only have to determine two rows,
%         the first one having a zero in the second column, and the second
%         one having a one in the same column, so as to impose diag(c)=0
%       
%         Note that we will use faultycicle and regularcycle to keep track of the
%         presence or absence of paths with or without faults.

            faultycicle=0;
            regularcycle=0;
        

            [sizePATH,lenghtPATH]=size(PATH{1,1});            
            for q=1:sizePATH
                    % If we are in the presence of a cycle
                if PATH{1,1}(q,1)==PATH{1,1}(q,2)
                        % If the cycle occurs after the firing of a fault transition in the c-th class
                    if PATH{1,2}(q,c)==1
                        faultycicle=1;
                        % othewise
                    elseif PATH{1,2}(q,c)==0
                        regularcycle=1;
                    end
                end

                    %If we have found both cycles we were looking for:
                if regularcycle==1 & faultycicle==1
                    diag(c)=0;
                    break
                end            
            end

        
                % If an indeterminate cycle is found out, we start examining 
                % the next fault class, otherwise, another cycle is explored.
            if(diag(c)==0)

                % Saving of indeterminate cycles
                cycle_indeterminate{c,1}=path;
                cycle_indeterminate{c,2}=cycle;
                
                break
            else
                fprintf('The following cycle is not indeterminate for class %d\n\n ',c)
                fprintf(' path = %s\n\n',path)
                fprintf(' cycle= %s\n\n',cycle)
            end
                end
            end
    
        end

    end

    
    if(diag==0)
        if numTclas==1
            fprintf('\nThe fault class in analysis can not be diagnosed')
            fprintf('\n since there is an indeterminate cycle.\n\n')
        else
            fprintf('\nAll fault classes are not diagnosable')
            fprintf('\n since for each of them there is an indeterminate')
            fprintf('\n loop.\n\n')
        end
    
        for i=1:numTclas
            if diag(i)==0
                fprintf('Fault class %d:\n',i)
                fprintf('\t path = %s\n',cycle_indeterminate{i,1})
                fprintf('\t cycle= %s\n\n',cycle_indeterminate{i,2})
            end
        end
        
        toc;
        t=toc;
    
        return 
    end

    t=toc;
    if t> 120
        return
    end
    
end

if(diag==1)
	fprintf('\nAll fault classes are diagnosable, since')
	fprintf('\n we have not found any indeterminate cycle.\n')
else
    fprintf('\nThe following fault classes are not diagnosable, since')
	fprintf('\n for each of them there is an indeterminate cycl.\n\n')
    
	for i=1:numTclas
        if diag(i)==0
            fprintf('Fault class %d:\n',i)
            fprintf('\t path = %s\n',cycle_indeterminate{i,1})
            fprintf('\t cycle= %s\n\n',cycle_indeterminate{i,2})
        end
	end
end

% Saving of the total time spent for running the program
toc;
t=toc;

end
