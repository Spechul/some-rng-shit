% Petri Net Diagnosability Toolbox
%
% Version 1   July 2009
%
% DIAGNOSIS:
%   graph    -  -  -  - builds the Reachability/Coverability Graph of a P/T
%                       system
%   BRG   -  -  -  -  - builds the Basis Reachability Graph of a P/T system
%   showBRG  -  -  -  - shows the Basis Reachability Graph of a P/T system
%   diagnosis   -  -  - for a given word computes diagnosis online of a P/T
%                       system
%   Mbasis   -  -  -  - compute couple of basis marking - relative j-vector
%                       for a a P/T system
%   miny  -  -  -  -  - computes minimal explanations of a P/T system
%
% DIAGNOSABILITY:
%   MBRG  -  -  -  -  - builds the Modified Basis Reachability Graph of a
%                       P/T system
%   showMBRG -  -  -  - shows the Modified Basis Reachability Graph of a
%                       P/T system
%   BRD   -  -  -  -  - builds the Basis Reachability Diagnoser of a P/T
%                       system
%   showBRD  -  -  -  - shows the Basis Reachability Diagnoser of a P/T
%                       system
%   diagnosability -  - checks if a P/T system is diagnosable wrt the i-th
%                       faulty class
%
% UTILITIES:
%   findifequal -  -  - check redundant columns from a given matrix imput
%   empty_vector   -  - computes an empty cell-vector of arbitrary
%                       dimension
%   constrainset   -  - checks if each faulty class is enabled for a
%                       specific P/T system
%   faultclass  -  -  - checks if each faulty class is enabled for a
%                       specific P/T system (same purpose of constrainset)
%   faultreachability - computes all MBRG's nodes reachable from a given
%                       other one by firing of faulty transition only
%   model -  -  -  -  - builds the parametric Petri net system analised
%                       
%   glpkmex  -  -  -  - MEX Interface for the GNU GLPK library.
%                       This routine calls the glpk library to solve a
%                       LP/MIP problem.
%
%
%       Copyright (c) 2009 by Marco Pocci
%                  [ except for graph, miny and glpkmex ]