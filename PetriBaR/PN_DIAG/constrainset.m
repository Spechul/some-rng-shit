function [Sol] =constrainset(Pre , Post , M , nu , F)
addpath(genpath(pwd));
% constrainset:    This function resolve the following constraits set  :
%                   _
%                  |  M0 +Cu*z >=0
%                 <                 .
%                  |_ z(tf) >0
% 
%                  Where M0 is whichever Marking obtained by the firing of
%                  the suffix i-w and tf is a fault transition.
%                  If the set admit solution the Sol will be 1, otherwise
%                  the Sol will be 0.
%
%                  ********************************************************
%                                       ## SYNTAX ##
%
%                  [Sol] =constrainset(Pre, Post, M0, nu, F)
%
%                  Given a Petri net with its matrices Pre and Post, the
%                  initial marking M0, the number of unobservable
%                  transitions nu and the matrix F, that indicates the
%                  Fault class.
%                  F must be a cell matrix [numFaultclass X 1] that 
%                  describes the faults class:in the i-th row of the matrix
%                  there will be the numbers of unobservable transitions
%                  that belong at the Ti-th fault class.


[numTclas,b]= size(F);
[pn,tn]=size(Pre);
[x,c]=size(M);
Sol=zeros(1,numTclas);

 % verification of the input data
if (size(Post)==size(Pre))&(c==1)&(pn==x),
     elseif size(Post)~=size(Pre)
      fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
      fprintf('\n Insert again the data\n')
     else
      fprintf('\n ERRORE! dimensions of the  marking are wrong!\n')
      return
end

    
 
C=Post-Pre;
Cu=(C(:,(tn-nu+1):tn));

for i=1:numTclas
     Faultclass=F{i,1};
    [bb,nfaulttran]=size(Faultclass);
    c=zeros(1,nu);
    
    for k=1:nfaulttran
        tfault=Faultclass(k);        
        c(tfault-(tn-nu))=1;
    end


SENSE=-1;
A=Cu;
B=-M;
C=c';
CTYPE='L';
CTYPE(1:pn)='L';
CTYPE=CTYPE';
LB=zeros(nu,1);
UB=Inf(nu,1);
VARTYPE='I';
VARTYPE(1:nu)='I';
VARTYPE=VARTYPE';
PARAM=0;
LPSOLVER=1;
SAVE=0;

[XMIN,FMIN]=glpkmex(SENSE,C,A,B,CTYPE,LB,UB,VARTYPE,PARAM,LPSOLVER,SAVE);



if(FMIN>0)   
    Sol(i)=1;
else
    Sol(i)=0;
end

end