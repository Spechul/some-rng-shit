function [T] =MBRG(Pre,Post,M0,F,L,E)

%MBRG:  This function builds the modified basis reachability graph of
%       basis markings of a Petri net.
%     
%       *****************************************************************
%                             ## SYNTAX ##
%
%       [T] = MBRG(Pre,Post,M0,F,L,E)
%
%       Given a Petri net with its matrices Pre and Post, the initial
%       marking M0, the matrix F that indicates the Fault class and the
%       matrix L that  indicates the Labeling function,this function
%       returns a matrix T that contains for each row:
%       1) the node's number;
%       2) the basis markings that belong to the node;
%       3) the Xfault vector;
%       4) the transitions enabled by the node;
%       5) the nodes that are reachable with the firing of any observable
%          or faulty transition in association with the j-vector
%          correspondent.
%          _______________________________________________________
%         |    1   |      2      |   3    |         4             |
%       T=|________|_____________|________|_______________________|
%         | # Node |Basis Marking|X fault | Enabled Transitions   |
%         |        |             |        |(observable and faulty)| 
%         |________|_____________|________|_______________________|
%          ___________________________________________
%         |     5      | 6 |      7      |     8      |
%         |____________|___|_____________|____________|
%         |Arc-t Matrix|Tag|Enabled label|Arc-l Matrix|
%         |            |   |             |            |
%         |____________|___|_____________|____________|
%
%      F must be a cell array that has as many rows as the number of fault
%      classes, that contains in each row the fault transitions that belong
%      to the corresponding fault class.
%      L must be a cell array that has as many rows as the cardinality of
%      the considered alphabet, that contains in each row the observable
%      transitions having the same label.
%      E must be a cell array that contains in each row a string of
%      characters, each one corresponding to a different label in the
%      considered alphabet. Obviously, the cell array E is ordered
%      according to L.
%
%       See also: BRG, BRD, showMBRG, diagnosability


% Verification of the input parameters

[m,n]=size(Post);
[c,p]=size(M0');
[sizeL,lenghtL]=size(L);
[numTclas,b]= size(F);

if (size(Post)==size(Pre))&(c==1)&(p==m),
elseif size(Post)~=size(Pre)
   fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
   fprintf('\n Put again the data\n')
else
   fprintf('\n  ERROR! dimensions of the  marking are wrong!\n')
end

if (lenghtL==1),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Labeling Function are wrong!\n')
end

if (b==1),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n the Fault Class are wrong!\n')
end

if (size(L)==size(E)),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Symbol set is not consistent wrt\n  the Labeling function!\n')
end

% Computation of the number of observable transitions (no), of faulty 
% transitions (nf), regular transitions (nreg) and non observable transitions (nu)

no=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    no=no+lenghtLi;
end


nf=0;
for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    nf=nf+lenghtFi;
end

nreg=n-no-nf;

nu=nreg+nf;

% It is necessary to order the transitions as follows:
% first, observable transitions; then, faulty transitions; finally, regular transitions.

MPre=[];
MPost=[];
MF=[];
ML=[];

% Vector used in the following to keep track of the original enumeration of 
% the transitions.
% As an example: if numbers(i)=j then transition tj has been enumerated as ti

numbers=zeros(1,n);

% Definition of a binary vector such that
% if(reg(i)==1)
% then the i-th transition is a regular transition.
% Finally, a counter is defined to reset the matrices who define the 
% labeling function and the fault classes.

reg=ones(1,n);
count=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    for j=1:lenghtLi
        count=count+1;
        t=L{i,1}(1,j);
        reg(t)=0;
        numbers(count)=t;
        MPre=[MPre,Pre(:,t)];
        MPost=[MPost,Post(:,t)];
        ML{i,1}(1,j)=count;
    end
end

for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    for j=1:lenghtFi
        count=count+1;
        f=F{i,1}(1,j);
        reg(f)=0;
        numbers(count)=f;
        MPre=[MPre,Pre(:,f)];
        MPost=[MPost,Post(:,f)];
        MF{i,1}(1,j)=count;
    end
end

for i=1:n
    if (reg(i)==1)
        count=count+1;
        numbers(count)=i;
        MPre=[MPre,Pre(:,i)];
        MPost=[MPost,Post(:,i)];
    end
end

Pre=MPre;
Post=MPost;
L=ML;
F=MF;

% Starting od the temporal counting
%tic

% Initialization of the data matrix of the MBRG

T=[{1} {[M0]} {[zeros(1,(numTclas))]} {[zeros(1,(n-nreg))]} {[(empty_vector(n-nreg))]} {[0]}];

c=cell2mat(T(:,6));
d=find(c==0);

% Until there exists at least one node with no TAG, it computes the coordinates 
% of the nodes to be explored and save them in the vector d


while ~isempty(d)
       d=find(c==0);
          
for i=1:n-nreg
    
    % For any reachable basis marking Mcurrent whose i-th transition is
    % observable or faulty, starting from the first unexplored marking MM
    % and for all the j-vectors associated with it, we define the arcs of
    % the graph.
    
    MM=T{d(1,1),2};
    Mb=Mbasis( Pre , Post ,MM , nreg , i );
    %sizeMb21 equal to the number of markings reachable from node MM
    [sizeMb21,lengthMb21]=size(Mb{2,1});
    
    %for all the reachable markings and the corresponding j-vector
    for j=1:sizeMb21 
       
        Nodeold=0;    
        Mcurrent=Mb{2,1}(j,:);
        
        % Verifies it the faulty transitions are enabled
        % This function should be recompiled to be used on
        % MAC-OS
        Sol =constrainset(Pre , Post , Mcurrent' , nu , F);
        %Sol=faultclass(Pre , Post , Mcurrent' , nu, 0 ,F);
        
        % Computes the number of j-vectors associated with the transition at hand
        [sizeJi,lenghtJi]=size(Mb{2,2}{j,1});
    
        for jj=1:sizeJi
            jcurrent=Mb{2,2}{j,1}(jj,:);

            % Check if a transition is enabled
            if( ~isempty( find(Mcurrent<0) ) )
                T{d(1,1),4}(1,i)=0;
                continue
    
            % Verifies that a self-loop transition does not fire if the corresponding place is unmarked
            elseif(min(Mcurrent'- Post(:,i))<0)
                T{d(1,1),4}(1,i)=0;
                continue
            
            else % if the transition is enabled and leads to a marking Mcurrent
                [numofnode, lengthT]=size(T);
                
                % Verifies if the marking is already in the graph
                for k=1:numofnode
                    if(Mcurrent'==T{k,2})
                        T{d(1,1),4}(1,i)=1;
                        [sizej,lengthj]=size(T{d(1,1),5}{1,i});
                        T{d(1,1),5}{1,i}{sizej+1,1}={[k] [jcurrent]};
                        Nodeold=1;
                    else
                    end
                end

                % If the marking is not already contained in the graph, we add a new node
                if(Nodeold==0) % creates the new node
                    
                    [numofnode, lengthT]=size(T);
                    T{numofnode+1,1}=[numofnode+1];
                    T{numofnode+1,2}=[Mcurrent'];
                    T{numofnode+1,3}= Sol;
                    T{numofnode+1,4}= zeros(1,n-nreg);
                    A =empty_vector(n-nreg);
                    T{numofnode+1,5}= A;
                    T{numofnode+1,6}= 0;

                    % Updates the considered node

                    T{d(1,1),4}(1,i)=1;
                    
                    % This function should be recompiled to be used on MAC-OS
                    Sol2 =constrainset(Pre , Post , MM , nu , F);
                    %Sol2=faultclass(Pre , Post , MM , nu, 0 ,F);

                    T{d(1,1),3}=Sol2;
            
                    [sizej,lengthj]=size(T{d(1,1),5}{1,i});
                    T{d(1,1),5}{1,i}{sizej+1,1}={[numofnode+1] [jcurrent]};
                    T{d(1,1),6}= 0;
                    
                    
                    [numofnode, lengthT]=size(T);
                end
            end
        end
    end
end
T{d(1,1),6}= 1;

% Verifies again the unexplored nodes
c=cell2mat(T(:,6));
d=find(c==0);
end

% We insert the new matrix of the arcs in the column number 7 of the matrix T of the MBRG

[sizeT,lenghtT]=size(T);
nArc=sizeL+numTclas;

% Generates the array where to include the binary vector of the enabled transitions

for n=1:sizeT
    T{n,7}=zeros(1,nArc);
end
% For all the nodes of the MBRG
for n=1:sizeT		
	T{n,8}=empty_vector(nArc);
    % for all the labels
    for i=1:sizeL
		[sizeLi,lenghtLi]=size(L{i,1});
		% for the j-th transition belonging to the j-th class
        count=0;
        for j=1:lenghtLi
            transition=L{i,1}(1,j);   
			if((T{n,4}(1,transition))==1)
				T{n,7}(1,i)=1;
                [sizeJi,lenghtJi]=size(T{n,5}{transition});
                for jj=1:sizeJi
                    count=count+1;
                    % {[symbol][transition] {[arrival node][j-vector]}}
                    T{n,8}{count,i}={[E{i,1}] [numbers(transition)] T{n,5}{1,transition}{jj,1}};
                end
			end
		end
    end
    
    % It could happen that there are no faulty transitions, thus the first verification is done
    [si,le]=size(F);
    
    if(si~=0)
    for k=1:numTclas
        [sizeFk,lenghtFk]=size(F{k,1});% F{k,1} is a row vector containing the transitions of the k-th fault class
        countf=0;
		for p=1:lenghtFk              % for the p-th transition belonging to the k-th fault class
			faulty=F{k,1}(1,p);       % reads the number associated with the faulty transition
            if((T{n,4}(1,faulty))==1) % if such a transition is enabled, i.e., there exists an output arc from the node
                T{n,7}(1,sizeL+k)=1;
                [sizeJi,lenghtJi]=size(T{n,5}{faulty});
                for jj=1:sizeJi
                    countf=countf+1;
                    % {[no label][t_failure] {[end node][j-vector]}}
                    T{n,8}{countf,sizeL+k}={0,[numbers(faulty)] T{n,5}{1,faulty}{jj,1}};
                end
			end
		end
    end
    end
        
end


%toc
%t=toc;
end