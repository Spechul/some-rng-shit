function [T] =BRG_D(Pre,Post,M0,F,L,E)

%BRG:  This function builds the basis reachability graph of basis
%      markings of a Petri net for diagnosis.
%      
%      ********************************************************************
%                                 ## SYNTAX ##
%
%      [T] = BRG(Pre,Post,M0,F,L,E)
%
%      Given a Petri net with its matrices Pre and Post,the initial marking
%      M0, the matrix F, that indicates the Fault class,the matrix L, that
%      indicates the Labeling function, and the Matrix E,that indicates the
%      Alphabet of label, this function returns a matrix T that contains
%      for each row:
%      1) the node's number;
%      2) the basis markings that belong to the node;
%      3) the Xfault vector;
%      4) the transitions enabled by the node;
%      5) the nodes that are reachable with the firing of any transition
%         in association with the j-vector correspondent.
%     
%         _______________________________________________________________
%        |   1    |      2      |   3   |        4          |     5      |
%      T=|________|_____________|_______|___________________|____________|
%        |# Number|Basis Marking|X fault|Enabled Transitions|Arc-t Matrix|
%        |________|_____________|_______|___________________|____________|
%
%         ______________________________
%        | 6 |       7     |     8      |
%        |___|_____________|____________|
%        |Tag|Enabled label|Arc-l Matrix|
%        |___|_____________|____________|
%
%      F must be a cell array that has as many rows as the number of fault
%      classes, that contains in each row the fault transitions that belong
%      to the corresponding fault class.
%      L must be a cell array that has as many rows as the cardinality of
%      the considered alphabet, that contains in each row the observable
%      transitions having the same label.
%      E must be a cell array that contains in each row a string of
%      characters, each one corresponding to a different label in the
%      considered alphabet. Obviously, the cell array E is ordered
%      according to L.
%
%      See also: MBRG,BRD, showBRG, diagnosability

% Verification of input parameters

[m,n]=size(Post);
[c,p]=size(M0');
[sizeL,lenghtL]=size(L);
[numTclas,b]= size(F);

if (size(Post)==size(Pre))&(c==1)&(p==m),
elseif size(Post)~=size(Pre)
   fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
   fprintf('\n Put again the data\n')
else
   fprintf('\n  ERROR! dimensions of the  marking are wrong!\n')
end

if (lenghtL==1),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Labeling Function are wrong!\n')
end

if (b==1),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n the Fault Class are wrong!\n')
end

if (size(L)==size(E)),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Symbol set is not consistent wrt\n  the Labeling function!\n')
end

% Computation of number of observable(no), faulty (nf), regular (nreg) and
% unobservable (nu) transitions

no=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    no=no+lenghtLi;
end


nf=0;
for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    nf=nf+lenghtFi;
end

nreg=n-no-nf;

nu=nreg+nf;


% It is strictly necessary to order transitions first considering observable 
% transitions, and then unobservable transitions. For consistency with the
% MBRG, unobservable transitions are ordered first positioning faulty transitions, 
% and then regular ones.

MPre=[];
MPost=[];
MF=[];
ML=[];

% Vector used in the following to keep track of the original enumeration of
% transitions
% Ex: if numbers(i)=j then transition tj has been enumerated as ti

numbers=zeros(1,n);

% Definition of a binary vector such that
% if(reg(i)==1)
% then the i-th transition is a regular transition.
% Finally, a counter useful to reset the matrices defining the labeling function 
% and the fault classes

reg=ones(1,n);
count=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    for j=1:lenghtLi
        count=count+1;
        t=L{i,1}(1,j);
        reg(t)=0;
        numbers(count)=t;
        MPre=[MPre,Pre(:,t)];
        MPost=[MPost,Post(:,t)];
        ML{i,1}(1,j)=count;
    end
end

for i=1:numTclas
    [sizeFi,lenghtFi]=size(F{i,1});
    for j=1:lenghtFi
        count=count+1;
        f=F{i,1}(1,j);
        reg(f)=0;
        numbers(count)=f;
        MPre=[MPre,Pre(:,f)];
        MPost=[MPost,Post(:,f)];
        MF{i,1}(1,j)=count;
    end
end

for i=1:n
    if (reg(i)==1)
        count=count+1;
        numbers(count)=i;
        MPre=[MPre,Pre(:,i)];
        MPost=[MPost,Post(:,i)];
    end
end


Pre=MPre;
Post=MPost;
L=ML;
F=MF;

% Starting of the temporal counting
%tic

% Initialization of the data matrix of the BRG

T=[{1} {[M0]} {[zeros(1,(numTclas))]} {[zeros(1,no)]} {[(empty_vector(no))]} {[0]}];

c=cell2mat(T(:,6));
d=find(c==0);


% Since there exist at least a node with no TAG, it finds out the coordinates of the nodes
% to be explored and save them in the vector d

while ~isempty(d)
	d=find(c==0);
	% For all the observable transitions:
    for i=1:no
    
    % For all the basis markings reachable from Mcurrent with the
    % i-th observable transition, starting from the first unexplored
    % marking MM and for each associated j-vector, we define the arcs of the graph
    
        MM=T{d(1,1),2};
        %fprintf('Transizione ', i)
        Mb=Mbasis( Pre , Post ,MM , nu , i);
        %sizeMb21 is the number of markings reachable from node MM
        [sizeMb21,lengthMb21]=size(Mb{2,1});
    
        for j=1:sizeMb21
            Nodeold=0;    
            Mcurrent=Mb{2,1}(j,:);
        
        % We verify if the fault transitions are enabled.
        % This function should be recompiled if it should be used on MAC-OS
            Sol =constrainset(Pre , Post , Mcurrent' , nu , F);
        
        % Use this one in the case that it is not possible to do that
        %Sol =faultclass(Pre , Post , Mcurrent' , nu , 0 ,F);
        
        % We verify the number of j-vectors associated with the considered transition
            [sizeJi,lenghtJi]=size(Mb{2,2}{j,1});
        
            for jj=1:sizeJi
                jcurrent=Mb{2,2}{j,1}(jj,:);
            % We should remember that the reordering of the transitions leads also 
            % to a reordering of the firing vector of the justification:
%             a=zeros(size(jcurrent));
%             for jc=1:nu
%                 a(numbers(no+jc)-no)=jcurrent(jc);
%             end
%             a

                a=[numbers(1,no+1:end);jcurrent];
                temp=sortrows(a')';
                jcurrent=temp(2,:);
            % We verify if a transition is enabled
                if( ~isempty( find(Mcurrent<0) ) )
                    T{d(1,1),4}(1,i)=0;
                    continue
        
            % We verify that a self-loop transition fires only if the corresponding 
            % place is marked
                elseif(min(Mcurrent'- Post(:,i))<0)
                    T{d(1,1),4}(1,i)=0;
                    continue
        
                else % if the transition is enabled and leads to Mcurrent
%             fprintf('\n\nFrom marking ')
%             fprintf('%d ',MM)
%             fprintf ('\n to marking ')
%             fprintf('%d ',Mcurrent)
%             fprintf('\nwith t%d and justification ',i)
%             fprintf('%d ',jcurrent)

                    [numofnode, lengthT]=size(T);

                % We verify that the marking is not already in the graph
                    for k=1:numofnode
                        if(Mcurrent'==T{k,2})
                            T{d(1,1),4}(1,i)=1;
                            [sizej,lengthj]=size(T{d(1,1),5}{1,i});
                            T{d(1,1),5}{1,i}{sizej+1,1}={[k] [jcurrent]};
                            Nodeold=1;
%                             fprintf('\n\narc %d from node %d to node %d with justification',i,d(1,1),k)
%                             fprintf(' %d',jcurrent)
                        end            
                    end
                
                % If the marking is not already present, Nodeold==0, we add
                % a new node
                    if(Nodeold==0) % we create the new node
                        
                        [numofnode, lengthT]=size(T);
                        T{numofnode+1,1}=[numofnode+1];
                        T{numofnode+1,2}=[Mcurrent'];
                        T{numofnode+1,3}= Sol;
                        A =empty_vector(n-nu);
                        T{numofnode+1,4}= zeros(1,no);
                        T{numofnode+1,5}= A;
                        T{numofnode+1,6}= 0;
                        
                    % We update the values of the considered node

                        T{d(1,1),4}(1,i)=1;
                    
                    % This function should be recompiled if it should be used on MAC-OS
                        Sol2 =constrainset(Pre , Post , MM , nu , F);
                    %Sol2=faultclass(Pre , Post , MM , nu , 0 ,F);
                    
                        T{d(1,1),3}=Sol2;
                        [sizej,lengthj]=size(T{d(1,1),5}{1,i});
                        T{d(1,1),5}{1,i}{sizej+1,1}={[numofnode+1] [jcurrent]};
                        T{d(1,1),6}= 0;
%                         fprintf('\n\narc %d from node %d to node %d with justification',i,d(1,1),numofnode+1)
%                         fprintf(' %d',jcurrent)
                        [numofnode, lengthT]=size(T);
                    end        
                end
            end
        end
    end

    T{d(1,1),6}= 1;

% We verify again the nodes not explored yet

    c=cell2mat(T(:,6));
    d=find(c==0);
end

% We insert the new matrix of the arcs in the column number 7 of the matrix T of the BRG

[sizeT,lenghtT]=size(T);

% We generate the array where to insert the binary vector of the enabled transitions

for n=1:sizeT
    T{n,7}=zeros(1,sizeL);
end

% For all the nodes of the BRG
for n=1:sizeT		
	T{n,8}=empty_vector(sizeL);
    % for all the labels
    for i=1:sizeL
		[sizeLi,lenghtLi]=size(L{i,1});
		% for the j-th transitions belonging to the j-th class
        count=0;
        for j=1:lenghtLi
            transition=L{i,1}(1,j);
			if((T{n,4}(1,transition))==1)
				T{n,7}(1,i)=1;
                [sizeJi,lenghtJi]=size(T{n,5}{transition});
                for jj=1:sizeJi
                    count=count+1;
                    % {[symbol][transition] {[arrival node][j-vector]}}
                    T{n,8}{count,i}={[E{i,1}] [numbers(transition)] T{n,5}{1,transition}{jj,1}};
                end
			end
		end
	end
end


%toc
%t=toc;
end