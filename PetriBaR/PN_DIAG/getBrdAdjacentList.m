function [A] =getBrdAdjacentList(BRD)

%GETBRDARCMATRIX: This function get arc costs matrix of the Basis
%                 Reachability Diagnoser'graph
%
%                 *********************************************************
%                                       ## SYNTAX ##
%
%                 [A] =getBrdAdjacentList(BRD)
%
%                 Given the BRD, this function returns the Adjacent List
%                 correseponding to the BRD.
%                         
%                         
%                             
%                 E(i,:)= {j: exist an arc from the i-th to j-t nodeh
%           
%                 See also: BRD,getBrdArcMatrix


% It verifies that the number of the input arguments to the function is 
% equal to two. If such is not the case, an error message is produced.

ni=nargin;
error(nargchk(1,1,ni));

% Initialization of the cost matrix of the arcs.
[sizeBRD,lenghtBRD]= size(BRD);
A=[];
temp=[];

for i=1:sizeBRD
    count=0;
    temp=[];
    [sizeBRDi,lenghtBRDi]=size(BRD{i,4});
    for j=1:lenghtBRDi
        if(size(BRD{i,4}{j})~=0)
            to=BRD{i,4}{j}{2};
            %if(to~=i)
                count=count+1;
                to=BRD{i,4}{j}{2};
                temp(1,count)=to;
            %end
        end
    end
    temp=sort(temp);
    A(i,:)=temp;
end


end
