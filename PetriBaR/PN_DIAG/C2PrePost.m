%C2PrePost(C)
%
%C: Incidence matrix
%
%Computing Pre e Post matrices from C; it is necessary that the network is pure (no loops).

function [Pre, Post] = C2PrePost(C)

[r, c] = size(C);
Pre = zeros(r,c);
Post = zeros(r,c);

for i = 1:r
    for j = 1:c
        if(C(i,j) < 0)
            Pre(i,j) = -C(i,j);
        end
        if(C(i,j) > 0)
            Post(i,j) = C(i,j);
        end
    end
end