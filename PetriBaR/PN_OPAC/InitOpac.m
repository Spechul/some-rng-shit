% function [ISO,EstB,yb]=InitOpac(Pre,Post,M0,L,E,S)
function [ISO,EstB,yb]=InitOpac(B,S)
% Given an LPN system (Pre,Post,M0,L,E) and a secret S, which is described
% by a set of GMECs, this function is to check if the system is
% initial-state opaque wrt S.

% S={W;K} is a cell array, where W is a m*r demension matrix, K is a r*1 vector, m is the no.
% of places and r is the no. of GMECs.  It denotes the secret
% S={M|W'*M<=K}.

%% construc the BRG of the LPN
%[B] =BRG_L(Pre,Post,M0,L,E);

%% construct the estimator
[EstB,yb]=estBRG(B);

%% check if each state of the estimator contains an exposable basis marking. Namely, if there exists wi'*M>ki
W=S{1};
K=S{2};
r=size(K,1); % no. of GMECs

Tag=[];
for i=1:EstB{1}
    j=1;
    tag=0;
    while tag==0 && j<=size(yb{i},2)
        Mb=B{yb{i}(1,j)+1,2};
        l=1;
        while tag==0 && l<=r
            if W(:,l)'*Mb>K(l) % Mb is exposable
                tag=1;
            else
                tag=0;
            end
            l=l+1;
        end
        j=j+1;
    end
    Tag(i)=tag;
end

if all(Tag)
    ISO=1;
    fprintf('\n The LPN is initial-state opaque wrt to the given secret.\n')
else
    ISO=0;
    fprintf('\n The LPN is NOT initial-state opaque wrt to the given secret.\n')
end
end