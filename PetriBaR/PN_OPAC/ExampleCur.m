function [CSO,Obs,y]=ExampleCur

% This funtion is to check if the system in Fig. 1 of the TAC17_Opacity
% paper is CSO

%% The net structure.
Post=[0 0 0 1 1;1 0 0 0 0;0 1 0 0 0;0 0 1 0 0];
Pre=[1 0 0 0 0;0 1 0 0 0;0 0 1 1 0;0 0 0 0 1];
M0=[1 1 0 0]';
E={['a']};
L={[1,3]};
W=[-1 0 0 -1];
K=-2;
S={W;K};

%% check if the system is CSO
[CSO,Obs,y]=CurOpac(Pre,Post,M0,L,E,S);
end