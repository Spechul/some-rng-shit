function [B] =BRG_Cur(Pre,Post,M0,L,E,S)

%BRG:  This function builds the basis reachability graph for opacity.
%      
%      ********************************************************************
%                                 ## SYNTAX ##
%
%      [T] = BRG(Pre,Post,M0,S,L,E)
%
%      Given a Petri net with its matrices Pre and Post,the initial marking
%      M0, the matrix F, that descibes the secret set,the matrix L, that
%      indicates the Labeling function, and the Matrix E,that indicates the
%      alphabet of label, this function returns a matrix T that contains
%      for each row:
%      1) the node's number;
%      2) the basis markings that belong to the node;
%      3) the opacity vector;
%      4) the transitions enabled by the node;
%      5) the nodes that are reachable with the firing of any transition
%         in association with the j-vector correspondent.
%     
%         _______________________________________________________________
%        |   1    |      2      |   3   |        4          |     5      |
%      T=|________|_____________|_______|___________________|____________|
%        |# Number|Basis Marking|Opacity|Enabled Transitions|Arc-t Matrix|
%        |________|_____________|_______|___________________|____________|
%
%         ______________________________
%        | 6 |       7     |     8      |
%        |___|_____________|____________|
%        |Tag|Enabled label|Arc-l Matrix|
%        |___|_____________|____________|
%
%      L must be a cell array that has as many rows as the cardinality of
%      the considered alphabet, that contains in each row the observable
%      transitions having the same label.
%      E must be a cell array that contains in each row a string of
%      characters, each one corresponding to a different label in the
%      considered alphabet. Obviously, the cell array E is ordered
%      according to L.

%      S={W;K} is a cell array, where W is a r*m demension matrix, K is a r*1 vector, m is the no.
%      of places and r is the no. of GMECs.  It denotes the secret
%      S={M|W*M<=K}.
% !! Note that function Cons.m calling here checks if there exists wi*M>ki having a solution.
% !! Therefore, the input S of funciton BRG_Cur,in fact, needs to be the
% !! complement of S

%% compute the complement of S
W1=S{1,1};
W=W1';
K=S{2,1};
[sizeW,lengthW]=size(W);
[sizeK,lengthK]=size(K); % no. of GMECs

NK=S{2}+ones(sizeK,1);

%% Verfy the input data

[m,n]=size(Post);
[c,p]=size(M0');
[sizeL,lenghtL]=size(L);

% Determine the number of GMECs


if (size(Post)==size(Pre))&(c==1)&(p==m),
elseif size(Post)~=size(Pre)
   fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
   fprintf('\n Put again the data\n')
else
   fprintf('\n  ERROR! dimensions of the  marking are wrong!\n')
end

if (lenghtL==1),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Labeling Function are wrong!\n')
end

% if (b==1),
% else
%    fprintf('\n  ERROR! dimensions of the Matrix representing\n the Fault Class are wrong!\n')
% end

if (size(L)==size(E)),
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the Symbol set is not consistent wrt\n  the Labeling function!\n')
end

if (sizeW==sizeK) & (lengthW==m) & (lengthK==1)
else
   fprintf('\n  ERROR! dimensions of the Matrix representing\n  the GMECs are wrong!\n')
end


% Determine respectively the number of obsrvable transitions (no),
% and unobservable transtions (nu)

no=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    no=no+lenghtLi;
end

nu=n-no;

numGMEC=sizeW; % Determine the number of GMECs

% It is strictly necessary to order the transitions first positioning the 
% observable ones, and then the unobservable ones. For consistency with the
% MBRG, unobservable transitions are ordered first considering the fault transitions 
% and then the regular ones.

MPre=[];
MPost=[];
MF=[];
ML=[];

% Vector used later on to keep track of the original enumeration of transitions.
% Ex: if numbers(i)=j then the transition tj has been enumerated as ti

numbers=zeros(1,n);

% Definition of a binary vector such that
% if(reg(i)==1)
% then the i-th transition is a regular transition.
% Finally, a counter is introduced to reset the matrices defining the labeling function and the fault classed

rego=ones(1,n);
count=0;
for i=1:sizeL
    [sizeLi,lenghtLi]=size(L{i,1});
    for j=1:lenghtLi
        count=count+1;
        t=L{i,1}(1,j);
        rego(t)=0;
        numbers(count)=t;
        MPre=[MPre,Pre(:,t)];
        MPost=[MPost,Post(:,t)];
        ML{i,1}(1,j)=count;
    end
end

for i=1:n
    if (rego(i)==1)
        count=count+1;
        numbers(count)=i;
        MPre=[MPre,Pre(:,i)];
        MPost=[MPost,Post(:,i)];
    end
end


Pre=MPre;
Post=MPost;
L=ML;
F=MF;

% Start the timer
%tic

% Initialize the BRG matrix T

B=[{1} {[M0]} {[0]} {[zeros(1,no)]} {[(empty_vector(no))]} {[0]}];

c=cell2mat(B(:,6));
d=find(c==0);


% Since there exists at least one node without TAG, it computes the coordinates 
% of the nodes that should be explored and save them in the vector d

while ~isempty(d)
	d=find(c==0);
	% For all the observable transitions:
    for i=1:no
    
    % For each basis marking reachable from Mcurrent with the i-th observable
    % transition, starting from the first unexplored marking MM and for each associated 
    % j-vector, we define the arcs of the graph.
    
        MM=B{d(1,1),2};
        %fprintf('Transition ', i)
        Mb=Mbasis( Pre , Post ,MM , nu , i);
        %sizeMb21 is the number of basis markings reachable from MM
        [sizeMb21,lengthMb21]=size(Mb{2,1});
    
        for j=1:sizeMb21 
            Nodeold=0;    
            Mcurrent=Mb{2,1}(j,:);
        
        % We verify if the fault transitions are enabled.
        % This function should be recompiled if it should be run on MAC-OS
        
          ftag=Cons(Pre , Post , Mcurrent' , nu , W , NK);
        
        %Use this, if it is not possible to do so
        %Sol =faultclass(Pre , Post , Mcurrent' , nu , 0 ,F);
        
        % We verify the number of j-vectors associated with the considered transition
            [sizeJi,lenghtJi]=size(Mb{2,2}{j,1});
        
            for jj=1:sizeJi
                jcurrent=Mb{2,2}{j,1}(jj,:);
                a=[numbers(1,no+1:end);jcurrent];
                temp=sortrows(a')';
                jcurrent=temp(2,:);
            % It verifies if a transition is enabled or not
                if( ~isempty( find(Mcurrent<0) ) )
                    B{d(1,1),4}(1,i)=0;
                    continue
        
            % It verifies that a self-loop transition does not fire if the corresponding place is not marked
                elseif(min(Mcurrent'- Post(:,i))<0)
                    B{d(1,1),4}(1,i)=0;
                    continue
        
                else % if the transition is enabled and leads to Mcurrent
%             fprintf('\n\nFrom marking ')
%             fprintf('%d ',MM)
%             fprintf ('\n to marking ')
%             fprintf('%d ',Mcurrent)
%             fprintf('\nwith t%d and justification ',i)
%             fprintf('%d ',jcurrent)

                    [numofnode, lengthT]=size(B);

                % we check if the marking is already in the graph
                    for k=1:numofnode
                        if(Mcurrent'==B{k,2})
                            B{d(1,1),4}(1,i)=1;
                            [sizej,lengthj]=size(B{d(1,1),5}{1,i});
                            B{d(1,1),5}{1,i}{sizej+1,1}={[k] [jcurrent]};
                            Nodeold=1;
%                             fprintf('\n\narc %d from node %d to node %d with justification',i,d(1,1),k)
%                             fprintf(' %d',jcurrent)
                        end            
                    end
                
                % If the marking is not already present, Nodeold==0, we add
                % a new node
                    if(Nodeold==0) % we create the new node
                        
                        [numofnode, lengthT]=size(B);
                        B{numofnode+1,1}=[numofnode+1];
                        B{numofnode+1,2}=[Mcurrent'];
                        B{numofnode+1,3}= ftag;
                        A =empty_vector(n-nu);
                        B{numofnode+1,4}= zeros(1,no);
                        B{numofnode+1,5}= A;
                        B{numofnode+1,6}= 0;
                        
                    % We update the values of the node ad hand

                        B{d(1,1),4}(1,i)=1;
                    
                    % This function should be recompiled to be used on MAC-OS
                    
                       ftag2=Cons(Pre , Post , MM , nu , W , NK);

                    %Sol2=faultclass(Pre , Post , MM , nu , 0 ,F);
                        
                        B{d(1,1),3}=ftag2;
                        [sizej,lengthj]=size(B{d(1,1),5}{1,i});
                        B{d(1,1),5}{1,i}{sizej+1,1}={[numofnode+1] [jcurrent]};
                        B{d(1,1),6}= 0;
%                         fprintf('\n\narc %d from node %d to node %d with justification',i,d(1,1),numofnode+1)
%                         fprintf(' %d',jcurrent)
                        [numofnode, lengthT]=size(B);
                    end        
                end
            end
        end
    end

    B{d(1,1),6}= 1;

% we check the nodes not yet explored

    c=cell2mat(B(:,6));
    d=find(c==0);
end

% We insert the new matrix of the arcs in the column number 7 of the matrix T of the BRG

[sizeT,lenghtT]=size(B);

% we generate the array where to include the binary vector of the enabled transitions

for n=1:sizeT
    B{n,7}=zeros(1,sizeL);
end

% For all the nodes of the BRG
for n=1:sizeT		
	B{n,8}=empty_vector(sizeL);
    % for all the labels
    for i=1:sizeL
		[sizeLi,lenghtLi]=size(L{i,1});
		% for the j-th transition belonging to the j-th class
        count=0;
        for j=1:lenghtLi
            transition=L{i,1}(1,j);
			if((B{n,4}(1,transition))==1)
				B{n,7}(1,i)=1;
                [sizeJi,lenghtJi]=size(B{n,5}{transition});
                for jj=1:sizeJi
                    count=count+1;
                    % {[symbol][transition] {[arrival node][j-vector]}}
                    B{n,8}{count,i}={[E{i,1}] [numbers(transition)] B{n,5}{1,transition}{jj,1}};
                end
			end
		end
	end
end


%toc
%t=toc;
end