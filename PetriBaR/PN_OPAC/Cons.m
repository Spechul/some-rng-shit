function [ftag]=Cons(Pre , Post , Mb , nu , W, K)
addpath(genpath(pwd));
% constrainset:    This function resolve the following constraits set  :
%                   _
%                  |  M0 +Cu*z >=0
%                 <                 .
%                  |_ z(tf) >0
% 
%                  Where M0 is whichever Marking obtained by the firing of
%                  the suffix i-w and tf is a fault transition.
%                  If the set admit solution the Sol will be 1, otherwise
%                  the Sol will be 0.
%
%                  ********************************************************
%                                       ## SYNTAX ##
%
%                  [Sol] =constrainset(Pre, Post, M0, nu, F)
%
%                  Given a Petri net with its matrices Pre and Post, the
%                  initial marking M0, the number of unobservable
%                  transitions nu and the matrix F, that indicates the
%                  Fault class.
%                  F must be a cell matrix [numFaultclass X 1] that 
%                  describes the faults class:in the i-th row of the matrix
%                  there will be the numbers of unobservable transitions
%                  that belong at the Ti-th fault class.


[pn,tn]=size(Pre);
[x,c]=size(Mb);
[numGMEC,lengthW]=size(W);

 % verification of the input data
if (size(Post)==size(Pre))&(c==1)&(pn==x),
     elseif size(Post)~=size(Pre)
      fprintf('\n ERROR!! Matrices Pre and Post have different dimension!\n')
      fprintf('\n Insert again the data\n')
     else
      fprintf('\n ERRORE! dimensions of the  marking are wrong!\n')
      return
end

    
 
C=Post-Pre;
Cu=(C(:,(tn-nu+1):tn));
c=zeros(1,nu);
ftag=0;

for i=1:numGMEC
    wi=W(i,:)';
    ki=K(i);
    SENSE=-1;
    A=[Cu;wi'*Cu];
    B=[-Mb;ki-wi'*Mb];
    C=c';
    CTYPE='L';
    CTYPE(1:pn+1)='L';
    CTYPE=CTYPE';
    LB=zeros(nu,1);
    UB=Inf(nu,1);
    VARTYPE='I';
    VARTYPE(1:nu)='I';
    VARTYPE=VARTYPE';
    PARAM=0;
    LPSOLVER=1;
    SAVE=0;
    
    [XMIN,FMIN,STATUS]=glpkmex(SENSE,C,A,B,CTYPE,LB,UB,VARTYPE,PARAM,LPSOLVER,SAVE);

    Tag=0;
    if(STATUS==5)   
        Tag=1;
    elseif (STATUS==4)
        Tag=0;
    end
    ftag=ftag|Tag;
end