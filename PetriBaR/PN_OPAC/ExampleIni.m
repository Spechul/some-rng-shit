function [ISO,EstB,yb]=ExampleIni
% function [Ibw]=ExampleIni
% This funtion is to compute the initial-state estimators corresponding to
% the RG and BRG of the LPN with different value of parameter k. Exmaple
% 5.9 in the TAC2017_Opacity paper.
%% k is the number of tokens in place p2.
%% Re is the initial-state estimator of the RG.
%% Be is the initial-state estimator of the BRG.

% The net structure.
Post=[0 0 0 1 1;1 0 0 0 0;0 1 0 0 0;0 0 1 0 0];
Pre=[1 0 0 0 0;0 1 0 0 0;0 0 1 1 0;0 0 0 0 1];
M0=[1 1 0 0]';
%M0(2)=k;
E={['a'];['b']};
L={[1];[3]};%%%% Lmat is supposed to be computed based on E and L.
S={[-1;0;0;0];[-1]};

[ISO,EstB,yb]=InitOpac(Pre,Post,M0,L,E,S);
end