% function [CSO,ObsR,yr]=CurOpac(Pre,Post,M0,L,E,S)
function [CSO,ObsB,yb]=CurOpac(B)
% Given an LPN system (Pre,Post,M0,L,E) and a secret S, which is described
% by a set of GMECs, this function is to check if the system is
% current-state opaque wrt S.

% S={W;K} is a cell array, where W is a m*r demension matrix, K is a r*1 vector, m is the no.
% of places and r is the no. of GMECs.  It denotes the secret
% S={M|W'*M<=K}.

%% construc the BRG for CSO
%[B] =BRG_Cur(Pre,Post,M0,L,E,S);

%% construct the osberver
NFA=Conv(B);
[ObsB,yb]=NFA2DFA(NFA);

%% check if each state of the observer contains a weakly exposable basis marking
Tag=[];
for i=1:ObsB{1}
    temp=0;
    for j=1:size(yb{i},2)
        Tag(i)=B{yb{i}(1,j)+1,3}(1)|temp;
        temp=B{yb{i}(1,j)+1,3}(1);
    end
end
if all(Tag)
    CSO=1;
    fprintf('\n The LPN is current-state opaque wrt to the given secret.\n')
else
    CSO=0;
    fprintf('\n The LPN is NOT current-state opaque wrt to the given secret.\n')
end
end